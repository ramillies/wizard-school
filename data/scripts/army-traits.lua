return {
	party = {
		evil = {
			{ immunityText = [[People in this party are very tough and won't be stopped by thrown objects and pain.]], negate = { "torture", "throw" }, enable = { } },
			{ immunityText = [[This is a party of giants that will be impossible to restrain or blow away.]], negate = { "grasping vines", "gust of wind", "swiftness", "stoneskin" }, enable = { } },
			{ immunityText = [[People in this party come from the harsh North where freezing cold is as common as lightning strikes from an angry god.]], negate = { "freeze", "lightning strike" }, enable = { } },
			{ immunityText = [[This is a party of shambling undead husks remaining from people long dead.]], negate = { "bestow curse", "drain life", "torture", "poison", "kill" }, enable = { "destroy undead", "control undead", "sanctify" } },
		},
		good = {
			{ immunityText = [[People in this party are very tough and won't be stopped by thrown objects and pain.]], negate = { "torture", "throw" }, enable = { } },
			{ immunityText = [[This is a party of giants that will be impossible to restrain or blow away.]], negate = { "grasping vines", "gust of wind", "swiftness", "stoneskin" }, enable = { } },
			{ immunityText = [[People in this party come from the harsh North where freezing cold is as common as lightning strikes from an angry god.]], negate = { "freeze", "lightning strike" }, enable = { } },
			{ immunityText = [[There is a wizard from the university in this party. You should use wizard duelling spells against it.]], negate = { "grasping vines", "throw", "gust of wind", "freeze", "lightning bolt", "poison", "kill", "bestow curse", "drain life" }, enable = { "absorb elements", "counterspell" } },
		},
	},
	unit = {
		evil = {
			{ immunityText = [[This is a unit of religious fanatics who cannot be tricked by spells affecting the mind.]], negate = { "mass hypnosis", "illusionary foes" }, enable = { } },
			{ immunityText = [[This unit has a skilled elementalist who can easily deal with any elemental spells.]], negate = { "gust of wind", "fireball", "lightning bolt" }, enable = { "absorb elements" } },
			{ immunityText = [[This is a unit of shambling undead husks of people long dead, moved by some twisted magic.]], negate = { "illusionary foes", "spread plague", "kill", "torture", "mass hypnosis" }, enable = { "destroy undead", "control undead", "sanctify" } }
		},
		good = {
			{ immunityText = [[This is a unit of religious fanatics who cannot be tricked by spells affecting the mind.]], negate = { "mass hypnosis", "illusionary foes" }, enable = { } },
			{ immunityText = [[This unit has a skilled elementalist who can easily deal with any elemental spells.]], negate = { "gust of wind", "fireball", "lightning bolt" }, enable = { "absorb elements" } },
			{ immunityText = [[This is a unit of mounted knights in full armor. Brute force will be of little use against them.]], negate = { "gust of wind", "enhance strength", "kill", "stoneskin" }, enable = { "magnetism", "control beasts" } },
		},
	},
	army = {
		evil = {
			{ immunityText = [[The leader of this army is a battle engineer armed with siege engines that can shoot down any flying hazards.]], negate = { "meteorite", "angel wings" }, enable = { } },
			{ immunityText = [[The leader of this army is a skilled magician who is constantly on lookout against anyone who would try to use an Armageddon spell.]], negate = { "armageddon" }, enable = { } },
			{ immunityText = [[The whole army, as well, as the leader, is undead. (They're too numerous for Destroy/Control Undead.)]], negate = { "spread plague" }, enable = { "sanctify" } },
		},
		good = {
			{ immunityText = [[The leader of this army is a battle engineer armed with siege engines that can shoot down any flying hazards.]], negate = { "meteorite", "angel wings" }, enable = { } },
			{ immunityText = [[The leader of this army is a skilled magician who is constantly on lookout against anyone who would try to use an Armageddon spell.]], negate = { "armageddon" }, enable = { } },
			{ immunityText = [[The leader of this army is a adevout paladin capable of destroying undead and demons just by a couple of words.]], negate = { "hellgate" }, enable = { } },
		},
	},
}
