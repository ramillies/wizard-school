return function (s, param)
	local race = param.enemy.race
	local level = param.enemy.level
	local negate = param.enemy.trait.negate
	local enable = param.enemy.trait.enable
	local immunityText = param.enemy.trait.immunityText
	local terrain = param.terrain
	local groupType = level <= 2 and "party" or level <= 4 and "unit" or "army"

	local effective = {
		party = { "grasping vines", "throw", "swiftness", "gust of wind", "freeze", "lightning bolt", "torture", "poison", "turn into frog", "kill", "bestow curse", "drain life" },
		unit = { "enhance strength", "lightning bolt", "fireball", "illusionary foes", "spread plague", "gust of wind", "kill", "mass hypnosis", "torture" },
		army = { "spread plague", "armageddon", "meteorite", "hellgate" },
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end

	local where = ({ "in the water", "on the beach", "in the forests", "in the hills", "in the mountains", "on the mountain peaks" })[terrain]

	function successScene(text)
		local fame, karma
		if isOneOf(race, { "Orcs", "Goblins", "Dark Elves" }) then
			karma = level >= 6 and 3 or level >= 4 and 2 or 1
		else
			karma = - (level >= 6 and 3 or level >= 4 and 2 or 1)
		end
		if karma * Player.getKarma() > 0 then
			fame = 30 * level^2 / (1 + math.abs(Player.getKarma()/level))
		else
			fame = 30 * level^2
			karma = -Player.getKarma() + karma
		end

		s:changeScene {
			background = groupType == "party" and "background battle party" or "background battle army",
			screen = {
				screenType = "MessageBox",
				title = "Victory!",
				text = string.format("%s\n\nFame earned: %d\nKarma earned %d.", text, fame, karma),
				onExit = function () s:exitScene(); Player.addFame(fame); Player.addKarma(karma); Player.addStress(-level) end
			},
			onInit = function () s:disableDistractor(); s:disableTimer() end
		}
	end
	
	function failScene(text)
		local fame = 360 / level^2
		local stress = 7*level

		s:changeScene {
			background = groupType == "party" and "background battle party" or "background battle army",
			screen = {
				screenType = "MessageBox",
				title = "Defeat!",
				text = string.format("%s\n\nFame lost: %d\nStress obtained: %d.", text, fame, stress),
				onExit = function () s:exitScene(); Player.addFame(-fame); Player.addStress(stress) end
			},
			onInit = function () s:disableDistractor(); s:disableTimer() end
		}
	end

	function failScene(text)
		local fame = 360 / level^2
		local stress = 7*level

		s:changeScene {
			background = groupType == "party" and "background battle party" or "background battle army",
			screen = {
				screenType = "MessageBox",
				title = "Defeat!",
				text = string.format("%s\n\nFame lost: %d\nStress obtained: %d.", text, fame, stress),
				onExit = function () s:exitScene(); Player.addFame(-fame); Player.addStress(stress) end
			},
			onInit = function () s:disableDistractor(); s:disableTimer() end
		}
	end

	function fleeScene(text)
		local distraction = 4*level^2
		local stress = level

		s:changeScene {
			background = groupType == "party" and "background battle party" or "background battle army",
			screen = {
				screenType = "MessageBox",
				title = "Flight!",
				text = string.format("%s\n\nDistracted the army for %d days.\nStress obtained: %d.", text, distraction, stress),
				onExit = function () s:exitScene(); param.enemy.distracted = distraction; Player.addStress(stress) end
			},
			onInit = function () s:disableDistractor(); s:disableTimer() end
		}
	end

	s:setScene {
		background = groupType == "party" and "background battle party" or "background battle army",
		screen = {
			screenType = "InputBox",
			title = "Battle",
			text = string.format([[You chose to do battle against a %s of %s %s. %s

What do you want to cast?]], groupType, race, where, immunityText),
			onSubmit = function (str)
				local a = string.lower(str)
				local spell = MagicWords.resolveSpell(a)

				if isOneOf(spell, negate) then
					failScene("Your enemies have blocked your spell and defeated you.")
				elseif isOneOf(spell, effective[groupType]) then
					successScene("Your spell proved effective and the battle is won!")
				elseif ((spell == "move mountains") and terrain >= 4) or
					((spell == "terraform") and terrain <= 1) or
					((spell == "wrath of nature") and terrain == 3) then
					successScene("Your mastery of magic and terrain is good enough to secure a victory!")
				elseif (spell == "stoneskin" and (groupType == "party" or groupType == "unit")) or
					(spell == "ghost form" and groupType == "party") or
					(spell == "absorb elements" and isOneOf("absorbElements", enabled)) or
					(spell == "sanctify" and isOneOf("absorbElements", enabled)) or
					(spell == "angel wings" and terrain ~= 3) or
					(spell == "plantgrowth" and terrain == 3) or
					(spell == "transmute rock" and terrain >= 4) or
					(spell == "freeze" and terrain == 0 and not isOneOf("freeze", effective[groupType])) then
					fleeScene("With a clever use of magic, you managed to grab the enemies' attention and vanish into thin air, distracting them greatly.")
				else
					failScene("Your spell was not effective against this type of enemy.")
				end
			end
		},
		onInit = function () s:enableTimer(12); s:enableDistractor(0.2 + level * 0.3) end,
		onTimeout = function () failScene("The enemy rushed you before you were ready and prepared you a bitter defeat.") end,
		onDistraction = function () failScene("You could not concentrate on your spells and the enemy was able to easily defeat you.") end,
	}
end
