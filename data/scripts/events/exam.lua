return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Exam period\n%s year",
		({ "First", "Second", "Third" })[params.year]))
	params.action = 1
	params.year = params.year + 1
	params.lecturesRemaining = { "spiritual", "spiritual", "practical", "practical", "primal", "primal", "dark", "dark" }
	
	s:setScene {
		background = "background exam classroom",
		screen = {
			screenType = "MessageBox",
			title = "Exam Period",
			text = "After a whole year of studying and practicing, you finally come to the exams. You learn and prepare day and night, and since you are a talented student, you pass all the exams quite easily" .. (
				params.year == 4 and ". This is your final year at the school, so you are going to be awarded your diploma." or ".\n\nNow it's high time to enjoy your three months of vacation. Of course, as it is always the case with pleasant things, the three months fly by like a flash of lightning. When the term starts and the headmaster has his usual speech to the freshmen, you feel as if the vacation did not last even a single day.\n\nHowever, the term started and now, you will go through the usual ups and downs of studying — lectures, parties, exams and so on."
			),
			onExit = function ()
				Player.addFatigue(-100)
				if params.year == 4 then
					s:exitScene()
					Events.runEvent("tutorial world", { })
				else
					s:exitScene()
					Events.runEvent("school hub", params)
				end
			end
		}
	}
end
