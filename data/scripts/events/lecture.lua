return function (s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	local spellsToTeach = { }
	for i = 1, (4 - params.state.year) do
		table.insert(spellsToTeach, table.remove(params.state.lectures[params.lectureType][params.state.year], math.random(#params.state.lectures[params.lectureType][params.state.year])))
	end

	local spellToRevise = spellsToTeach[math.random(#spellsToTeach)].spell
	local professor = loadfile("data/scripts/professors.lua")()[params.lectureType]

	local endScene = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "MessageBox",
			title = "Lecture End",
			text = professor.phrases.endLecture,
			onExit = function () s:exitScene(); Events.runEvent("school hub", params.state) end
		},
		onInit = function ()
			s:disableTimer()
			s:disableDistractor()
			Player.addFatigue(30 - 4*params.state.professorFavor[params.lectureType] - 10*Player.countFriends(function (friend) return friend.magicType == params.lectureType end))
		end
	}


	local reviseSceneOk = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "MessageBox",
			title = "Revise",
			text = professor.phrases.reviseOk(spellToRevise, params.state.professorFavor[params.lectureType]),
			onExit = function () s:changeScene(endScene) end
		},
		onInit = function ()
			s:disableDistractor()
		end
	}

	local reviseSceneFail = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "MessageBox",
			title = "Revise",
			text = professor.phrases.reviseFail(spellToRevise, params.state.professorFavor[params.lectureType]),
			onExit = function () s:changeScene(endScene) end
		},
		onInit = function ()
			s:disableDistractor()
		end
	}

	local reviseScene = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "InputBox",
			title = "Revise",
			text = professor.phrases.revise(spellToRevise, params.state.professorFavor[params.lectureType]),
			onSubmit = function (str)
				local spell = string.lower(str)
				if MagicWords.resolveSpell(spell) == spellToRevise then
					params.state.professorFavor[params.lectureType] = params.state.professorFavor[params.lectureType] + 1
					s:changeScene(reviseSceneOk)
				else
					params.state.professorFavor[params.lectureType] = params.state.professorFavor[params.lectureType] - 0.5
					s:changeScene(reviseSceneFail)
				end
			end
		},
		onInit = function () s:disableTimer() end,
		onDistraction = function () s:changeScene(fallAsleepScene) end
	}

	local fallAsleepScene = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "MessageBox",
			title = "Zzzzz...",
			text = professor.phrases.fallAsleep(params.state.professorFavor[params.lectureType]),
			onExit = function ()
				professor.fallAsleepPunishment(params.state.professorFavor[params.lectureType])
				params.state.professorFavor[params.lectureType] = params.state.professorFavor[params.lectureType] - 1
				Player.addFatigue(30 - 4*params.state.professorFavor[params.lectureType] - 10*Player.countFriends(function (friend) return friend.magicType == params.lectureType end))
				s:exitScene()
				Events.runEvent("school hub", params.state)
			end
		},
		onInit = function () s:disableTimer(); s:disableDistractor() end
	}

	local scenes = { }
	for n, v in ipairs(spellsToTeach) do
		local incantation = string.upper(MagicWords.spellToIncantation[v.spell])
		local words = { }
		for w in string.gmatch(incantation, "%a+") do
			table.insert(words, w)
		end

		local function generateLecture()
			local rv = ""
			if #words == 1 then
				rv = string.format(v.lecture, words[1])
			elseif #words == 2 then
				rv = string.format(v.lecture, words[1], words[2])
			else
				rv = string.format(v.lecture, words[1], words[2], words[3])
			end
			return rv
		end

		table.insert(scenes, {
			background = "background classroom",
			characters = { professor.icon },
			screen = {
				screenType = "MessageBox",
				title = "Lecture",
				text = generateLecture(),
				onExit = function ()
					if n < #spellsToTeach then
						s:changeScene(scenes[n+1])
					else
						if math.random() < 0.25 then
							s:changeScene(reviseScene)
						else
							s:changeScene(endScene)
						end
					end
				end
			},
			onInit = function ()
				if n == 1 then
					s:enableTimer(60)
					s:enableDistractor(0.5 + Player.getFatigue()/50)
				end
			end,
			onTimeout = function ()
				if n < #spellsToTeach then
					s:changeScene(scenes[n+1])
				else
					if math.random() < 0.15 then
						s:changeScene(reviseScene)
					else
						s:changeScene(endScene)
					end
				end
			end,
			onDistraction = function ()
				s:changeScene(fallAsleepScene)
			end
		})
	end

	local startScene = {
		background = "background classroom",
		characters = { professor.icon },
		screen = {
			screenType = "MessageBox",
			title = "Lecture Start",
			text = professor.phrases.beginLecture,
			onExit = function () s:changeScene(scenes[1]) end
		},
	}

	s:setScene(startScene)
end
