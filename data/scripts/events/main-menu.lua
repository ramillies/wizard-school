return function (s, params)
	s:setHudVisibility { gametime = false, fame = false, stressmeter = false, realtime = false }
	s:setScene {
		background = "background outside university",
		screen = {
			screenType = "ChoiceBox",
			title = "Magisterium",
			text = "\n",
			choices = {
				{ text = "New Game", callback = function () s:exitScene(); Events.runEvent("school start", {}) end },
				{ text = "Cheat and go straight to the world map", callback = function() Player.addFatigue(-100); Player.addStress(10); Game.switchToWorld(10) end },
				{ text = "Exit", callback = function () Game.quit() end }
			}
		}
	}
end
