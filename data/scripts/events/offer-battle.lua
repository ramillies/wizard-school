return function (s, param)
	local race = param.enemy.race
	local level = param.enemy.level
	local negate = param.enemy.trait.negate
	local enable = param.enemy.trait.enable
	local immunityText = param.enemy.trait.immunityText
	local terrain = param.terrain
	local groupType = level <= 2 and "party" or level <= 4 and "unit" or "army"
	local where = ({ "in the water", "on the beach", "in the forests", "in the hills", "in the mountains", "on the mountain peaks" })[terrain]

	s:setScene {
		background = groupType == "party" and "background battle party" or "background battle army",
		screen = {
			screenType = "ChoiceBox",
			title = "Battle",
			text = string.format([[You meet a %s of %s %s. %s

Do you want to attack them?]], groupType, race, where, immunityText),
			choices = {
				{ text = "Attack!", callback = function () s:exitScene(); Events.runEvent("battle", param) end },
				{ text = "No, go away.", callback = function () s:exitScene() end },
			}
		},
	}
end
