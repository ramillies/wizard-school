return function (s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.action-1, ({ "First", "Second", "Third" })[params.year]))
	
	local friend = #params.friends > 0 and table.remove(params.friends, math.random(#params.friends)) or nil

	local noFriendsLeft = {
		background = "background tavern",
		screen = {
			screenType = "MessageBox",
			title = "No New Friends",
			text = "Sadly, you haven't met any new friends on this party.\n\n(Technically, it's because you have already had the opportunity to make friends with all the people included in the game, so there may be little value in partying for you.)",
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.action == 9 then
					Events.runEvent("exam", params)
				else
					Events.runEvent("school hub", params)
				end
			end
		}
	}

	local explainEloquence = {
		background = "background tavern",
		screen = {
			screenType = "MessageBox",
			title = "Eloquence",
			text = "You earned your first point of eloquence.\n\nEloquence makes it possible to talk out of verious situations without use of magic. You can try to use it in any situation where a spell can be cast. Instead of a spell, just write TALK into the text box.\n\nAfter that, you will be presented with an upper-case text that you will need to type exactly as shown (and you will often be limited by a timer or distracted).\n\nBy the way, remember that the input is case insensitive, so you don't need to write it in all caps.", 
			onExit = function ()
				if friend ~= nil then
					s:exitScene()
					Events.runEvent(friend.partyEvent, {
						friend = friend,
						state = params
					})
				else
					s:changeScene(noFriendsLeft)
				end
			end
		}
	}

	s:setScene {
		background = "background tavern",
		screen = {
			screenType = "MessageBox",
			title = "Party",
			text = "In the student's life, there's always a place for a good party. Some beer, wine and talking always comes in handy and lets you forget the chores of attending lectures and working out homeworks. And of course, you can meet new friends at a party and you get better and talking and socializing in general.",
			onExit = function ()
				Player.addEloquence(1)
				if Player.getEloquence() == 1 then
					s:changeScene(explainEloquence)
				else
					if friend ~= nil then
						s:exitScene()
						Events.runEvent(friend.partyEvent, {
							friend = friend,
							state = params
						})
					else
						print(s, noFriendsLeft)
						s:changeScene(noFriendsLeft)
					end
				end
			end
		}
	}
end
