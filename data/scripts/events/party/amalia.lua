return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local amalia = params.friend

	local badEnding = {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Your effort was not enough to help you win. More experienced enemy players outmaneuvered you very easily and you weren't able to do anything about it. After the play, Amalia didn't blame you, since you didn't have any previous experience with the game, so you left the arena and went back to your room.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local goodEnding = {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[After the match, Amalia comes to you through the group of cheering students. “I'm sorry that I expected so little from you, but your help turned out to be invaluable, we won only because you carried us to victory. If you would like to play again, come to the practical magic lecture and we can play a few games after that.” With that, she turns and walks back to the crowd of cheering fans. You head back to your room knowing you made a new friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(amalia)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background hall",
			characters = { params.friend },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background hall",
		characters = { params.friend },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[If you can distract your opponents, your team can use the numerical advantage to win. COMMUNITY COLLEGE OF BABA YAGA? MORE LIKE A COMMUNITY COLLEGE OF BEING LAME, AM I RIGHT?]],
			onSubmit = function (str)
				if string.upper(str) == "COMMUNITY COLLEGE OF BABA YAGA? MORE LIKE A COMMUNITY COLLEGE OF BEING LAME, AM I RIGHT?" then
					s:changeScene {
						background = "background tavern",
						characters = { evelin, gareth },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[Your insults manage to get the enemy players frustrated and while their morale is low, your team uses the opportunity to its advantage and score many points, before your enemies realize what's going on.]],
							onExit = function () s:changeScene(goodEnding) end
						},
						onInit = function () s:disableTimer() s:disableDistractor() end
					}
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(16 + 4*Player.getEloquence()) end,
		onTimeout = function () s:changeScene(badEnding) end,
		onDistraction = function () s:changeScene(badEnding) end,
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	local castScene = {
		background = "background hall",
		characters = { params.friend },
		screen = {
			screenType = "InputBox",
			title = "",
			text = [[While you talk, you enter the Wizardball tournament stadium. You quickly change into Wizardball dress and wait until both teams prepare themselves. Players from the Baba Yaga's community college look like tough opponents, so if you want your team to win, you should probably use your spells to score some goals.]],
			onSubmit = function (str)
				local a = string.lower(str)
				local spell = MagicWords.resolveSpell(a)

				if isOneOf(spell, { "instant summon", "throw" }) then
					s:changeScene(positiveScene([[With your spell, you have complete control over the ball, and after a few minutes, the competition turns into a charade, since no opponent can stop you from scoring goals over and over again. Finally, after the score became 563:0 the enemy team surrendered and you became a hero of the university.]]))
				elseif isOneOf(spell, { "swiftness", "teleport", "angel wings", "spiderclimb" }) then
					s:changeScene(positiveScene([[Thanks to your superior mobility, there is nothing the enemy team can do to stop you. Whenever they get close enough to grab you, you can effortlessly get into a safe position in the blink of an eye. After the game ends, your smart positioning earned your team enough points to win with a landslide.]]))
				elseif spell == "enhance strength" then
					s:changeScene(positiveScene([[Maybe you’re not the fastest player in the field, but when you get the ball, no one can stop you. Even when the whole enemy team tries to tackle you, all they can do is just to barely slow you. This greatly shifts the balance of power into your team's favor.]]))
				elseif spell == "counterspell" then
					s:changeScene(positiveScene([[The enemy team finds that it's very hard to play the game when their every spell is countered by you. In a few minutes, they are overwhelmed by your magically enhanced team, because they haven’t had any chance of stopping them without their magic.]]))
				elseif spell == "bless" then
					s:changeScene(positiveScene([[Thanks to the spell, your team quickly gets the upper hand. Enemy players keep tripping when they get the ball and your teammates score shots even when they throw blindly. This luck is enough to turn the tide and your team is soon victorious.]]))
				elseif a == "talk" then
					s:changeScene(talkScene)
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(15); s:enableDistractor(1) end,
		onTimeout = function () s:changeScene(badEnding) end,
		onDistraction = function () s:changeScene(badEnding) end,
	}
	
	s:setScene {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Amalia Everbright",
			text = [[You drank a little too much and when you wake up, you realize that you fell asleep in the middle of the common room, where the party was held. Because you know how to drink, you wake up very fresh, but not everyone has that luxury. Next to you, you see some more drunk strugglers that are too drunk to do anything. Suddenly the doors fly open and you see Amalia Everbright, Captain of the university Wizardball team. She runs towards one of the drunks sleeping on the floor and curses when she realizes that he is too drunk to do anything. More Wizardball players start to appear and when she sees you, she whispers something to other players and in a few moments, you find yourself being carried away by a pair of very strong students. Amalia starts to explain the situation, while the whole team runs through the corridors.]],
			onExit = function ()
				s:changeScene {
					background = "background corridor dark",
					characters = { params.friend },
					screen = {
						screenType = "MessageBox",
						title = "Amalia Everbright",
						text = [["I'm sorry that I had to grab you without asking, but we are simply running out of time and you were the first person that we saw. Today we play a Wizardball tournament against Baba Yaga's community college and the guy you saw was one of our players. Without him, we won't have enough players to even qualify, so we need somebody to fill the place. Don't worry, it's safe and it won't take long. The rules of the Wizardball are simple, you simply need to get the ball inside the portal of your opponent's team, while protecting your portal.  While you can use any spells you want, you're not allowed to cast anything on your opponents or to change the layout of the room in any way."]],
						onExit = function ()
							s:changeScene(castScene)
						end
					},
					onInit = function () s:enableTimer(15) end,
					onTimeout = function () s:changeScene(castScene) end
				}
			end
		},
	}
end
