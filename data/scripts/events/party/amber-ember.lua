return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local amber = { name = "Amber Joltfist", tilenumber = 7, description = "Spiritual Magic Student" }
	local ember = { name = "Ember Joltfist", tilenumber = 5, description = "Primal Magic Student" }

	local secondBadEnding = {
		background = "background tavern",
		characters = { evelin },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[You're unable to fend off the furious attacks of the headmaster's pet. Badly burnt and wounded by its talons, you finally have to leave the office. You're only lucky that it doesn't follow you outside. However, you have no idea how you will attend lectures in such a bad shape...]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(50)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		},
		onInit = function () s:disableDistractor() end
	}

	local firstBadEnding = {
		background = "background corridor dark",
		characters = { evelin },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Whatever you do, the door remains firmly closed. Finally, you leave in resignation.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local goodEnding = {
		background = "background tavern",
		characters = { amber, ember },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[You return to the meeting place with the hat in hand. Ember and Amber are already waiting there for you and when you emerge with their wanted hat, they erupt in celebration. “Thanks, you will see the reason we wanted it so badly tomorrow! For now, let it be known that you are our friend and if you'd ever come to a spiritual or primal magics lecture, we can surely do some mischief afterward.” The next day you finally see why they wanted the hat so badly. It serves as a key to the part of the library where all tests wait to be marked, and they gave everyone an A+. Without any way of knowing which As were legitimate and which were not, the teachers were forced to let it pass, so you can rest with the knowledge that you have two good friends and even better marks.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend {
					name="Ember Joltfist",
					tilenumber = 5,
					description = "Primal Magic Student",
					magicType = "primal",
					partyEvent = "party with ember"
				}
				Player.addFriend {
					name="Amber Joltfist",
					tilenumber = 7,
					description = "Spiritual Magic Student",
					magicType = "spiritual",
					partyEvent = "party with amber"
				}
				for k, v in ipairs(params.state.friends) do
					if v.tilenumber == 5 or v.tilenumber == 7 then
						table.remove(params.state.friends, k)
						break
					end
				end
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background tavern",
			characters = { evelin, gareth },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local secondTalk = {
		background = "background corridor dark",
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You try to calm phoenix down with some clever lies. HEY, STOP THAT, I WAS SENT HERE TO GET THIS BACK TO THE HEADMASTER. WHY ELSE WOULD I BE HERE?]],
			onSubmit = function (str)
				if string.upper(str) == "HEY, STOP THAT, I WAS SENT HERE TO GET THIS BACK TO THE HEADMASTER. WHY ELSE WOULD I BE HERE?" then
					s:changeScene {
						background = "background corridor dark",
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[The phoenix stops its assault and when you finish your speech, it lets you grab the hat and leave.]],
							onExit = function () s:changeScene(goodEnding) end
						},
						onInit = function () s:disableDistractor() end
					}
				else
					s:changeScene(secondBadEnding)
				end
			end
		},
		onDistraction = function () s:changeScene(secondBadEnding) end
	}

	local secondScene = {
		background = "background lab",
		screen = {
			screenType = "InputBox",
			title = "Inside!",
			text = [[Once you get inside, you relax a bit, since you think that from now on it will be an easy ride, but that casualty is quickly turned to panic once you see a big flaming bird flying towards you. The siblings somehow forgot to mention the headmaster's pet phoenix. You will need to get rid of him or evade him somehow if you want to continue. Don't worry about killing him, he will always come back.]],
			onSubmit = function (str)
				local a = string.lower(str)
				local spell = MagicWords.resolveSpell(a)

				if isOneOf(spell, { "turn into frog", "kill", "create water", "freeze" }) then
					s:changeScene(positiveScene([[After you take down the phoenix with your spell, you know it won't be able to do you any harm in the next few hours, so you grab the hat and leave.]]))
				elseif isOneOf(spell, { "swiftness", "teleport", "instant summon" }) then
					s:changeScene(positiveScene([[Just a few moments before the talons of the phoenix burrow themselves deep into your arm, you quickly dodge, and thanks to your magic, the hat is soon in your hands. Then it just takes a few more dodges and blocks and you can leave the room the way you entered it.]]))
				elseif isOneOf(spell, { "stoneskin", "absorb elements" }) then
					s:changeScene(positiveScene([[Your spells protect you against the attacks of the phoenix, so you simply grab the hat and leave. It doesn't chase you outside the office, so you can peacefully go back to the meeting place.]]))
				elseif a == "talk" then
					s:changeScene(secondTalk)
				else
					s:changeScene(secondBadEnding)
				end
			end
		},
		onInit = function () s:enableDistractor(1.2) end,
		onDistraction = function () s:changeScene(secondBadEnding) end
	}

	local function bridgeToSecondScene(text)
		return {
			background = "background tavern",
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(secondScene) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local firstTalk = {
		background = "background corridor dark",
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You know that these doors can often be opened by the right key phrase, so you try the most common ones. PASSWORD1, PASSWORD2, 1234, WORDPASS, WORDPASS1, MERINTHEMAGNIFICENTISTHEBESTWIZARDTHATEVERLIVED.]],
			onSubmit = function (str)
				if string.upper(str) == "PASSWORD1, PASSWORD2, 1234, WORDPASS, WORDPASS1, MERINTHEMAGNIFICENTISTHEBESTWIZARDTHATEVERLIVED." then
					s:changeScene {
						background = "background corridor dark",
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[You try some of the passphrases and to your amazement, when you pronounce one of them, the door swings wide open! You can continue inside.]],
							onExit = function () s:changeScene(secondScene) end
						},
						onInit = function () s:disableTimer() end
					}
				else
					s:changeScene(firstBadEnding)
				end
			end
		},
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background tavern",
		characters = { amber, ember },
		screen = {
			screenType = "MessageBox",
			title = "Amber and Ember Joltfist",
			text = [[You drank too much and when you wake up, you see faces of two siblings you vaguely remember drinking with last night. They look at you with anticipation until the male one starts talking. “Hey, do you remember me? I'm Ember Joltfist, the student of primal magic you drank with, and this,” he motions towards the girl, “is my sister Amber. You promised us that you would help us get the headmaster's hat. You don't remember, well, there is no time to explain, all you need to know is that you promised us your help with a prank and we really need it now, so come with us.”]],
			onExit = function ()
				s:changeScene {
					background = "background corridor dark",
					characters = { amber, ember },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[They take you into the corridor where the headmaster's office is located. “Okay, we will distract the Headmaster and you will need to get inside and steal the hat, do you understand? Okay, let's do it!” They both run away so you are left alone with a magically and physically sealed door. Unless you know how to open it, you will need to use brute force either by using a strong spell or by summoning a few helpers.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if isOneOf(spell, { "fireball", "enhance strength", "throw" }) then
								s:changeScene(bridgeToSecondScene([[With the door reduced to splinters, nothing is holding you out anymore, so you enter.]]))
							elseif spell == "knock" then
								s:changeScene(bridgeToSecondScene([[The door clicks and opens before you. Without nothing holding you out, you can enter.]]))
							elseif isOneOf(spell, { "summon golems", "create undead" }) then
								s:changeScene(bridgeToSecondScene([[The door is strong, but it cannot resist the combined strength of many arms. Soon it flies open and you are free to continue.]]))
							elseif isOneOf(spell, { "ghost form", "earth meld", "transmute rock" }) then
								s:changeScene(bridgeToSecondScene([[Luckily for you, while the protective magic on the door is masterful, there is no such magic cast at the walls. Because of that, you can pass w ithout an incident.]]))
							elseif a == "talk" then
								s:changeScene(firstTalk)
							else
								s:changeScene(firstBadEnding)
							end
						end
					},
				}
			end
		},
	}
end
