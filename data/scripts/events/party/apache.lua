return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local apache = params.friend

	local badEnding = {
		background = "background tavern",
		characters = { james },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Despite your skills, you weren’t able to get the goblet so you go back to the teleportation circle empty-handed. Your way back into the university is very silent and once you are both safely back, you part ways. Maybe you didn’t make a friend today, but at least you weren’t bitten by snakes.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background tavern",
		characters = { james },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[As you leave the temple with the goblet in hand, you can see the excitement in James’s eyes. “Thanks, I wouldn’t be able to do it without you. You should come to the practical magic lecture and afterward, we can do an expedition to the nearest pub. I just hope that not all of my archeological trips will involve snakes, I freaking hate them.” After being teleported back into the school, you go your separate ways, but you know that you made a friend today.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(james)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background ruined temple",
			characters = { evelin, gareth },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background ruined temple",
		characters = { james },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You once heard about some guy who managed to live with the wolves by pretending to be one of them, you same strategy to get the goblet and then leave. SSSSSSS, I'M SSSNAKE, I SSSPEND MY TIME DOING NONSSSUSSSPICIOUSSS SSSNAKE SSSTUFF.]],
			onSubmit = function (str)
				if string.upper(str) == "SSSSSSS, I'M SSSNAKE, I SSSPEND MY TIME DOING NONSSSUSSSPICIOUSSS SSSNAKE SSSTUFF." then
					s:changeScene {
						background = "background ruined temple",
						characters = { james },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[Your superior acting leaves the snakes clueless about your true identity and they allow you to get the goblet and leave without any trouble.]],
							onExit = function () s:changeScene(goodEnding) end
						},
					}
				else
					s:changeScene(badEnding)
				end
			end
		},
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "\"Apache\" James",
			text = [[It’s quite late, so you decided that it’s time to stop partying and head into your room, but when you were leaving, a young half-elf in a long robe and leather satchel on his back grabbed your sleeve and started to pull you away from the rest of the group

“Hey man, I need your help, but where are my manners, they call me “Apache” James and I study practical magic here at the university, but my real passion is archeology. I did some archeological research around the dungeons, you see, and I found an ancient teleportation circle that is still intact. Just imagine all the artifacts we can retrieve from there! I will need your help though since I'm a pretty bad wizard.” As he continues with his explanation, you walk with him until you reach an ancient underground hall covered with dust. James goes to one of the corners, where an ornate circle is chiseled into the ground. As soon as both of you enter it, you are transported to another location.]],
			onExit = function ()
				s:changeScene {
					background = "background ruined temple",
					characters = { evelin, gareth },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[You are teleported just outside of some ancient temple. When you look around you can see remnants of the city that used to be around the temple. It was surely magnificent in its age, but now it's nothing more than piles of rubble. The temple itself is in quite good shape, so after a few minutes of looking around, you decide to enter. Once inside, you see a large overgrown room with a pedestal at its end. Once you look more carefully, you can spot an ornate goblet sitting atop of it, but you can also see that the whole room is filled with snakes. “Why did it have to be snakes,” says James, while looking at his treasure that is so close yet so far. Remember, it’s not only about getting to the pedestal, but it’s also about getting there while the snakes are not there.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)


							if isOneOf(spell, { "grasping vines", "instant summon", "magnetism" }) then
								s:changeScene(positiveScene([[You simply extend your hand towards the goblet and in a few moments it's in your hand, so you can leave this place without disturbing the snakes.]]))
							elseif spell == "control beasts" then
								s:changeScene(positiveScene([[As you cast the spell, all snakes raise their heads as one big slithering mass and when you finish your command, they move out of your way, so you can simply go get the goblet and leave.]]))
							elseif isOneOf(spell, { "wrath of nature", "plant growth" }) then
								s:changeScene(positiveScene([[You use the plants against the snakes, they resist at first, but the whole temple is full of vegetation, so snakes find themselves outmatched quite quickly. With snakes dragged to the side, you can simply get the cup and leave without any difficulty. ]]))
							elseif spell == "fireball" then
								s:changeScene(positiveScene([[The smell of burned snake flesh indicates that your spell is very effective. You let the fireball burn every visible snake and then you simply walk over their corpses and grab the goblet and leave.]]))
							elseif spell == "create feast" then
								s:changeScene(positiveScene([[You use your magical power to create large amounts of roasted meat. Snakes don't recognize the smell at first, but once they do, every single snake is piled on the feasting place, so you can simply grab the goblet and leave.]]))
							elseif spell == "transmute rock" then
								s:changeScene(positiveScene([[You use your magic to build a passageway under the temple and while under the pedestal you use it once again to get the goblet safely from the floor. With the goblet in hand, you walk away.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
				}
			end
		},
	}
end
