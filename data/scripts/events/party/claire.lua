return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local claire = params.friend

	local badEnding = {
		background = "background dark forest",
		characters = { claire },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[You can’t help in this situation, so you do the only thing you can, run. When you're halfway out of the Unallowed Forest ®, you hear sounds of fight coming from behind you, but you don't have time to turn. When you see Claire in the hallway the next day, she is alive, but you can see that the lich broke a few of her bones, so she cannot walk without help.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background dark forest",
		characters = { claire },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[You soon leave the Unallowed Forest ® behind you and once in the safety of the university, Claire turns to you. “Damm man, that was badass. He was like ‘boo’ and you were like ‘KABLAM’, you showed him how do we handle business here at the university, am I right? Anyway, you should come to dark school lectures sometimes, we can hang out in the cultist hideout afterward and do some fun stuff like a ritual sacrifice or beer pong. Anyway, I should go. It's getting late, see ya.” As you go back to your room, you can sleep with the knowledge that you made a friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(claire)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background dark forest",
			characters = { claire },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background dark forest",
		characters = { claire },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[If you can force the lich to attack you for a while, Claire will soon renew her resolve and she will take care of the lich. MIGHTY LICH? MORE LIKE LITTLE BITCH. YO SKELLY BOY, WHY DON'T YOU PICK SOMEONE YOUR SIZE?]],
			onSubmit = function (str)
				if string.upper(str) == "MIGHTY LICH? MORE LIKE LITTLE BITCH. YO SKELLY BOY, WHY DON'T YOU PICK SOMEONE YOUR SIZE?" then
					s:changeScene {
						background = "background caves",
						characters = { eleanor, scales },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[The enraged lich tries to attack you, but he is too angry to aim his blows carefully, so you can easily dodge them. After a few moments, you can see liche’s head get ripped out of his spine and when the skeletal remains fall to the ground, you can see Claire standing behind them holding the skull as a souvenir.]],
							onExit = function () s:changeScene(goodEnding) end
						},
						onInit = function () s:disableTimer(); s:disableDistractor() end
					}
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(12 + 4*Player.getEloquence()); s:enableDistractor(0.75) end,
		onTimeout = function () s:changeScene(badEnding) end
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background dark forest",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Claire Daggestabber",
			text = [[When returning from the party, you were unfortunate enough to stumble upon Fargus, the infamous caretaker at the university. He is the only one that still cares about the old rules, one of which is “Students can’t be out of bed after sundown”, and since they are still theoretically in effect, other professors have no other choice than to send all caught students into the Unallowed Forest ® in small groups, as the rules dictate. Since only you and one other student were unlucky enough to get caught by Fargus, you must go in pair.]],
			onExit = function ()
				s:changeScene {
					background = "background dark forest",
					characters = { claire },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“This is bullshit, why should I be punished for something so trivial as midnight rituals? And who even came up with these punishments? Why would you send students into the unmeasurably deadly Unallowed Forest ®? That's the best way to lose all your students to some giant ass spiders or those half-horse half-human dudes. Anyway, I'm Claire Daggerstabber, one of the dark magic students and a proud member of the cultist club. We are always looking for new members so if we survive this, you can join us.”

While you walk through the Unallowed Forest ®, suddenly a lich drunk on unicorn blood, that for some reason happened to be nearby, jumps out of the forest and starts to yell. Claire panics and falls to the ground in terror. From what you remember, Liches hate fire, so you could conjure some to scare him away, or you could clear Claire's mind from fear and she should be able to fight him herself.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if isOneOf(spell, { "soothing words", "instill bravery" }) then
								s:changeScene(positiveScene([[Claire stands up and lunges at the lich. Turns out that liches are fairly fragile and they are no match for the rugged Claire. In a few moments, the Lich is reduced to a pile of weeping bones, while happy Claire didn't even break a sweat.]]))
							elseif isOneOf(spell, { "destroy undead", "ignite", "fireball" }) then
								s:changeScene(positiveScene([[Instantly after you finish casting the spell, terrified shrieking tells you that your efforts to frighten the lich were successful. After a few minutes, Claire overcomes her fear and together you leave the forest.]]))
							elseif isOneOf(spell, { "wrath of nature", "grasping vines" }) then
								s:changeScene(positiveScene([[Trees themselves lunge at the lich and bind him with their branches, His resistance is futile and soon enough, he is bound safely away from anyone who he might hurt. After a few minutes, Claire overcomes her fear and together you leave the forest.]]))
							elseif spell == "turn into frog" then
								s:changeScene(positiveScene([[You recite the magical words and before your eyes, the powerful lich is turned into a small frog that slowly hops away, after a few minutes, Claire overcomes her fear and together you leave the forest.]]))
							elseif spell == "control beasts" then
								s:changeScene(positiveScene([[Your primal roar echoes through the forest and after you finish the spell, a dozen of giant spiders emerge from the trees and pounce at the unsuspecting lich. After the spiders are finished, all that remains from the lich is a ball of bones and silk. After a few minutes, Claire overcomes her fear and together you leave the forest.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(17) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
