return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local eleanor = params.friend
	local scales = { name = "Scales", tilenumber = 0, description = "Young Dragon" }

	local badEnding = {
		background = "background tavern",
		characters = { eleanor, scales },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Your efforts are not understood by the dragon and after a few failed attempts, you give up. Eleanor with sadness in her eyes shows you the way out of the cave. As you leave, you are quite disappointed that you weren’t able to help, but you quickly shift your focus back to studying.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background caves",
		characters = { eleanor, scales },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[Eleanor looks at Scales, who happily experiments with his newfound ability, and smiles at you. “I admit that was very clever, thanks to you, his development won’t be slowed, so he will grow into a powerful and healthy dragon one day. If you want to come here again, just find me at primal magic lectures and I will show you a secret passage that leads here, so you wouldn’t need to go through the floor again. I found out that playing games with Scales is a great way to get rid of the stress from studying, and it will help you too.” As you climb out of the underground, you know that you made a new friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(eleanor)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background caves",
			characters = { eleanor, scales },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background caves",
		characters = { eleanor, scales },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You will need to somehow communicate your idea to the dragon, even when you cannot speak like a dragon. YOU SCALES, I AM A GREAT MAGICIAN, YOU LISTEN TO ME, YOU OPEN YOUR MOUTH AND TRY THE ELEMENTS TO GO PFOOOO, OKAY?]],
			onSubmit = function (str)
				if string.upper(str) == "YOU SCALES, I AM A GREAT MAGICIAN, YOU LISTEN TO ME, YOU OPEN YOUR MOUTH AND TRY THE ELEMENTS TO GO PFOOOO, OKAY?" then
					s:changeScene {
						background = "background caves",
						characters = { eleanor, scales },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[Scales understands what you want from him and after a few attempts, he can use his breath weapon without any difficulties.]],
							onExit = function () s:changeScene(goodEnding) end
						},
						onInit = function () s:disableTimer() end
					}
				else
					s:changeScene(badEnding)
				end
			end
		}
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background caves",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Eleanor Flamefond",
			text = [[When you return from the party, you decide to go through the dungeons, because the air there is much fresher, but to your surprise, the floor tiles collapse under you and you fall into a large cavern lit by a few magical torches. When you get up, you suddenly hear the sound of footsteps, and in a few moments, you see a young dark-haired woman running towards you through the underground cavern. When you look more carefully, you can see that she’s holding a little dragon in her hands.]],
			onExit = function ()
				s:changeScene {
					background = "background tavern",
					characters = { eleanor, scales },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“Who are you and how did you get here? Well, that was a pointless question, since I can see the broken roof, anyway my name is Eleanor Flamefond and this is Scales. I found him here when he was just an egg, but since then he grew a lot, but for some reason, he still cannot breathe any elements such as fire, lightning, water, ice, and even poison. I study primal magic at the university, but I also study dragons and when I accidentally found this cave of draconic origin with a dragon egg in it, I was thrilled, but since then I realized that raising a young dragon without being a dragon yourself is quite hard. The problem is, that if he doesn’t learn how to breathe elements soon enough, he will have an even smaller chance to learn it later in life. Could you help me?”

You could use the spell to imitate the dragon’s breath, or you could always try to imitate it with burning oil, but you will either need to protect yourself from its heat, or you’ll need to throw it away faster than you would normally be able.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if isOneOf(spell, { "ignite", "fireball", "freeze", "lightning bolt", "create water", "poison" }) then
								s:changeScene(positiveScene([[You use your magic to create raw elemental energy and then try mimic breathing. Surely enough, Scales understands what you want from him and after a few attempts, he can use his breath weapon without any difficulties.]]))
							elseif isOneOf(spell, { "stoneskin", "absorb elements" }) then
								s:changeScene(positiveScene([[You take some oil into your mouth and spit it through one of the torches. Scales understands what you want from him and after a few attempts, he can use his breath weapon without any difficulties.]]))
							elseif spell == "throw" then
								s:changeScene(positiveScene([[You grab an open flask of common lamp oil and quickly launch it through the torch towards the nearest wall. A trail of oil leaking from the flask quickly ignited and created a long jet of burning oil, which looked very similar to dragon fire. Scales understands what you want from him and after a few attempts, he can use his breath weapon without any difficulties.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
				}
			end
		},
	}
end
