return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local evelin = params.friend
	local gareth = { name = "Dark Knight Gareth", tilenumber = 11, description = "Dark Magic Student" }

	local badEnding = {
		background = "background tavern",
		characters = { evelin },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Your actions didn't help much, so Evelin was quickly knocked on the ground where she was forced to defend herself until the rest of her energy run out. Finally, she had to surrender. Gareth emerges victorious and everyone starts to celebrate him. Meanwhile, Evelin goes back to her room and you don't see her again that night.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding2 = {
		background = "background tavern",
		characters = { evelin },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[“You saved me, so I guess the drinks are on me. Of course, we can go drinking again if you want, just come to the spiritual lecture and we will go afterwards. I just hope that Dark Knight Gareth won't come up with more ridiculous bets because this was probably fifth this year.” Then the party returns to normal and when it finally ends, you know that you made a friend today.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(evelin)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local goodEnding1 = {
		background = "background tavern",
		characters = { evelin, gareth },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[Gareth curses as he sees his skeletons reduced to piles of bones. He swears "I will be back" and then leaves the room, while the rest of the students cheer for Evelin. After many congratulations from her peers, she comes back to you.]],
			onExit = function () s:changeScene(goodEnding2) end
		}
	}

	local function positiveScene(text)
		return {
			background = "background tavern",
			characters = { evelin, gareth },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding1) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local function negativeScene(text)
		return {
			background = "background tavern",
			characters = { evelin, gareth },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(badEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background tavern",
		characters = { evelin, gareth },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[Skeletons need orders, so if you distract Gareth for long enough, Evelin will be able to destroy them quite easily. HEY, YOU. YES, YOU. YOUR MOMMA NEVER LOVED YOU AND YOUR FACE LOOKS WORSE THAN A TROLL’S FOOT.]],
			onSubmit = function (str)
				if string.upper(str) == "HEY, YOU. YES, YOU. YOUR MOMMA NEVER LOVED YOU AND YOUR FACE LOOKS WORSE THAN A TROLL'S FOOT." then
					s:changeScene {
						background = "background tavern",
						characters = { evelin, gareth },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[Gareth looks at you with rage in his eyes, and then he jumps at you. You try to defend yourself, but he stops because he realizes that he needs to command his skeletons, but it's too late. Evelin already destroyed both of them, because they were immobile.]],
							onExit = function () s:changeScene(goodEnding1) end
						},
						onInit = function () s:disableTimer() end
					}
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(12 + 4*Player.getEloquence()) end,
		onTimeout = function () s:changeScene(badEnding) end
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Evelin the Righteous",
			text = [[While partying, you find yourself drinking with a student wearing plate mail. You know that she is one of the paladin students who need to take spiritual magic lectures at the university to finish their studies. She laughs and drinks but then she stops and frantically looks around. Then in desperation, she turns to you.]],
			onExit = function ()
				s:changeScene {
					background = "background tavern",
					characters = { evelin, gareth },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“You there, can you help me? My name is Evelin the Righteous and I made a bet with Dark Knight Gareth, that I'm good enough to beat two of his skeletons even with a cursed sword, but I forgot that the fight is today and I'm in no shape to win it right now, can you help me?” As she speaks to you you see Dark Knight Gareth enter the room with his skeletons and the rest of the students start to form a circle.

You can either enhance her ability before the fight, or you can try to subtly manipulate the fight once it starts.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if spell == "remove curse" then
								s:changeScene(positiveScene([[You use your magic to uncurse Evelin's sword, Even when she is drunk, she can fight very precisely and when the sword does work for her instead of against her, she can destroy both skeletons very quickly.]]))
							elseif spell == "restoration" then
								s:changeScene(positiveScene([[When you finish casting, Evelin is once again sober and she jumps right into the fight. The cursed sword tries to resist her will, but with her mind clear, she doesn't need it, since her plated gauntlets can do the same job quite well. After a few seconds, the skeletons are reduced to dust.]]))
							elseif isOneOf(spell, { "enhance strength", "bless", "stoneskin", "instill bravery" }) then
								s:changeScene(positiveScene([[You enhance Evelin with your spell and she starts the fight. While it's really hard to fight with the cursed sword while drunk, your magic helped her enough to turn the tide. With her newfound strength, she can withstand the initial assault of the undead and then slowly overpower both skeletons.]]))
							elseif isOneOf(spell, {"sanctify", "destroy undead", "control undead" }) then
								s:changeScene(positiveScene([[You manipulate the fight with your magic and because of your spells, the undead creatures are not able to fight properly, so Evelin can destroy them like sitting ducks.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(15) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
