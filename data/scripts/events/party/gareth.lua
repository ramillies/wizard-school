return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local gareth = params.friend
	local evelin = { name = "Evelin the Righteous", tilenumber = 8, description = "Spiritual Magic Student" }

	local badEnding = {
		background = "background tavern",
		characters = { gareth },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Your actions didn't help much so Gareth’s skeletons are demolished when they receive no orders. Evelin emerges victorious and everyone starts to celebrate her. Gareth meanwhile goes back to his room and you don't see him again that night.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding2 = {
		background = "background tavern",
		characters = { gareth },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[“You saved me, so I guess the drinks are on me. Of course, we can go drinking again if you want, just come to the dark magic lecture and we will go afterward. I just hope that Evelin the Righteous won't come up with more ridiculous bets because this was probably fifth this year.” Then the party returns to normal and when it finally ends, you know that you made a friend today.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(gareth)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local goodEnding1 = {
		background = "background tavern",
		characters = { gareth, evelin },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[Evelin curses as she sees Gareths skeletons standing above her. She swears “I will be back” and then leaves the room, while the rest of the students cheer for Gareth. After many gratulations from his peers, he comes back to you.]],
			onExit = function () s:changeScene(goodEnding2) end
		}
	}

	local function positiveScene(text)
		return {
			background = "background tavern",
			characters = { gareth, evelin },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding1) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background tavern",
		characters = { gareth, evelin },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You will need to command the skeletons yourself if you want them to be victorious. PARRY, DODGE, RIPOSTE, EN GARDE, BLOCK, THRUST, DUCK, DO NOT LET HER TAKE BACK THE SWORD, STRIKE HER WITH A CHAIR, PIN HER DOWN.]],
			onSubmit = function (str)
				if string.upper(str) == "PARRY, DODGE, RIPOSTE, EN GARDE, BLOCK, THRUST, DUCK, DO NOT LET HER TAKE BACK THE SWORD, STRIKE HER WITH A CHAIR, PIN HER DOWN." then
					s:changeScene(goodEnding)
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(16 + 6*Player.getEloquence()) end,
		onTimeout = function () s:changeScene(badEnding) end
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Dark Knight Gareth",
			text = [[While partying, you find yourself drinking with a student wearing plate mail. You know that he is one of the dark knight students who need to take dark magic lectures at the university to finish their studies. He laughs and drinks but then he stops and frantically looks around. Then in desperation, he turns to you.]],
			onExit = function ()
				s:changeScene {
					background = "background tavern",
					characters = { gareth, evelin },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“You there, can you help me? My name is Dark Knight Gareth and I made a bet with Evelin the Righteous, that my skeletons are good enough that they beat her without any weapons, but I forgot that the fight is today and I'm in no shape to command my skeletons to win it right now, can you help me?” As he speaks to you, you see Evelin the Righteous entering the room in her plate armor and the rest of the students start to form a circle.

You could either sabotage the paladin or help the skeletons somehow.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if spell == "magnetism" then
								s:changeScene(positiveScene([[You use the power of magnetism to manipulate paladins sword. She tries to fight against it, but even with commands issued by drunken Gareth, the skeletons can make use of this opportunity and mercilessly punch Evelin until she is forced to surrender.]]))
							elseif spell == "control undead" then
								s:changeScene(positiveScene([[You were always a better necromancer than Gareth and unlike him, you can’t get drunk so easily, so you take control of his undead minions. When Evelin sees that the commander of the undead is so drunk he can barely stand, she stops paying attention and that's when you surprise her with a coordinated attack of both skeletons. Before she realizes what's going on, she is disarmed and with her sword pointing at her neck, she surrenders.]]))
							elseif isOneOf(spell, { "torture", "bestow curse", "drain life", "spread plague" }) then
								s:changeScene(positiveScene([[You sneak your way close to Evelin and then use your magic to weaken her. Since she doesn't know what's going on, she tries to fight as if everything was normal, but one strike to the hand holding a sword disarms her, and then she has no chance against two undamaged skeletons, so she surrenders.]]))
							elseif isOneOf(spell, {"gust of wind"}) then
								s:changeScene(positiveScene([[Soon after the fight starts, you cast your spell at the paladin and it knocks her out of balance. Normally it wouldn't be fatal, but since you timed it exactly when she was dodging a blow from one of the skeletons, she lost balance and fell to the ground where the skeletons pinned her until her surrender.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(15) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
