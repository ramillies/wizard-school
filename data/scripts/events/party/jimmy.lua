return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local jimmy = params.friend
	local scales = { name = "Scales", tilenumber = 0, description = "Young Dragon" }

	local badEnding = {
		background = "background bedroom",
		characters = { jimmy },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[Sadly your efforts came in vain and Jimmy is quickly discovered and hauled away. The guards give you a warning not to try anything similar in the future, but you didn't do anything illegal, so they cannot punish you.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background bedroom",
		characters = { jimmy },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[“Geez man, that was awesome, thanks. Those guards would catch me, but you saved me like bro I always knew you would be. See ya later alligator, I need to sell these drugs I stole, but when you come to the Dark magic lecture, we can chat and stuff for sure.” He runs away and soon you are alone in your room again. Maybe you broke the law today, but at least you made a friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(jimmy)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background bedroom",
			characters = { jimmy },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background bedroom",
		characters = { jimmy },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You will need to persuade the guards to leave. HELLO GOOD GUARDS, WHAT BRINGS YOU HERE? NO, I DIDN'T SEE ANYONE HERE, THANKS FOR ASKING.]],
			onSubmit = function (str)
				if string.upper(str) == "HELLO GOOD GUARDS, WHAT BRINGS YOU HERE? NO, I DIDN'T SEE ANYONE HERE, THANKS FOR ASKING." then
					s:changeScene(goodEnding)
				else
					s:changeScene(badEnding)
				end
			end
		}
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background bedroom",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Jimmy Quickfinger",
			text = [[You enjoyed the party, but none of your friends showed up, so you decided to go back to your room and study a little, but when you enter, sounds of running feet and clanking armor coming from the corridor attracts your attention. You look outside to see what is happening. Running through the corridor is Jimmy Quickfinger, a notorious student of Dark magic that is well known for stealing everything that is not nailed down. When he sees your open doors, he immediately jumps through them and starts talking.]],
			onExit = function ()
				s:changeScene {
					background = "background bedroom",
					characters = { jimmy },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“Yo bro, I know we never met but I need a big favor. Just convince the guards that you didn't see me or hide me somehow, just don't rat me out. If you help me, ill help you in the future, just please help me. You should find a way to hide him, use a spell to allow him fast escape, or make the guards stop looking, but you should not attack them, since you know that they would overwhelm you soon.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if isOneOf(spell, { "dig", "turn into frog", "earth meld" }) then
								s:changeScene(positiveScene([[You manage to hide Jimmy just seconds before the guards enter the room. They search it and ask questions, but with no evidence, they must leave, so Jimmy is free to exit his hideout.]]))
							elseif isOneOf(spell, { "swiftness", "angel wings", "ghost form", "spiderclimb" }) then
								s:changeScene(positiveScene([[Thanks to the mobility you bestowed upon Jimmy, he outmaneuvers the guards really easily. You can just watch and laugh as the heavily armored guards try to catch up with him and fail miserably each time. After an hour, they give up and leave and Jimmy returns to you.]]))
							elseif isOneOf(spell, { "command", "forget", "mass hypnosis" }) then
								s:changeScene(positiveScene([[When the guards come to your room, you simply say a few magical words and they leave in confusion. Jimmy looks at you with awe and then continues talking.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(15) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
