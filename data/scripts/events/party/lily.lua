return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	s:setScene {
		background = "background corridor dark",
		characters = { params.friend },
		screen = {
			screenType = "ChoiceBox",
			title = "Lily \"The Weirdo\" Miceland",
			text = [[As you come back from the party, sounds of laughter emitting from the nearby corridor attract your attention. Upon locating the source you see a group of popular students standing around the girl that is known as “The Weirdo”, a quiet bookworm that is often bullied for everything she does because she's easy to target. “What do you have here weirdo? More of your princess bullshit books?” Says the head bully while kicking one of her books. “No, I never read books about princesses, this one is about a half-elf diplomat that needs to save ….” is all that Lily manages to stutter until she's interrupted by one of the she-bullies “Needs to save? We are the ones in need of saving from your ugly robe,” says the bully while tearing the book to pieces supported by the laughter of her friends. Your magic and talking skills won't change much, but you can try to DEFEND the girl and maybe make a friend in the process, or you can JOIN the bullying group to train your insulting skills at the poor girl's expense and restore your stress in the process. (Seriously, do not try anything else)]],
			choices = {
				{ text = "Defend her", callback = function ()
					s:changeScene {
						background = "background corridor dark",
						characters = { params.friend },
						screen = {
							screenType = "MessageBox",
							title = "",
							text = [[When you tell the group that they should leave, your only answer is their laughter, but after you punch the two biggest bullies a few times, they quickly run away, since these pack-cowards don’t know how to handle opposition. Lily still cries, but when you repair her book she relaxes a little and soon you're talking about your favorite books like you knew each other your whole lives. “Thanks for helping me, no one did that before, you know. But if you want, it would be great to talk about more books sometimes, just come to the spiritual magic lecture and we can talk afterward as long as you want since I don't have any other friends.” You continue talking deep into the night and when you finally go to sleep, you can rest with the knowledge you have one more friend.]],
							onExit = function ()
								s:exitScene()
								Player.addFatigue(30)
								Player.addFriend(params.friend)
								if params.state.action == 9 then
									Events.runEvent("exam", params.state)
								else
									Events.runEvent("school hub", params.state)
								end
							end
						}
					}
				end },
				{ text = "Join the bullies", callback = function ()
					s:changeScene {
						background = "background corridor dark",
						characters = { params.friend },
						screen = {
							screenType = "MessageBox",
							title = "",
							text = [[“WHERE DID YOU GET THESE BOOKS? IN THE DUMPSTER?” Others start to laugh at your joke and soon you are an integral part of the group. You make fun of her for a good hour, until you get bored and go to the night looking for some more targets to bully. While your newfound friends are absolutely useless, at least you are not the one getting bullied, and even better, you're one of the cool kids now, and few ruined lives are well worth it.]],
							onExit = function ()
								s:exitScene()
								Player.addFatigue(-10)
								if params.state.action == 9 then
									Events.runEvent("exam", params.state)
								else
									Events.runEvent("school hub", params.state)
								end
							end
						}
					}
				end }
			}
		}
	}
end
