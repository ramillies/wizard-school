return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local michael = params.friend

	local badEnding = {
		background = "background tavern",
		characters = { michael },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[You try as much as you can, but the case resists all your efforts. Finally, after a few minutes of fruitless trying, Zart gives up and sits on the case while he laments about his fate. You know you cannot help him anymore, so you go back to your drink.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background tavern",
		characters = { michael },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[“Damn dude, I owe you one, you should come to a practical magic lecture sometimes, we can then go to some pub and I know more princesses I can introduce you to. Anyway, see you around, I have a concert tomorrow, so I should get some sleep.” Zart runs away and you return to your room. It was quite extraordinary party, but you can rest with the knowledge that you made a new friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(michael)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background tavern",
			characters = { michael },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding1) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background tavern",
		characters = { michael },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You will need to get the location of the key from one of the students who locked it. PLEASE, COULD YOU GIVE ME BACK THE KEY? I REALLY NEED IT. PRETTY PLEASE.]],
			onSubmit = function (str)
				if string.upper(str) == "PLEASE, COULD YOU GIVE ME BACK THE KEY? I REALLY NEED IT. PRETTY PLEASE." then
					s:changeScene {
						background = "background tavern",
						characters = { michael },
						screen = {
							screenType = "MessageBox",
							title = "Success!",
							text = [[Your silver tongue helps you again and the prankster quickly reveals the location of the key, which you use to open the case. Zart grabs the violin and runs away. When he returns after an hour, he has a smile on his face.]],
							onExit = function () s:changeScene(goodEnding) end
						},
						onInit = function () s:disableTimer() end
					}
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(12 + 4*Player.getEloquence()) end,
		onTimeout = function () s:changeScene(badEnding) end
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background tavern",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Michael Olivander Zart",
			text = [[Party is going great, beer and wine mix with sounds of chat and laughter. You are almost too tired to continue partying, but then your attention shifts to the small crowd gathered around a young black-haired man who’s trying to open a violin case that seems to be stuck. The crowd laughs at him and mocks him but after a while, they get bored and leave making you a sole spectator. In desperation, he looks around and gestures you to come closer.]],
			onExit = function ()
				s:changeScene {
					background = "background tavern",
					characters = { michael },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“You there, please help me, my name is Michael Olivander Zart and I study practical magic, but I’m also a minstrel. You see, there is a pretty young princess that studies here. We met a few months ago, and from that point, I play my violin under her windows every night, but those uncultured swines locked my violin case with some kind of magical key and I can’t open it. If I miss one night, she might start to think that I decided to play for another lady instead, so I really can’t miss it.”

You could somehow open the case or you could get a new violin for him either by getting it in the far away violin shop, or by making it yourself, but you shouldn’t use brute force unless you can repair the violin after you smash it. You could also somehow play instead of him, or you could try to find the magical key, but it would be like looking for a needle in a haystack since you don’t know where they hid it.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if isOneOf(spell, { "counterspell", "knock", "enhance strength" }) then
								s:changeScene(positiveScene([[You use your magic to simply open the case. Zart looks at you in awe and quickly grabs the violin and runs away. After an hour or so, he returns with a smile on his face.]]))
							elseif spell == "fabricate" then
								s:changeScene(positiveScene([[You use your magic to create a brand new violin from a nearby table and a few pieces of wire. Zart grabs it and tries to play a few notes and then runs away with a smile on his face. He returns in an hour.]]))
							elseif spell == "repair" then
								s:changeScene(positiveScene([[You use the nearest chair to smash the case to pieces. Zart looks horrified, but when you use your magic to repair everything he relaxes. He grabs the violin and runs away. When he returns in about an hour, he has a smile on his face.]]))
							elseif spell == "find" then
								s:changeScene(positiveScene([[You concentrate on the magical key and soon enough you detect one in one of the closest trashcans. After you spend a few moments searching it, you find the key and quickly use it to unlock the case. Zart grabs the violin and runs away. When he returns after an hour, he has a smile on his face.]]))
							elseif spell == "analyze magic" then
								s:changeScene(positiveScene([[You touch a case and run some analysis. After a few moments, you find a weakness in the structure of the spell so you use a few magical commands to dismantle it. Zart looks at you in awe and quickly grabs the violin and runs away. After an hour or so, he returns with a smile on his face.]]))
							elseif spell == "instant orchestra" then
								s:changeScene(positiveScene([[You take Zart with you to the place where he usually plays the violin, you use the spell and the sound of violin fills the air. Zart looks very happy and when you return to the party, he is immeasurably thankful.]]))
							elseif spell == "teleport" then
								s:changeScene(positiveScene([[In the few blinks, you appear before the shop of the violin maker. You persuade the owner to borrow you one of his violins if you help him clean his shop, which is not hard for a skilled magician like you. You quickly return to Zart and give him the new violin. He looks shocked, but he quickly grabs it and runs away. When he returns in about an hour, he has a smile on his face.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(20) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
