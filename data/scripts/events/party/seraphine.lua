return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.state.action-1, ({ "First", "Second", "Third" })[params.state.year]))
	
	local seraphine = params.friend

	local badEnding = {
		background = "background library",
		characters = { seraphine },
		screen = {
			screenType = "MessageBox",
			title = "Failure",
			text = [[“I'm sorry, I don't feel the inspiration. Maybe I should go back to writing romance books, it's much easier than horror.” Your attempts at horror atmosphere are not so successful, so you go back to your room.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}
	local goodEnding = {
		background = "background library",
		characters = { seraphine },
		screen = {
			screenType = "MessageBox",
			title = "Success!",
			text = [[“You scared me good, I must admit that, and it was even worth it. I have enough material to write an entire trilogy. Thanks, if you'd ever come to the primal magic lecture, we can discuss the spells and I can even borrow you my homework. Anyway, I should go to sleep, I spent more time here than I probably should have, See you.” With that, she takes her notebook and walks away. You can go to sleep knowing that you have one more friend.]],
			onExit = function ()
				s:exitScene()
				Player.addFatigue(30)
				Player.addFriend(seraphine)
				if params.state.action == 9 then
					Events.runEvent("exam", params.state)
				else
					Events.runEvent("school hub", params.state)
				end
			end
		}
	}

	local function positiveScene(text)
		return {
			background = "background library",
			characters = { seraphine },
			screen = {
				screenType = "MessageBox",
				title = "",
				text = text,
				onExit = function () s:changeScene(goodEnding) end
			},
			onInit = function () s:disableTimer(); s:disableDistractor(); end
		}
	end

	local talkScene = {
		background = "background library",
		characters = { seraphine },
		screen = {
			screenType = "InputBox",
			title = "Talk",
			text = [[You will need to act as her inspiration by imitating a vampire

AH, AH, AH, I'M THE MOST TERRIBLE VAMPIRE THAT EVER LIVED. TREMBLE IN MY PRESENCE AND FEEL MY WRATH, MORTAL. AH, AH, AH.]],
			onSubmit = function (str)
				if string.upper(str) == "AH, AH, AH, I'M THE MOST TERRIBLE VAMPIRE THAT EVER LIVED. TREMBLE IN MY PRESENCE AND FEEL MY WRATH, MORTAL. AH, AH, AH." then
					s:changeScene(goodEnding)
				else
					s:changeScene(badEnding)
				end
			end
		},
		onInit = function () s:enableTimer(16 + 6*Player.getEloquence()) end,
		onTimeout = function () s:changeScene(badEnding) end
	}

	local function isOneOf(text, tab)
		local rv = false
		for k, v in ipairs(tab) do
			if v == text then rv = true end
		end
		return rv
	end
	
	s:setScene {
		background = "background library",
		characters = { params.friend },
		screen = {
			screenType = "MessageBox",
			title = "Seraphine Charhollow",
			text = [[As you are coming back from the party, you decide to take it through the library for once and you are quite surprised that you see someone there, since very late. Before you sits a young black-haired girl that frantically writes something in her notebook. When she sees you, she motions you to come closer.]],
			onExit = function ()
				s:changeScene {
					background = "background library",
					characters = { seraphine },
					screen = {
						screenType = "InputBox",
						title = "",
						text = [[“Hi, I'm sorry to bother you, but I don't know what to do. My name is Seraphine Charhollow and I'm a student of primal magic, but my real passion is writing books. I write everything from romance to epic sagas and I’m trying to write a horror story about a young girl trapped in a dark castle, but I lack inspiration. I know that it's a weird thing to ask since a barely know you, but could you help me with making the atmosphere here scarier?”

You could try adding some scary features to the room such as blood (even yours, but you will need to heal yourself afterward) and bones or you could try to imitate a storm that always appears in horror books. Or you could even bring some monsters here, for the horror to be believable. Building a haunted castle should also work.]],
						onSubmit = function (str)
							local a = string.lower(str)
							local spell = MagicWords.resolveSpell(a)

							if spell == "cure wounds" then
								s:changeScene(positiveScene([[You cut yourself and spray the blood all over the surrounding tables. Seraphine looks very shocked but after a few moments, shock is turned into inspiration and she starts writing down notes, so you have enough time to heal yourself.]]))
							elseif isOneOf(spell, { "create water", "lightning bolt" }) then
								s:changeScene(positiveScene([[You quickly run onto a roof and cast your spell. it takes a while to time the right intervals, but after a few minutes, your spell creates an almost perfect imitation of the storm, when you return, you see that Seraphine is happy and that her notebook is filled with notes.]]))
							elseif spell == "decay" then
								s:changeScene(positiveScene([[You touch a desk where she wrote her notes and in a matter of seconds, it starts to rot until it vanishes completely. Seraphine seems very impressed and from her fast scribbling in the notebook, you know that your lesson in the temporariness of everything made its mark.]]))
							elseif spell == "instant orchestra" then
								s:changeScene(positiveScene([[You use your magic to bring the gothic orchestra to life. Long suspenseful organ passages are repeatedly interrupted by the sound of cymbals which creates a very tense atmosphere. Seraphine soon starts to write something in her notebook and when she finishes, she looks quite happy.]]))
							elseif isOneOf(spell, { "create undead", "illusionary foes" }) then
								s:changeScene(positiveScene([[Seraphine soon finds herself surrounded by creatures you made, she tries to destroy some with her spells, but then she just covers her head and screams. After some time she pokes her head out and while still in shock, she begins to write something in her notebook. Your method, while very stressful, seems to have a great effect.]]))
							elseif spell == "build fortress" then
								s:changeScene(positiveScene([[You take Seraphine to the spell training range, where any spell cast will automatically stop working after an hour, and you summon a castle for her. You make sure that the castle looks unused so it would look even scarier. After an hour or so, she returns from the castle with a book full of notes.]]))
							elseif a == "talk" then
								s:changeScene(talkScene)
							else
								s:changeScene(badEnding)
							end
						end
					},
					onInit = function () s:enableTimer(20) end,
					onTimeout = function () s:changeScene(badEnding) end
				}
			end
		},
	}
end
