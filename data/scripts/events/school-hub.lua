return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.action, ({ "First", "Second", "Third" })[params.year]))
	params.action = params.action + 1

	local lectureType = table.remove(params.lecturesRemaining, math.random(#params.lecturesRemaining))
	local magicName = ({ spiritual = "Spiritual", dark = "Dark", practical = "Practical", primal = "Primal" })[lectureType] .. " Magic"

	local forcedSleepScene = {
		background = "background bedroom",
		screen = {
			screenType = "MessageBox",
			title = "What a horrible morning!",
			text = string.format("You wake up, but you find that you're barely able to scramble out of bed. You got some sleep, but you're so horribly tired that you find it hard to even stand on your legs. Your head hurts horribly and the whole world seems to be turning over and over around you.\n\nYou heard that a lecture on %s is supposed to be held today, but you really cannot force yourself going there. And you can't enjoy a party when you can hardly keep your eyes open. With a sigh, you hit the bed once more and immediately fall into a deep slumber that restores all of your fatigue.", magicName),
			onExit = function ()
				Player.addFatigue(-100)
				params.professorFavor[lectureType] = params.professorFavor[lectureType] - 0.2
				s:exitScene()
				if params.action == 9 then
					Events.runEvent("exam", params)
				else
					Events.runEvent("school hub", params)
				end
			end
		}
	}

	local lectureString = ""

	if params.professorFavor[lectureType] > -2 then
		lectureString = "You can attend it if you want, but of course you can enjoy the \"student life\" by either going to a party, or just calling it a day and having a nice sleep and relax."
	else
		lectureString = "However, you just cannot force yourself to go there. You hate that subject and the professor seems to hate you just as much -- maybe it's because you have dozed off at the lectures, or you didn't really pay attention. Perhaps a little party or a healthy sleep would improve your moods?"
	end

	local choiceScene = {
		background = "background school corridor",
		screen = {
			screenType = "ChoiceBox",
			title = "A Day at School",
			text = string.format("You wake up to another day at school. Today, a lecture on %s is announced. %s\n\nWhat do you do?", magicName, lectureString),
			choices = {
				{
					text = string.format("Attend the lecture on %s", magicName),
					callback = function ()
						s:exitScene()
						params.professorFavor[lectureType] = params.professorFavor[lectureType] + 0.2
						Events.runEvent("lecture", {
							lectureType = lectureType,
							state = params
						})
					end,
					disabled = params.professorFavor[lectureType] <= -2
				},
				{
					text = "Go to a party",
					callback = function ()
						s:exitScene()
						params.professorFavor[lectureType] = params.professorFavor[lectureType] - 0.2
						Events.runEvent("party", params)
					end
				},
				{
					text = "Just sleep and relax",
					callback = function ()
						s:exitScene()
						params.professorFavor[lectureType] = params.professorFavor[lectureType] - 0.2
						Events.runEvent("sleep", params)
					end
				},
			}
		}
	}

	if Player.getFatigue() == 100 then
		s:setScene(forcedSleepScene)
	else
		s:setScene(choiceScene)
	end
end
