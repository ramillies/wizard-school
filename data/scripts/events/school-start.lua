return function (s, params)
	s:setHudVisibility { gametime = false, fame = false, stressmeter = false }
	local contScene = {
		background = "background university night",
		screen = {
			screenType = "ChoiceBox",
			title = "The Beginning",
			text = "Finally, in your seventeen years of age, you applied to the university and you were accepted. After a long journey, you finally arrived at the university just in time to start your first term.\n\nAll students and professors have gathered together in a huge central hall of the university. Even the headmaster, Merlin the Magnificent, is here, and he is going to give his usual speech to the freshmen.\n\nDo you want to hear the speech (go through the tutorial)?",
			choices = {
				{ text = "Yes, hear the speech", callback = function ()
					s:exitScene()
					Events.runEvent("tutorial school", {})
				end },
				{ text = "No, I want to go studying right now!", callback = function ()
					local getLectureList = loadfile("data/scripts/lectures.lua")
					s:exitScene()
					Events.runEvent("school hub", {
						year = 1,
						action = 1,
						friends = loadfile("data/scripts/friends.lua")(),
						lectures = getLectureList(),
						professorFavor = {
							dark = 0,
							practical = 0,
							spiritual = 0,
							primal = 0
						},
						lecturesRemaining = { "spiritual", "spiritual", "dark", "dark", "practical", "practical", "primal", "primal" }
					})
				end },
			}
		}

	}
	s:setScene {
		background = "background university night",
		screen = {
			screenType = "MessageBox",
			title = "The Beginning",
			text = "Even when you were born, it was clear that you were no ordinary child. And after a while, you started to show magical talent.\n\nTo develop and control your powers, you wanted to go to the famous magical university called Magisterium. Studying magic, however, is not easy, and you need to be well educated before you even apply. Because of that, you had to spend many years in normal, mundane schools.",
			onExit = function () s:changeScene(contScene) end
		}
	}
end
