return function(s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString(string.format("Lesson %d of 8\n%s year",
		params.action-1, ({ "First", "Second", "Third" })[params.year]))
	s:setScene {
		background = "background bedroom",
		screen = {
			screenType = "MessageBox",
			title = "Sleep",
			text = "Of course mastering new magic is fun (and it would be even more fun if it didn't need all the boring lectures and seminars), and parties are even better, but all of that takes its toll on you. You decide that sometimes, it's just best to have a good sleep and slack off while everyone else is working.\n\nYour fatigue will be replenished.",
			onExit = function ()
				s:exitScene()
				Player.addFatigue(-100)
				if params.action == 9 then
					Events.runEvent("exam", params)
				else
					Events.runEvent("school hub", params)
				end
			end
		}
	}
end
