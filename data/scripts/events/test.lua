return function (s, params)
	s:setHudVisibility { fame = false }
	s:setTimeString("Lesson 3 of 8\nYear 1")
	local timeoutEnding = {
		background = "background classroom",
		screen = {
			screenType = "InputBox",
			title = "End of lesson",
			text = "So, before you leave, we will revise today's lesson... Hey, student! How would you play an orchestra?",
			onSubmit = function (string)
				local spell = MagicWords.resolveSpell(string)
				if MagicWords.resolveSpell(string) == "instant orchestra" then
					s:changeScene {
						background = "background classroom",
						screen = {
							screenType = "MessageBox",
							title = "Umm... ok...",
							text = "You've been paying some attention, I guess? Or perhaps students aren't such morons as they always used to be? Hmm..."
						}
					}
				else
					s:changeScene {
						background = "background classroom",
						screen = {
							screenType = "MessageBox",
							title = "You Idiot!",
							text = "You think that you can make laughing stock of me? Did you pay attention for at least one second? Get lost!"
						}
					}
				end
			end
		},
		onInit = function () s:disableTimer(); s:disableDistractor() end
	}

	local clickEnding = {
		background = "background classroom",
		screen = {
			screenType = "MessageBox",
			title = "What?!",
			text = "You're leaving in the middle of the lesson? Where are your manners, student? Get lost!"
		},
		onInit = function () s:disableTimer(); s:disableDistractor() end
	}

	local distractedEnding = {
		background = "background classroom",
		screen = {
			screenType = "InputBox",
			title = "What?!",
			text = "Do you think you can just sleep at all the lectures or what? So you will perhaps tell us how to conjure a lightning bolt, right?",
			onSubmit = function (string)
				if MagicWords.resolveSpell(string) == "lightning bolt" then
					s:changeScene {
						background = "background classroom",
						screen = {
							screenType = "MessageBox",
							title = "Well... that's correct",
							text = "I bet some of your little friends told you anyway... And now get lost! Now!!"
						}
					}
				else
					s:changeScene {
						background = "background classroom",
						screen = {
							screenType = "MessageBox",
							title = "You Idiot!",
							text = "Get lost!"
						}
					}
				end
			end
		},
		onInit = function () s:disableTimer(); s:disableDistractor() end
	}

	s:setScene {
		background = "background classroom",
		characters = { { name = "Dracon", tilenumber = 0, description = "Professor" }, { name = "Pepa", tilenumber = 1, description = "Classmate" } },
		screen = {
			screenType = "MessageBox",
			title = "Přednáška",
			text = string.format("Good morning.\n\nYou can play an orchestra with the spell %s.\nA lightning can be made using the spell %s.", MagicWords.spellToIncantation["instant orchestra"], MagicWords.spellToIncantation["lightning bolt"]),
			onExit = function () s:changeScene(clickEnding) end
		},
		onInit = function () s:enableTimer(15); s:enableDistractor(1) end,
		onTimeout = function () s:changeScene(timeoutEnding) end,
		onDistraction = function () s:changeScene(distractedEnding) end
	}
end
