return function (s, params)
	s:setHudVisibility { gametime = false, fame = false, stressmeter = false }
	s:setTimeString("Lesson 0 of 8\nFirst year")

	local scene11 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "End of the Speech",
			text = "Okay, thanks to all the teachers for presenting their schools of magic so nicely! And now it's time to stop babbling and start studying! Off you go!",
			onExit = function()
				local getLectureList = loadfile("data/scripts/lectures.lua")
				s:exitScene()
				Events.runEvent("school hub", {
					year = 1,
					action = 1,
					friends = loadfile("data/scripts/friends.lua")(),
					lectures = getLectureList(),
					professorFavor = {
						dark = 0,
						practical = 0,
						spiritual = 0,
						primal = 0
					},
					lecturesRemaining = { "spiritual", "spiritual", "dark", "dark", "practical", "practical", "primal", "primal" }
				})
			end
		}
	}

	local scene10 = {
		background = "background hall",
		characters = { { name = "Erga Darkbane", tilenumber = 4, description = "Teacher of Dark Magic" } },
		screen = {
			screenType = "MessageBox",
			title = "Dark Magic",
			text = 'Greetings younglings, my name is Erga Darkbane and I teach the newly allowed dark magic. Since it\'s heavily stigmatized, it was banned for a long time, but in this new progressive age, everyone should be able to learn whatever they want. Dark magic is all about kil…. I mean “spreading the love”, so you should attend my lectures if you want to “spread the love” to your enemies, if you want to raise unde... ehm, friends to spread the love for you or if you want to hypnoti….. I mean “convince” everyone to do your bidding.',
			onExit = function() s:changeScene(scene11) end
		}
	}

	local scene9 = {
		background = "background hall",
		characters = { { name = "Jonah Quickfoot", tilenumber = 2, description = "Teacher of Practical Magic" } },
		screen = {
			screenType = "MessageBox",
			title = "Practical Magic",
			text = "Hey kids, you wanna be cool? If yes then you should come to my lectures, because that’s exactly what I teach there. My name is Jonah Quickfoot, but everyone calls me Tough-As-Nails because that’s exactly what I am. I teach practical magic, which is of course the best kind of magic. I will teach you stuff that will make your lives easier, by doing most of the chores for you. Just come to my lectures if you want to be the coolest magician that ever lived.",
			onExit = function() s:changeScene(scene10) end
		}
	}

	local scene8 = {
		background = "background hall",
		characters = { { name = "Eleanor Capefleet", tilenumber = 3, description = "Teacher of Primal Magic" } },
		screen = {
			screenType = "MessageBox",
			title = "Primal Magic",
			text = "Eh, hi… I mean good evening everyone, my name is Eleanor Capefleet, and I’m a doctoral student at the department of primal magic. As part of my doctorate, I need to teach lectures, so you will see me throughout your studies if you wish to study primal magic…. Ah yes, primal magic, I’m supposed to explain it to you. Simply put, it is about controlling the primal powers around us such as the elements and magic itself. It is…………. awesome and you should attend my lectures if…….. you want to learn primal magic. I’ll better go now, I still didn’t learn how to speak in front of crowds that big.",
			onExit = function() s:changeScene(scene9) end
		}
	}

	local scene7 = {
		background = "background hall",
		characters = { { name = "Brother Dorian", tilenumber = 1, description = "Teacher of Spiritual Magic" } },
		screen = {
			screenType = "MessageBox",
			title = "Spiritual Magic",
			text = "Hello everyone, I’m brother Dorian and I teach spiritual magic here at the university. Spiritual magic is all about concentrating divine or natural power to make the world a better place. I’m primarily a divine caster, but I also have a degree in druidic magic, so I will teach you both. You should attend my lectures if you want to control nature, fight evil forces and heal and support others.",
			onExit = function() s:changeScene(scene8) end
		}
	}

	local scene6 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "InputBox",
			title = "",
			text = "Here you can practise entering a spell. Sometimes you will also have a choice to do something non-magical. If there is such a choice, it will be written in the text in CAPITAL LETTERS LIKE THIS. If you want to do that instead of casting, just type the capitalized text. You should also know that the magical incantations as well as the non-magical actions are case insensitive, so you can type it in lower or upper case, as you want.\n\nNow, that will be all. I will explain the “world” part once you get to it. For now, I will leave you in the hands of teachers, who will introduce their schools of magic. To get there, just type anything you want and hit Enter.",
			onSubmit = function(str) s:changeScene(scene7) end
		},
	}

	local scene5 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "",
			text = "Partying will allow you to make a friend. Every friend studies one of the schools of magic. Attending lectures of that school will then be less tiring since everything is more fun with some friends. Parties will also raise your talking skill, which will allow you to talk yourself out of the problems, instead of solving them with magic. If you want to talk instead of casting a spell, just write TALK into the spell bar.\n\nSleeping will simply restore your tiredness. Sleeping is not something you should do unless you need to, since it doesn’t contribute anything in the long run, but it’s better than falling asleep in the lectures, so it’s all about the risk and reward.\n\nCasting spells is easy. First, learn them at the lectures, you will need just the magic words, so you can ignore the gibberish about gestures and ritual components (you will need to note what the spell you learned does though). Then when the opportunity to cast something shows itself, you will be presented with a text bar like this where you can write anything. Hit Enter when you're finished. If you write all the words needed to cast the spell (in the right order) it will be cast. Ready to try it?",
			onExit = function() s:changeScene(scene6) end
		},
		onInit = function() s:disableTimer(); s:disableDistractor() end
	}

	local scenePongLost = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "",
			text = "Ouch, I see you almost fell asleep! It's okay for now, but be sure to pay more attention at the lectures. Some of the teachers can get quite angry if you sleep while they are teaching... and by the way, if you tend to doze off often, you should give Dark Magic a miss. Some students that managed to fall asleep in that class came out in a pretty bad shape...",
			onExit = function() s:changeScene(scene5) end
		},
		onInit = function() s:disableTimer(); s:disableDistractor() end
	}

	local scene4 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "",
			text = "Move your mouse to move the paddle, and don't let the ball escape. You will need to do this while taking the notes. Speed of the ball depends on your tiredness level, so you can theoretically attend all lectures without going to sleep, but it will take serious skill.\n\nThere is also a timer on the left. When it runs out, you will just skip to the next part of the lecture. Falling asleep, however (the ball escapes) can lead to some repercussions.",
			onExit = function() s:changeScene(scene5) end,
		},
		onInit = function() s:enableDistractor(0.9); s:enableTimer(60) end,
		onTimeout = function() s:changeScene(scene5) end,
		onDistraction = function() s:changeScene(scenePongLost) end
	}

	local scene3 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "",
			text = "You can spend your time in university as you want. You can either attend the lectures, party, or sleep. Studying and partying will tire you and sleeping will restore your fatigue, you can always see your tiredness in the meter on the top. Being tired won’t kill you, but it will make listening to lectures much harder.\n\nLectures are the primary focus of the university. There are four schools of magic you can currently learn here; they will be introduced by their teachers later. For now, you just need to know that you are not limited in the type of lecture you can attend, so you can learn all spells from one, a couple spells from each, or mix and match.\n\nIn each lecture, you will learn a set number of spells. Learning itself is easy, just listen to the lecture and take notes (really, you will need to take notes with pen and paper). Every spell consists of up to three words. Just note all the words and a brief description of the spell. You will also need to fend off sleep by blocking it in the top right corner right. Ready to try it?",
			onExit = function() s:changeScene(scene4) end
		},
		onInit = function() s:setHudVisibility { stressmeter = true } end
	}

	local scene2 = {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "",
			text = "First, you will need to get through the lectures and seminars. You have a certain amount of time before the exam period (you can see that in the top left corner). After successfully finishing it, you will get into next year. When you finish the exam period in the third year, you will be free to venture into the world and use your magic as you see fit.",
			onExit = function() s:changeScene(scene3) end
		},
		onInit = function() s:setHudVisibility { gametime = true } end
	}

	s:setScene {
		background = "background hall",
		characters = { { name = "Merlin the Magnificent", tilenumber = 0, description = "Headmaster" } },
		screen = {
			screenType = "MessageBox",
			title = "Headmaster's Speech",
			text = "Hello students, my name is Merlin the Magnificent and it is my pleasure to welcome you all to our university. Here you can learn all we know about magic and how to use it to become famous magicians like me. You can do that by learning all the spells we teach here and then by using them in the world to gain FAME, but I’m getting ahead of myself.\n\nOften, you will be presented texts in boxes like this. You can just click the mouse to continue when you're done reading.",
			onExit = function() s:changeScene(scene2) end
		}
	}
end
