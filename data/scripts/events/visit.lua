return function(screen, village)
	screen:setScene {
		background = "background classroom",
		screen = {
			screenType = "MessageBox",
			title = village:getName(),
			text = "You visit the quiet village.\nAs a consequence of your visit, the village vanishes.",
			onExit = function () screen:exitScene(); village:remove() end
		}
	}
end
