return {
	{
		name="Amalia Everbright",
		tilenumber = 1,
		description = "Practical Magic Student",
		magicType = "practical",
		partyEvent = "party with amalia"
	},
	{
		name="\"Apache\" James",
		tilenumber = 2,
		description = "Practical Magic Student",
		magicType = "practical",
		partyEvent = "party with apache"
	},
	{
		name="Michael Olivander Zart",
		tilenumber = 3,
		description = "Practical Magic Student",
		magicType = "practical",
		partyEvent = "party with michael"
	},
	{
		name="Eleanor Flamefond",
		tilenumber = 4,
		description = "Primal Magic Student",
		magicType = "primal",
		partyEvent = "party with eleanor"
	},
	{
		name="Ember Joltfist",
		tilenumber = 5,
		description = "Primal Magic Student",
		magicType = "primal",
		partyEvent = "party with amber and ember"
	},
	{
		name="Seraphine Charhollow",
		tilenumber = 6,
		description = "Primal Magic Student",
		magicType = "primal",
		partyEvent = "party with seraphine"
	},
	{
		name="Amber Joltfist",
		tilenumber = 7,
		description = "Spiritual Magic Student",
		magicType = "spiritual",
		partyEvent = "party with amber and ember"
	},
	{
		name="Evelin the Righteous",
		tilenumber = 8,
		description = "Spiritual Magic Student",
		magicType = "spiritual",
		partyEvent = "party with evelin"
	},
	{
		name="Lily \"The Weirdo\" Miceland",
		tilenumber = 9,
		description = "Spiritual Magic Student",
		magicType = "spiritual",
		partyEvent = "party with lily"
	},
	{
		name="Claire Daggerstabber",
		tilenumber = 10,
		description = "Dark Magic Student",
		magicType = "dark",
		partyEvent = "party with claire"
	},
	{
		name="Dark Knight Gareth",
		tilenumber = 11,
		description = "Dark Magic Student",
		magicType = "dark",
		partyEvent = "party with gareth"
	},
	{
		name="Jimmy Quickfinger",
		tilenumber = 12,
		description = "Dark Magic Student",
		magicType = "dark",
		partyEvent = "party with jimmy"
	}
}
