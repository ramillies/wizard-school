dofile("data/scripts/city-names.lua")

local size = World:size()
local objects = size[1] == 1024 and 6 or size[1] == 2048 and 12 or size[1] == 4096 and 30
local separation = math.sqrt(size[1]*size[2] / 2 / objects / math.pi) * .85

for i = 1, objects do
	World:addObject {
		objectType = "good town",
		pos = World:randomPoint(function (pos) return World:heightLevelAt(pos) > 0 and World:distanceFromNearest(pos) > separation end),
		name = table.remove(cityNames, math.random(#cityNames)),
		level = i/objects <= .5 and 1 or i/objects <= .84 and 2 or 3
	}

	World:addObject {
		objectType = "evil lair",
		pos = World:randomPoint(function (pos) return World:heightLevelAt(pos) > 0 and World:distanceFromNearest(pos) > separation end),
		level = i/objects <= .5 and 1 or i/objects <= .84 and 2 or 3
	}
end
