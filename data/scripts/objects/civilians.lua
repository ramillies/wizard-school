return {
	init = function (self, params)
		self.from = params.from
		self.to = params.to
		self.level = 0

		local name
		if self.from.level == 1 and self.to.level == 1 then name = "Farmers"
		elseif self.from.level > 1 and self.from.level > 1 then name = "Merchants"
		elseif self.from.level == 1 then name = "Farmers"
		else name = "Merchants" end

		self.civilianType = name

		self:setSprite { tileset = "overworld characters", tilenumber = name == "Farmers" and 2 or 3 }
		self:setName(name)
		self:setType("civilians")
		self:setDescription(string.format("A group of %s going from %s to %s.", string.lower(name), self.from:getName(), self.to:getName()))

		self.fleeing = nil
	end,
	update = function (self, days)
		if self.to.invalid then
			self.to = World:nearestObject(self, function (o) return o:getType() == "good town" end)
		end

		local myPos = self:getPos()
		local nearestEvilArmy = World:nearestObject(self, function (o) return o:getType() == "evil army" end)
		if nearestEvilArmy.getPos ~= nil then
			local evilPos = nearestEvilArmy:getPos()

			if math.sqrt( (evilPos[1] - myPos[1])^2 + (evilPos[2] - myPos[2])^2 ) < 300 then
				self.fleeing = nearestEvilArmy
				self.to = World:nearestObject(self, function (o)
					local oPos = o:getPos()
					return o:getType() == "good town" and 2*o.level >= nearestEvilArmy.level
						and ((evilPos[1] - oPos[1])^2 + (evilPos[2] - oPos[2])^2) > ((myPos[1] - oPos[1])^2 + (myPos[2] - oPos[2])^2)
				end)
				if self.to.getPos ~= nil then
					self:setDescription(string.format("A group of %s fleeing to %s.", string.lower(self.civilianType), self.to:getName()))
				end
			end
		end

		if self.to.getPos ~= nil then
			local targetPos = self.to:getPos()
			local difference = { targetPos[1] - myPos[1], targetPos[2] - myPos[2] }
			local magnitude = 3 * (self.fleeing ~= nil and 2 or 1) * days / math.sqrt(difference[1]^2 + difference[2]^2)
			self:move { difference[1] * magnitude, difference[2] * magnitude }
		end
	end,
	collide = function (self, other)
		if other:getType() == "good town" and other == self.to then
			self:remove()
		end
	end,
	collidePlayer = function(self)
	end
}
