return {
	init = function (self, params)
		self.level = params.level
		self.race = params.race

		local name
		if self.level <= 2 then name = "Party"
		elseif self.level <= 4 then name = "Unit"
		else name = "Army" end

		local traits = loadfile("data/scripts/army-traits.lua")()[string.lower(name)].evil
		self.trait = traits[math.random(#traits)]
		self:setDescription(self.trait.immunityText)

		self.target = params.target

		self:setSprite { tileset = "overworld characters", tilenumber = self.race == "Orcs" and 4 or self.race == "Goblins" and 5 or 6 }
		self:setName(string.format("Evil Hunting %s", name))
		self:setType("evil army")
		self.distracted = 0
		self.playerCooldown = 0
	end,
	update = function (self, days)
		if self.distracted > 0 then
			self.distracted = math.max(0, self.distracted - days)
		else
			local speed = (12 - self.level) * days
			local pos = self:getPos()

			if self.target.invalid then
				self.target = World:nearestObject(self, function (o) return o:getType() == "evil lair" end)
			end

			if self.target.getPos ~= nil then
				local targetPos = self.target:getPos()
				local diff = { targetPos[1] - pos[1], targetPos[2] - pos[2] }
				local magnitude = speed / math.sqrt(diff[1]^2 + diff[2]^2)
				self:move { diff[1] * magnitude, diff[2] * magnitude }
			end

			if self.playerCooldown > 0 then
				self.playerCooldown = self.playerCooldown - days
			end
		end
	end,
	collide = function (self, other)
		if self.distracted == 0 then
			if other:getType() == "evil lair" and other == self.target then
				self:remove()
			elseif other:getType() == "civilians" or other:getType() == "good army" then
				if (self.level + 3*math.random()) >= (other.level + 3*math.random()) then
					other:remove()
				else
					self:remove()
				end
			end
		end
	end,
	collidePlayer = function(self)
		if self.distracted == 0 and self.playerCooldown == 0 then
			Events.runEvent("offer battle", { enemy = self, terrain = World:heightLevelAt(self:getPos()) })
		end
		self.playerCooldown = 5
	end
}
