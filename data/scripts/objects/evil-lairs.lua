return {
	init = function (self, params)
		self.level = params.level
		self.race = ({ "Goblins", "Orcs", "Dark Elves" })[math.random(3)]

		local adjective = self.race == "Goblins" and "Goblin" or self.race == "Orcs" and "Orc" or "Dark Elf"
		self:setSprite { tileset = "large buildings", tilenumber = ({ 9, 7, 8 })[params.level] }
		self:setType("evil lair");
		self:setName(string.format("%s Lair level %d", adjective, params.level))

		self.countdownNextArmy = 14 + 28*math.random()
		self.canHunt = true
		self.besiegers = { }
	end,
	update = function (self, days)
		if #self.besiegers >= 3 then
			self:remove()
			for k, v in ipairs(self.besiegers) do
				if k <= 3 then v:remove() end
			end
		else
			self.besiegers = { }
			self.countdownNextArmy = self.countdownNextArmy - days
			if self.countdownNextArmy <= 0 then
				if math.random() < 0.1 then
					local armyLevel = math.random(1, 2*self.level)
					if math.random() < 0.6 then
						World:addObject {
							objectType = "evil raider",
							pos = self:getPos(),
							level = armyLevel,
							race = self.race
						}
					else
						World:addObject {
							objectType = "evil patrol",
							pos = self:getPos(),
							level = armyLevel,
							race = self.race,
							patrolRadius = 250 * armyLevel
						}
					end
					self.countdownNextArmy = 11 * (1+math.random()) * armyLevel
					self.canHunt = true
				else
					self.countdownNextArmy = 14 + 14*math.random()
				end
			end
		end
	end,
	collide = function (self, other)
		local minLevelToAttack = self.level == 1 and 1 or self.level == 2 and 3 or 5
		if other:getType() == "good army" and other.level >= minLevelToAttack then
			table.insert(self.besiegers, other)
		end
		if other:getType() == "evil army" and other.fleeing ~= nil and self.canHunt then
			other:remove()
			World:addObject {
				objectType = "evil hunter",
				pos = self:getPos(),
				level = 2*self.level+1,
				race = self.race,
				target = other.fleeing
			}
			self.countdownNextArmy = 14 * (1+math.random()) * (2*self.level + 1)
			self.canHunt = false
		end
	end,
	collidePlayer = function(self)
	end
}
