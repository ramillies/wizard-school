return {
	init = function (self, params)
		self.level = params.level
		self.race = params.race

		local name
		if self.level <= 2 then name = "Party"
		elseif self.level <= 4 then name = "Unit"
		else name = "Army" end

		self.patrolCenter = params.pos
		self.patrolRadius = params.patrolRadius

		self:setSprite { tileset = "overworld characters", tilenumber = self.race == "Orcs" and 4 or self.race == "Goblins" and 5 or 6 }
		self:setName(string.format("Evil Patrol %s", name))
		self:setType("evil army")
		self.pursuing = nil
		self.fleeing = nil
	end,
	update = function (self, days)
		local speed = (9 - self.level) * days
		local pos = self:getPos()
		if self.pursuing == nil then
			local vecFromCenter = { pos[1] - self.patrolCenter[1], pos[2] - self.patrolCenter[2] }
			local distFromCenter = math.sqrt(vecFromCenter[1]^2 + vecFromCenter[2]^2)

			if distFromCenter < 16 then
				local x = math.random()*2*math.pi
				self:move { speed*math.cos(x), speed*math.sin(x) }
			elseif distFromCenter < (self.patrolRadius-32) then
				self:move { speed*vecFromCenter[1]/distFromCenter, speed*vecFromCenter[2]/distFromCenter }
			elseif distFromCenter > (self.patrolRadius+32) then
				self:move { -speed*vecFromCenter[1]/distFromCenter, -speed*vecFromCenter[2]/distFromCenter }
			else
				self:move { speed*vecFromCenter[2]/distFromCenter, -speed*vecFromCenter[1]/distFromCenter }
			end

			local nearestGoodArmy = World:nearestObject(self, function (o) return o:getType() == "good  army" or o:getType() == "civilians" end)
			if nearestGoodArmy.getPos ~= nil then
				local evilPos = nearestGoodArmy:getPos()

				if math.sqrt( (evilPos[1] - pos[1])^2 + (evilPos[2] - pos[2])^2 ) < 500 then
					if nearestGoodArmy.level <= self.level+1 then
						self.pursuing = nearestGoodArmy
					else
						self.fleeing = nearestGoodArmy
						local bestTown = World:nearestObject(self, function (o)
							local oPos = o:getPos()
							return o:getType() == "evil town" and 2*o.level >= nearestGoodArmy.level
								and ((evilPos[1] - oPos[1])^2 + (evilPos[2] - oPos[2])^2) > ((pos[1] - oPos[1])^2 + (pos[2] - oPos[2])^2)
						end)
						if bestTown.getPos == nil then
							self.fleeing = nil
							self.pursuing = nearestGoodArmy
						else
							self.pursuing = bestTown
						end
					end
				end
			end
		else
			if self.pursuing.invalid then
				self.pursuing = nil
			else
				local targetPos = self.pursuing:getPos()
				local diff = { targetPos[1] - pos[1], targetPos[2] - pos[2] }
				local magnitude = speed / math.sqrt(diff[1]^2 + diff[2]^2)
				self:move { diff[1] * magnitude, diff[2] * magnitude }
			end
		end
	end,
	collide = function (self, other)
		if other:getType() == "civilians" or other:getType() == "good army" then
			if (self.level + 3*math.random()) >= (other.level + 3*math.random()) then
				other:remove()
			else
				self:remove()
			end
		end
	end,
	collidePlayer = function(self)
	end
}
