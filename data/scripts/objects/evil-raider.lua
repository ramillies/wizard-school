return {
	init = function (self, params)
		self.level = params.level
		self.race = params.race

		local name
		if self.level <= 2 then name = "Party"
		elseif self.level <= 4 then name = "Unit"
		else name = "Army" end

		local traits = loadfile("data/scripts/army-traits.lua")()[string.lower(name)].evil
		self.trait = traits[math.random(#traits)]
		self:setDescription(self.trait.immunityText)

		self:setSprite { tileset = "overworld characters", tilenumber = self.race == "Orcs" and 4 or self.race == "Goblins" and 5 or 6 }
		self:setName(string.format("Evil Raiding %s", name))
		self:setType("evil army")
		self.siege = false
		self.distracted = 0
		self.playerCooldown = 0
	end,
	update = function (self, days)
		if self.distracted > 0 then
			self.distracted = math.max(0, self.distracted - days)
		elseif not self.siege then
			local maxLevelToAttack = self.level <=0 and 0 or self.level <=2 and 1 or self.level <= 4 and 2 or 3
			local attackableTowns = World:listObjects(function (obj) return (obj:getType() == "good town" and obj.level <= maxLevelToAttack) end)
			local maxLevel = 0
			for k, v in pairs(attackableTowns) do
				if v.level > maxLevel then maxLevel = v.level end
			end

			local nearestTarget = World:nearestObject(self, function (obj)
				return (obj:getType() == "good town" and obj.level == maxLevel) or
					obj:getType() == "civilians" or
					(obj:getType() == "good army" and obj.level <= self.level - 2)
			end)

			if nearestTarget.getPos ~= nil then
				local targetPos = nearestTarget:getPos()
				local myPos = self:getPos()
				local difference = { targetPos[1] - myPos[1], targetPos[2] - myPos[2] }
				local magnitude = (15 - 2*self.level) * days / math.sqrt(difference[1]^2 + difference[2]^2)
				self:move { difference[1] * magnitude, difference[2] * magnitude }
			end

			if self.playerCooldown > 0 then
				self.playerCooldown = math.max(0, self.playerCooldown - days)
			end
		end
	end,
	collide = function (self, other)
		if self.distracted == 0 then
			local maxLevelToAttack = self.level <=0 and 0 or self.level <=2 and 1 or self.level <= 4 and 2 or 3
			if other:getType() == "good town" and other.level <= maxLevelToAttack then
				self.siege = true
			elseif other:getType() == "civilians" or other:getType() == "good army" then
				if (self.level + 3*math.random()) >= (other.level + 3*math.random()) then
					other:remove()
				else
					self:remove()
				end
			end
		end
	end,
	collidePlayer = function(self)
		if self.distracted == 0 and self.playerCooldown == 0 then
			Events.runEvent("offer battle", { enemy = self, terrain = World:heightLevelAt(self:getPos()) })
		end
		self.playerCooldown = 5
	end
}
