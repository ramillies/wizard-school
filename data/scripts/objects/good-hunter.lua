return {
	init = function (self, params)
		self.level = params.level
		self.race = params.race

		local name
		if self.level <= 2 then name = "Party"
		elseif self.level <= 4 then name = "Unit"
		else name = "Army" end

		self.target = params.target

		self:setSprite { tileset = "overworld characters", tilenumber = self.race == "Humans" and 7 or self.race == "Elves" and 8 or 9 }
		self:setName(string.format("Good Hunting %s", name))
		self:setType("good army")
	end,
	update = function (self, days)
		local speed = (12 - self.level) * days
		local pos = self:getPos()

		if self.target.invalid then
			self.target = World:nearestObject(self, function (o) return o:getType() == "good town" end)
		end

		if self.target.getPos ~= nil then
			local targetPos = self.target:getPos()
			local diff = { targetPos[1] - pos[1], targetPos[2] - pos[2] }
			local magnitude = speed / math.sqrt(diff[1]^2 + diff[2]^2)
			self:move { diff[1] * magnitude, diff[2] * magnitude }
		end
	end,
	collide = function (self, other)
		if other:getType() == "good town" and other == self.target then
			self:remove()
		end
	end,
	collidePlayer = function(self)
	end
}
