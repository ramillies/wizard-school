return {
	init = function (self, params)
		self.level = params.level
		self.race = params.race

		local name
		if self.level <= 2 then name = "Party"
		elseif self.level <= 4 then name = "Unit"
		else name = "Army" end

		self:setSprite { tileset = "overworld characters", tilenumber = self.race == "Humans" and 7 or self.race == "Elves" and 8 or 9 }
		self:setName(string.format("Good Raiding %s", name))
		self:setType("good army")
		self.siege = nil
	end,
	update = function (self, days)
		if self.siege ~= nil and self.siege.invalid then
			self.siege = nil
		end
		if self.siege == nil then
			local maxLevelToAttack = self.level <=0 and 0 or self.level <=2 and 1 or self.level <= 4 and 2 or 3
			local attackableTowns = World:listObjects(function (obj) return (obj:getType() == "evil lair" and obj.level <= maxLevelToAttack) end)
			local maxLevel = 0
			for k, v in pairs(attackableTowns) do
				if v.level > maxLevel then maxLevel = v.level end
			end

			local nearestTarget = World:nearestObject(self, function (obj)
				return (obj:getType() == "evil lair" and obj.level == maxLevel) or
					(obj:getType() == "evil army" and obj.level <= self.level - 2)
			end)

			if nearestTarget.getPos ~= nil then
				local targetPos = nearestTarget:getPos()
				local myPos = self:getPos()
				local difference = { targetPos[1] - myPos[1], targetPos[2] - myPos[2] }
				local magnitude = (13 - 2*self.level) * days / math.sqrt(difference[1]^2 + difference[2]^2)
				self:move { difference[1] * magnitude, difference[2] * magnitude }
			end
		end
	end,
	collide = function (self, other)
		local maxLevelToAttack = self.level <=0 and 0 or self.level <=2 and 1 or self.level <= 4 and 2 or 3
		if other:getType() == "evil lair" and other.level <= maxLevelToAttack then
			self.siege = other
		end
	end,
	collidePlayer = function(self)
	end
}
