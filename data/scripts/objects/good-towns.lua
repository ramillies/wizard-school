return {
	init = function (self, params)
		self.level = params.level
		self:setSprite { tileset = "large buildings", tilenumber = ({ math.random(0,2), math.random(3,4), math.random(5,6) })[params.level] }
		if params.name ~= nil then
			self:setName(params.name);
		end
		self:setType("good town");

		self.race = ({ "Humans", "Dwarves", "Elves" })[math.random(3)]

		local adjective = self.race == "Humans" and "Human" or self.race == "Dwarves" and "Dwarven" or "Elven"

		self.countdownNextCivilian = 14 + 28*math.random()
		self.countdownNextArmy = 14 + 28*math.random()
		self.canHunt = true
		self.besiegers = { }
	end,
	update = function (self, days)
		if #self.besiegers >= 3 then
			self:remove()
			for k, v in ipairs(self.besiegers) do
				if k <= 3 then v:remove() end
			end
		else
			self.besiegers = { }
			self.countdownNextCivilian = self.countdownNextCivilian - days
			self.countdownNextArmy = self.countdownNextArmy - days
			if self.countdownNextCivilian <= 0 then
				if math.random() < 0.2 then
					local otherTowns = World:listObjects(function (o) return o:getType() == "good town" and o:getName() ~= self:getName() end)
					if #otherTowns > 0 then
						World:addObject {
							objectType = "civilians",
							pos = self:getPos(),
							from = self,
							to = otherTowns[math.random(#otherTowns)]
						}
					end
				end
				self.countdownNextCivilian = 14 + 28*math.random()
			end
			if self.countdownNextArmy <= 0 then
				if math.random() < 0.1 then
					local armyLevel = math.random(1, 2*self.level)
					if math.random() < 0.5 then
						World:addObject {
							objectType = "good patrol",
							pos = self:getPos(),
							level = armyLevel,
							race = self.race,
							patrolRadius = 250 * armyLevel
						}
					else
						World:addObject {
							objectType = "good raider",
							pos = self:getPos(),
							level = armyLevel,
							race = self.race,
						}
					end
					self.countdownNextArmy = 14 * (1+math.random()) * armyLevel
					self.canHunt = true
				else
					self.countdownNextArmy = 14 + 14*math.random()
				end
			end
		end
	end,
	collide = function (self, other)
		local minLevelToAttack = self.level == 1 and 1 or self.level == 2 and 3 or 5
		if other:getType() == "evil army" and other.level >= minLevelToAttack then
			table.insert(self.besiegers, other)
		end
		if (other:getType() == "civilians" or other:getType() == "good army") and other.fleeing ~= nil and self.canHunt then
			other:remove()
			if other:getType() == "good army" or math.random() < 0.1 then
				World:addObject {
					objectType = "good hunter",
					pos = self:getPos(),
					level = 2*self.level+1,
					race = self.race,
					target = other.fleeing
				}
				self.countdownNextArmy = 14 * (1+math.random()) * (2*self.level + 1)
				self.canHunt = false
			end
		end
	end,
	collidePlayer = function(self)
	end
}
