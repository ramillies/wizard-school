return {
	init = function (self, params)
		self:setSprite { tileset = "overworld characters", tilenumber = 1 }
		self:setName("Spider");
		self:setType("monster");
		self:setDescription("An evil spider that destroys villages.");
	end,
	update = function (self, days)
		local nearestVillage = World:nearestObject(self, function (obj) return obj:getType() == "village" end)
		if nearestVillage.getPos ~= nil then
			local villagePos = nearestVillage:getPos()
			local myPos = self:getPos()
			local difference = { villagePos[1] - myPos[1], villagePos[2] - myPos[2] }
			local magnitude = 20 * days / math.sqrt(difference[1]^2 + difference[2]^2)
			self:move { difference[1] * magnitude, difference[2] * magnitude }
		end
	end,
	collide = function (self, other)
		if other:getType() == "village" then
			other:remove()
		end
	end,
	collidePlayer = function(self)
		self:remove()
	end
}

