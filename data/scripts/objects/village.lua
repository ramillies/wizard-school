return {
	init = function (self, params)
		self:setSprite { tileset = "large buildings", tilenumber = math.random(0, 3) }
		if params.name ~= nil then
			self:setName(params.name);
		end
		self:setType("village");
		self:setDescription("A quiet village.");
	end,
	update = function (self, days)
	end,
	collide = function (self, other)
	end,
	collidePlayer = function(self)
		Events.runEvent("visit village", self);
	end
}
