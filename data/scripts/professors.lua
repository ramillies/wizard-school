return {
	spiritual = {
		icon = { name = "Brother Dorian", tilenumber = 13, description = "Teacher of Spiritual Magic" },
		phrases = {
			beginLecture = "Let's start the lecture.",
			endLecture = "Now we must stop. See you on the next lecture!",
			revise = function (spell, favor)
				return string.format("Let's revise. How would you cast the %s spell?", spell)
			end,
			reviseOk = function (spell, favor)
				return "Splendid!"
			end,
			reviseFail = function (spell, favor)
				return string.format("What?! Have you been paying attention? I said it was %s like million times!", string.upper(MagicWords.spellToIncantation[spell]))
			end,
			fallAsleep = function (favor)
				return "What?! You fell asleep? Get lost!"
			end
		},
		fallAsleepPunishment = function (favor)
		end
	},
	primal = {
		icon = { name = "Eleanor Capefleet", tilenumber = 14, description = "Teacher of Primal Magic" },
		phrases = {
			beginLecture = "Let's start the lecture.",
			endLecture = "Now we must stop. See you on the next lecture!",
			revise = function (spell, favor)
				return string.format("Let's revise. How would you cast the %s spell?", spell)
			end,
			reviseOk = function (spell, favor)
				return "Splendid!"
			end,
			reviseFail = function (spell, favor)
				return string.format("What?! Have you been paying attention? I said it was %s like million times!", string.upper(MagicWords.spellToIncantation[spell]))
			end,
			fallAsleep = function (favor)
				return "What?! You fell asleep? Get lost!"
			end
		},
		fallAsleepPunishment = function (favor)
		end
	},
	practical = {
		icon = { name = "Jonah Quickfoot", tilenumber = 16, description = "Teacher of Practical Magic" },
		phrases = {
			beginLecture = "Let's start the lecture.",
			endLecture = "Now we must stop. See you on the next lecture!",
			revise = function (spell, favor)
				return string.format("Let's revise. How would you cast the %s spell?", spell)
			end,
			reviseOk = function (spell, favor)
				return "Splendid!"
			end,
			reviseFail = function (spell, favor)
				return string.format("What?! Have you been paying attention? I said it was %s like million times!", string.upper(MagicWords.spellToIncantation[spell]))
			end,
			fallAsleep = function (favor)
				return "What?! You fell asleep? Get lost!"
			end
		},
		fallAsleepPunishment = function (favor)
		end
	},
	dark = {
		icon = { name = "Erga Darkbane", tilenumber = 15, description = "Teacher of Dark Magic" },
		phrases = {
			beginLecture = "Let's start the lecture.",
			endLecture = "Now we must stop. See you on the next lecture!",
			revise = function (spell, favor)
				return string.format("Let's revise. How would you cast the %s spell?", spell)
			end,
			reviseOk = function (spell, favor)
				return "Splendid!"
			end,
			reviseFail = function (spell, favor)
				return string.format("What?! Have you been paying attention? I said it was %s like million times!", string.upper(MagicWords.spellToIncantation[spell]))
			end,
			fallAsleep = function (favor)
				return "What?! You fell asleep? Get lost!"
			end
		},
		fallAsleepPunishment = function (favor)
		end
	},
}
