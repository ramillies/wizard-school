return {
	init = function (self, params)
		self.nextSpider = Player.getDay() + 28
		self.fires = 3
	end,
	fire = function (self, days)
		if Player.getDay() > self.nextSpider then
			World:addObject {
				objectType = "spider",
				pos = World:randomPoint(function (pos) return true end),
			}
			self.nextSpider = Player.getDay() + 28
			self.fires = self.fires - 1
			if self.fires == 0 then
				self:remove()
			end
		end
	end
}
