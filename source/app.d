import std.stdio;
import std.array;
import std.algorithm;
import std.range;
import std.format;

import bindbc.sfml;
import luad.all;

import magicwords;
import resources;
import coolsprite, reacttext;
import mainloop;
import messagebox;
import inputbox;
import choicebox;
import scenescreen;
import lua;
import event;
import world;
import gamescreen;

import sfml;

void main()
{
	ConfigFiles.load;
	Fonts.load;
	Images.load;
	MagicWords.generate;
	Lua.init;
	Events.load;

	RenderWindow win = new RenderWindow(VideoMode.desktopMode, "Magisterium", WindowStyle.None);
	win.fpsLimit = 60;
	win.cursorGrabbed = true;
	win.cursorVisible = false;

	Mainloop.init(win);
	Events.runEvent("main menu");
	/*
	World w = new World();
	w.generateMap(9);
	Mainloop.changeScreen(new GameScreen(w));
	*/
	Mainloop.mainloop;
}
