import std.conv;

import resources;

import boilerplate;
import sfml;
import bindbc.sfml;

class CoolSprite: Sprite
{
	private
	{
		@Read uint _tilenumber;
		string _texturename;
	}
	Vec2 tileSize, fullSize;

	mixin(GenerateFieldAccessors);

	this() { super(); }

	@property void textureName(string name)
	{
		_texturename = name;
		this.texture = Images.texture(name);
		fullSize = Images.texture(name).size;
		tileSize = Images.tileSize(name);
	}
	@property string textureName() { return _texturename; }

	@property void tilenumber(int x)
	{
		auto tilecount = Images.tileCount(_texturename);
		_tilenumber = to!uint((x % tilecount + tilecount) % tilecount); // we need this to work around the fact
		// that modulus of a negative number is negative.

		int tx = _tilenumber % (fullSize.x.to!int / tileSize.x.to!int);
		int ty = _tilenumber / (fullSize.x.to!int / tileSize.x.to!int);

		this.textureRect = Rect(tx*tileSize.x, ty*tileSize.y, tileSize.x, tileSize.y);
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		this.updateOrigin;
		super.draw(target, states);
	}
}
