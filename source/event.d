import std.algorithm;
import std.range;
import std.array;
import std.string;
import std.json;
import std.stdio;
import std.format;

import luad.all;

import resources;
import mainloop;
import messagebox;
import lua;
import scenescreen;

class Events
{
	private static JSONValue[string] events;
	static void load()
	{
		events = ConfigFiles.get("events");
	}

	static void runEvent(string eventName, LuaTable params = Lua.makeTable)
	{
		if(eventName !in events)
		{
			Mainloop.pushScreen(new MessageBox("Error!", "Attempted to run the event '" ~ eventName ~ "' but it could not be found.", delegate(){}));
			return;
		}
		
		SceneScreen s = new SceneScreen;
		auto makeEventScene = Lua.loadfile("data/scripts/events/" ~ events[eventName]["scriptFile"].str).call!(void delegate(LuaTable, LuaTable));
		makeEventScene(s.toLuaTable, params);
		Mainloop.pushScreen(s);
	}

	static void luaPutInto(LuaTable t)
	{
		t["runEvent"] = (string name, LuaTable params = Lua.makeTable) => runEvent(name, params);
	}
}
