import charactericon;
import lua;
import luad.all;

struct Friend
{
	CharacterIcon icon;
	string magicType;
	
	LuaTable toLuaTable()
	{
		LuaTable t = Lua.makeTable;
		t["name"] = icon.name;
		t["tilenumber"] = icon.tilenumber;
		t["magicType"] = magicType;

		return t;
	}
}
