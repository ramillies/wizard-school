import std.array;
import std.algorithm;
import std.range;

import luad.all;

import magicwords;
import mainloop;
import player;
import event;
import world;
import gamescreen;

class Lua
{
	static LuaState lua;

	static void init()
	{
		lua = new LuaState;
		lua.openLibs;

		lua.doString(`math.randomseed(os.time())`);
		lua.doString(`
			MagicWords = { }
			World = { }
			Player = { }
			Events = { }
			Game = { }
		`);
		MagicWords.luaPutInto(lua.get!LuaTable("MagicWords"));
		Player.luaPutInto(lua.get!LuaTable("Player"));
		Events.luaPutInto(lua.get!LuaTable("Events"));

		populateGameTable();
	}

	private static void populateGameTable()
	{
		LuaTable game = lua.get!LuaTable("Game");
		game["quit"] = delegate void() { Mainloop.quit; };
		game["popAllScreens"] = delegate void() { Mainloop.popAll; };
		game["switchToWorld"] = delegate void(int size)
		{
			World w = new World();
			w.generateMap(size);
			Mainloop.changeScreen(new GameScreen(w));
		};
	}

	static void dostring(string code) { lua.doString(code); }
	static void dofile(string path) { lua.doFile(path); }
	static LuaFunction loadstring(string code) { return lua.loadString(code); }
	static LuaFunction loadfile(string path) { return lua.loadFile(path); }
	static LuaTable makeTable() { return lua.loadString(`return {}`).call!LuaTable(); }
	static void debugGlobalTable()
	{
		lua.doString(`for k, v in pairs(_G) do print(k, v) end`);
	}

}
