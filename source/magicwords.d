import std.array;
import std.algorithm;
import std.range;
import std.exception;
import std.stdio;
import std.json;
import std.random;
import std.string;

import luad.all;

import resources;

class MagicWords
{
	static string[string] spellToIncantation, incantationToSpell;

	static void generate()
	{
		auto wordTable = ConfigFiles.get("magic words");
		File f;
		try
		{
			f = File("data/magic-words-origin", "r");
		}
		catch(std.exception.ErrnoException e)
		{
			writefln("ERROR! Could not open the magical words text ('data/magic-words-origin').");
			throw e;
		}
		string magicOriginText = f.byLine.join(" ").idup;

		int[char][string] frequencyTable;
		enum lookahead = 3;

		for(int i = 0; i < magicOriginText.length - lookahead - 1; i++)
		{
			auto key = magicOriginText[i .. i+lookahead];
			auto val = magicOriginText[i+lookahead];
			if(key in frequencyTable)
				frequencyTable[key][val] = frequencyTable[key].get(val, 0) + 1;
			else
				frequencyTable[key] = [ val : 1 ];
		}

		char[] result = frequencyTable.keys.choice.dup;

		while(result.length < 100000)
		{
			auto last = result[$-lookahead .. $];
			if(last in frequencyTable)
			{
				auto probabilities = frequencyTable[last];
				result ~= probabilities.keys[dice(probabilities.values)];
			}
			else
				result ~= frequencyTable.keys.choice;
		}

		string[] randomWords = result.split.filter!`a.length >= 6 && a.length <= 11`.map!`a.idup`.array;
		randomWords.sort;
		randomWords = randomWords.uniq.array.randomSample(3*wordTable.length).array.randomShuffle.array;
		int wordIndex = 0;

		foreach(word, data; wordTable)
		{
			int level = data["level"].get!int;
			string incantation = randomWords[wordIndex .. wordIndex + level].join(" ").idup;
			wordIndex += level;

			incantationToSpell[incantation] = word;
			spellToIncantation[word] = incantation;
		}

//		writefln("Magic words:\n%-(\t%-20s%-30s\n%)", spellToIncantation);
	}

	static string resolveSpell(string incantation)
	{
		char[] incantationCopy = incantation.dup;
		if(incantationCopy.skipOver("cheat:"))
			return incantationCopy.strip.idup;
		else
			return incantationToSpell.get(incantationCopy.strip.idup, "FAIL");
	}

	static void luaPutInto(LuaTable t)
	{
		t["spellToIncantation"] = spellToIncantation;
		t["incantationToSpell"] = incantationToSpell;
		t["resolveSpell"] = delegate string(string s) { return MagicWords.resolveSpell(s); };
	}
}
