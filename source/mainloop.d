import std.range;
import std.stdio;
import std.format;

import coolsprite, reacttext;
import choicebox;
import event;
import player;
import charactericon;

import sfml;
import bindbc.sfml;

interface Screen
{
	void init();
	void setWindow(RenderWindow win);
	void event(sfEvent e);
	void update(double dt);
	void updateInactive(double dt);
	void draw();
	void finish();
}

class Mainloop
{
	static Screen[] screens;
	static Clock[] clocks;
	static RenderWindow win = null;
	static CoolSprite cursor;

	static void init(RenderWindow w)
	{
		win = w;
		cursor = new CoolSprite;
		cursor.textureName = "cursor";
		cursor.scale = 50 * win.size.x / 1920 / cursor.bounds.size.x * Vec2(1, 1);
	}

	static void quit()
	{
		win.close();
	}

	static void popAll()
	{
		while(!screens.empty)
			popScreen();
	}

	static void changeScreen(Screen s)
	{
		popAll();
		pushScreen(s);
	}

	static void pushScreen(Screen s)
	{
		s.setWindow(win);
		screens ~= s;
		clocks ~= new Clock;

		s.init;
	}

	static void popScreen()
	{
		if(!screens.empty)
		{
			screens[$-1].finish;
			screens = screens[0 .. $-1];
			clocks = clocks[0 .. $-1];
		}
	}

	static void popUntil(Screen s)
	{
		while(screens[$-1] !is s && !screens.empty)
			popScreen();
	}

	static void mainloop()
	{
		if(!win)
			return;

		while(win.isOpen())
		{
			sfEvent e;

			while(win.pollEvent(&e))
			{
				if(e.type == sfEvtClosed)
					win.close;
				else if(e.type == sfEvtKeyPressed && e.key.code == sfKeyEscape)
					Mainloop.pushScreen(new ChoiceBox(
						"End the Game?",
						"Do you want to stop playing?",
						[
							Choice(null, "No, I want to continue.", delegate(){}, new ReactiveText),
							Choice(null, "Return to the main menu.", delegate() { Events.runEvent("main menu"); }, new ReactiveText),
							Choice(null, "Quit the game.", delegate () { Mainloop.quit; }, new ReactiveText)
						]
					));
				else if(e.type == sfEvtKeyPressed && e.key.code == sfKeyF && e.key.control)
				{
					Choice[] choices = [ Choice(null, "Go back.", delegate(){}, new ReactiveText) ];
					foreach(friend; Player.friends)
						choices ~= Choice(friend.icon.spriteCopy, format("%s\n%s", friend.icon.name, friend.icon.description), delegate(){}, new ReactiveText, false, false);

					Mainloop.pushScreen(new ChoiceBox(
						"List of Friends",
						Player.friends.empty ? "You have no friends yet." : format("Here you can look at all the %s friends you have made. (This is just a reference list; the friends cannot be activated in any way.)", Player.friends.length),
						choices
					));
				}
				else if(!screens.empty)
					screens[$-1].event(e);
			}

			foreach(n, s; screens)
				if(n == screens.length-1)
					s.update(clocks[n].restart);
				else
					s.updateInactive(clocks[n].restart);
			cursor.pos = Mouse.position(win);

			win.clear;
			foreach(s; screens)
				s.draw;
			win.draw(cursor);
			win.display;
		}

		popAll();
	}
}
