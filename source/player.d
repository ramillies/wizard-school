import std.range;
import std.algorithm;
import std.array;
import std.format;
import std.conv;

import sfml;
import luad.all;

import event;
import lua;
import friend;
import charactericon;

class Player
{
	static float fatigue = 0., stress = 0.;
	static int fame = 0, karma = 0;
	static double day = 0;
	static double eloquence = 0;
	static Friend[] friends;

	static int age() { return 20 + day.to!int/336; }
	static void addFatigue(float f)
	{
		fatigue += f;
		fatigue = clamp(fatigue, 0, 100);
	}
	static void addStress(float f)
	{
		stress += f > 0 ? f*age/30 : f;
		if(stress < 0.) stress = 0;
		if(stress > 100.)
			Events.runEvent("death");
	}

	static void addFriend(Friend f)
	{
		friends ~= f;
	}

	static string dateString()
	{
		const months = [ "September", "October", "November", "December", "January",
		      "February", "March", "April", "May", "June", "July", "August" ];
		int roundedDay = day.to!int;
		return format("%d %s", 1 + (roundedDay%28), months[(roundedDay % 336) / 28]);
	}

	static void luaPutInto(LuaTable t)
	{
		t["addFatigue"] = (float x) => Player.addFatigue(x);
		t["addStress"] = (float x) => Player.addStress(x);
		t["addFame"] = (int x) => Player.fame = max(Player.fame + x, 0);
		t["addKarma"] = (int x) => Player.karma += x;
		t["getAge"] = () => Player.age;
		t["getDay"] = () => Player.day;
		t["getFame"] = () => Player.fame;
		t["getKarma"] = () => Player.karma;
		t["getFatigue"] = () => Player.fatigue;
		t["getStress"] = () => Player.stress;
		t["addFriend"] = (LuaTable t) => Player.addFriend(Friend(
					CharacterIcon.fromLuaTable(t),
					t.get!string("magicType")
				));
		t["countFriends"] = (bool delegate(LuaTable) f) => Player.friends.filter!(x => f(x.toLuaTable)).array.length;
		t["getEloquence"] = () => Player.eloquence;
		t["addEloquence"] = (double x) => Player.eloquence += x;
	}
}
