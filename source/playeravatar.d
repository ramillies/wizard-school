import std.algorithm;
import std.range;
import std.array;

import sfml;
import bindbc.sfml;
import luad.all;

import coolsprite;
import worldobject;
import lua;
import util;

enum TargetType { Nothing, Point, Object };

class PlayerAvatar: WorldObject
{
	private CoolSprite sprite;
	private TargetType targetType;
	private Vec2 targetPoint;
	private WorldObject targetObject;

	private ConvexShape movementLine;

	enum pixelsPerDay = 20;

	this()
	{
		sprite = new CoolSprite;
		sprite.textureName = "overworld characters";
		sprite.tilenumber = 0;
		transform = new Transform;

		targetType = TargetType.Nothing;
		movementLine = new ConvexShape;
		movementLine.color = Color.Red;
	}

	mixin(customTransformable);
	mixin(relativeOriginMixin);
	mixin(transformableBbox);

	@property Rect localBounds() { return sprite.localBounds; }
	override @property string name() { return ""; }
	override @property string description() { return ""; }
	override @property string type() { return "player"; }

	void target(Vec2 point) { targetType = TargetType.Point; targetPoint = point; }
	void target(WorldObject obj) { targetType = TargetType.Object; targetObject = obj; }
	bool hasTarget() { return targetType != TargetType.Nothing; }

	override @property bool removed() { return false; }

	void putIntoLua()
	{
		LuaTable t = Lua.lua.get!LuaTable("Player");

		t["ptr"] = ptr2string(cast(void *) this);
		t["getPosition"] = delegate double[](LuaTable t)
		{
			Vec2 pos = string2ptr!PlayerAvatar(t.get!string("ptr")).bounds.center;
			return [ pos.x, pos.y ];
		};
	}

	double travelTime(Vec2 where) { return (this.pos - where).norm/pixelsPerDay; }
	override void mouseOver(bool b) { }

	override void update(double days)
	{
		if(!hasTarget()) return;

		double distance = days * pixelsPerDay;
		Vec2 goTo = targetType == TargetType.Point ? targetPoint : targetObject.bounds.center;
		Vec2 difference = goTo - this.bounds.center;
		this.pos = this.pos + difference.normalized * distance;
		if(difference.norm <= 16.)
		{
			targetType = TargetType.Nothing;
			return;
		}

		Vec2 perp = Vec2(difference.y, -difference.x).normalized;
		
		movementLine.points([
			this.bounds.center + perp, this.bounds.center - perp,
			goTo - perp, goTo + perp
		]);
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		if(targetType != TargetType.Nothing)
			target.draw(movementLine, RenderStates());
		target.draw(sprite, statesToUse);
	}
}
