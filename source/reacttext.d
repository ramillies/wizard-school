import std.string;
import std.algorithm;
import std.array;
import std.math;
import std.stdio;
import std.conv;

import boilerplate;
import sfml;

class ReactiveText: Text
{
	private
	{
		@Write string delegate() _textCallback;
		@Write Color delegate() _colorCallback;
		@Write Color delegate() _outlineColorCallback;
		@Write TextStyle delegate() _styleCallback;
		@Write uint delegate() _sizeCallback;
		@Write Vec2 delegate() _positionCallback;
		@Read @Write double _boxWidth;
	}

	mixin(GenerateFieldAccessors);

	this()
	{
		super();
		_boxWidth = 0.;
	}

	private void linebreaks(string txt)
	{
		string doBreaking(string t)
		{
			if(t == "") return "";
			auto words = t.split.array;
			double[] widths;
			foreach(n; 0 .. words.length)
			{
				this.text = words[0..n+1].join(" ").idup;
				widths ~= this.localBounds.width - (widths.empty ? 0 : widths.sum);
			}

			double optimal = widths.sum / ceil(widths.sum/boxWidth);
			char[] broken;
			double w = 0;
			foreach(k, v; widths)
			{
				if(w + v > boxWidth)
				{
					broken ~= (w == 0 ? "" : "\n") ~ words[k] ~ (w == 0 ? "\n" : " ");
					w = (w == 0) ? 0 : v;
				}
				else if(optimal <= (w+v))
				{
					broken ~= words[k] ~ "\n";
					w = 0;
				}
				else
				{
					broken ~= words[k] ~ " ";
					w += v;
				}
			}
			return broken.idup;
		}
		this.text = txt.split("\n").map!((a) => doBreaking(a).strip("\n")).join("\n").idup;
	}

	void update()
	{
		if(_colorCallback !is null) color = _colorCallback();
		if(_styleCallback !is null) style = _styleCallback();
		if(_sizeCallback !is null) fontSize = _sizeCallback();
		if(_positionCallback !is null) this.pos = _positionCallback();
		string t = (_textCallback is null) ? this.text : _textCallback();
		if(boxWidth > 0.)
			linebreaks(t);
		else
			this.text = t;

		this.updateOrigin;
	}
}
