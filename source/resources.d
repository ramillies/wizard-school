import std.algorithm;
import std.stdio;
import std.string;
import std.json;
import std.exception;
import std.regex;

import sfml;

class ConfigFiles
{
	private static JSONValue[string] files;
	private static JSONValue missing;

	static void load()
	{
		missing = parseJSON("{}");

		foreach(name, path; [
			"magic words": "data/magic-words",
			"events": "data/events",
			"objects": "data/objects",
			"triggers": "data/triggers",
			"textures": "data/textures"
		])
		{
			File f;
			try
			{
				f = File(path ~ ".json", "r");
			}
			catch(std.exception.ErrnoException e)
			{
				writefln("ERROR! Could not open configuration file '%s.json'!", path);
				continue;
			}
			files[name] = f.byLine.join("\n").parseJSON;
		}
	}

	static JSONValue[string] get(string name) { return files.get(name, missing).object; }
	static void unload() { }
}

class Fonts
{
	static Font text, heading, italic;

	static void load()
	{
		text = Font.fromFile("data/fonts/EBGaramond08-Regular.ttf");
		heading = Font.fromFile("data/fonts/EBGaramondSC08-Regular.ttf");
		italic = Font.fromFile("data/fonts/EBGaramond08-Italic.ttf");
	}

	static void unload() { }
}

class Images
{
	private static Texture[string] textures;
	private static Texture missing;

	static void load()
	{
		RenderTexture miss = new RenderTexture(64,64);
		miss.clear(Color.Red);
		miss.display;
		missing = miss.texture;

		foreach(name, file; ConfigFiles.get("textures"))
		{
			textures[name] = Texture.fromFile("data/" ~ file["path"].get!string);
			textures[name].smooth = false;
		}
		writefln("Loaded textures: %s", textures.byKey);
	}

	static Texture texture(string name) { return textures.get(name, missing); }
	static bool exists(string name) { return (name in textures) ? true : false; }
	static Vec2 tileSize(string name)
	{
		auto list = ConfigFiles.get("textures");
		return (name in list) ? Vec2(list[name]["tilesize"][0].get!double, list[name]["tilesize"][1].get!double) : Vec2(0,0);
	}
	static uint tileCount(string name)
	{
		auto list = ConfigFiles.get("textures");
		return (name in list) ? list[name]["tilecount"].get!uint : 0;
	}
}
