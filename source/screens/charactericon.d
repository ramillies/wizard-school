import std.algorithm;
import std.array;
import std.range;
import std.stdio;

import sfml;
import bindbc.sfml;
import luad.all;

import mainloop;
import coolsprite;
import reacttext;
import timer;
import pongdistractor;
import resources;
import util;
import messagebox;
import inputbox;
import lua;

class CharacterIcon: Transformable, Drawable
{
	string name, description;
	uint tilenumber;

	private CoolSprite sprite, nameParchment, descParchment;
	private ReactiveText nameText, descText;

	this(string name, uint tilenumber, string profession = "")
	{
		this.name = name;
		this.tilenumber = tilenumber;
		this.description = profession;

		sprite = new CoolSprite;
		sprite.textureName = "character faces";
		sprite.tilenumber = tilenumber;

		nameParchment = new CoolSprite;
		nameParchment.textureName = "name tag";
		nameParchment.relativeOrigin = Vec2(0.5, 0);
		nameParchment.pos = sprite.localBounds().relativePoint(Vec2(0.5,1)) + Vec2(0, 10);

		nameText = new ReactiveText;
		nameText.font = Fonts.text;
		nameText.fontSize = 50;
		nameText.color = Color.Black;
		nameText.text = name;
		nameText.relativeOrigin = Vec2(0.5, 0.5);
		nameText.pos = nameParchment.bounds.center;

		descParchment = new CoolSprite;
		descParchment.textureName = "name tag";
		descParchment.relativeOrigin = Vec2(0.5, 1);
		descParchment.pos = sprite.localBounds().relativePoint(Vec2(0.5,0)) - Vec2(0, 10);

		descText = new ReactiveText;
		descText.font = Fonts.text;
		descText.fontSize = 50;
		descText.color = Color.Black;
		descText.text = description;
		descText.relativeOrigin = Vec2(0.5, 0.5);
		descText.pos = descParchment.bounds.center;

		transform = new Transform;
	}

	mixin(customTransformable);
	mixin(relativeOriginMixin);
	mixin(transformableBbox);

	@property Rect localBounds()
	{
		if(description == "")
			return Rect(sprite.bounds.left, sprite.bounds.top, sprite.bounds.width, sprite.bounds.height + nameParchment.bounds.height + 10);
		else
			return Rect(sprite.bounds.left, descParchment.bounds.top, sprite.bounds.width, sprite.bounds.height + nameParchment.bounds.height + descParchment.bounds.height + 20);
	}

	static CharacterIcon fromLuaTable(LuaTable t)
	{
		return new CharacterIcon(
			t.get!string("name"),
			t.get!uint("tilenumber"),
			t["description"].isNil ? "" : t.get!string("description")
		);
	}

	@property CoolSprite spriteCopy()
	{
		CoolSprite rv = new CoolSprite;
		rv.textureName = "character faces";
		rv.tilenumber = tilenumber;
		return rv;
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		target.draw(sprite, statesToUse);
		target.draw(nameParchment, statesToUse);
		target.draw(nameText, statesToUse);
		if(description != "")
		{
			target.draw(descParchment, statesToUse);
			target.draw(descText, statesToUse);
		}
	}
}

