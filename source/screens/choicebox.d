import std.array;
import std.algorithm;
import std.stdio;
import std.json;
import std.range;
import std.conv;

import mainloop;
import resources;
import reacttext;
import coolsprite;

import luad.all;
import sfml;
import bindbc.sfml;

struct Choice
{
	CoolSprite sprite;
	string text;
	void delegate() callback;
	ReactiveText textRendering;
	bool disabled = false, popBox = true;

	static Choice fromLuaTable(LuaTable obj)
	{
		CoolSprite sprite;
		string text = "";
		void delegate() callback = delegate void() { };
		ReactiveText t = new ReactiveText;
		bool disabled = false;
		bool popBox = true;
		
		if(!obj["text"].isNil) text = obj.get!string("text");
		if(!obj["callback"].isNil) callback = obj.get!(void delegate())("callback");
		if(!obj["disabled"].isNil) disabled = obj.get!bool("disabled");
		if(!obj["popBox"].isNil) popBox = obj.get!bool("popBox");
		if(!obj["tileset"].isNil && Images.exists(obj.get!string("tileset")))
		{
			sprite = new CoolSprite;
			sprite.textureName = obj.get!string("tileset");
			if(!obj["tilenumber"].isNil)
				sprite.tilenumber = obj.get!int("tilenumber");
		}

		return Choice(sprite, text, callback, t, disabled, popBox);
	}
}

class ChoiceBox: Screen
{
	private ReactiveText[] texts;
	private RenderWindow win;
	private string heading, msg;
	private Choice[] choices;
	const int ROWHEIGHT = 65;
	private View camera;
	private RectangleShape cursor;
	private CoolSprite background;

	private double choicesTop, choicesHeight;

	this(string h, string m, Choice[] c)
	{
		heading = h;
		msg = m;
		choices = c;
	}

	override void init()
	{
		texts = 2.iota.map!((x) => new ReactiveText).array;
		foreach(txt; texts)
		{
			txt.font = Fonts.text;
			txt.color = Color.Black;
			txt.boxWidth = win.size.y * .8 - 50;
		}
		with(texts[0])
		{
			font = Fonts.heading;
			fontSize = 80;
			color = Color.Black;
			style = TextStyle.Bold;
			relativeOrigin = Vec2(.5, .5);
			positionCallback = () => Vec2(.5*win.size.x, .10*win.size.y + 5);
			text = heading;
		}
		with(texts[1])
		{
			fontSize = 40;
			relativeOrigin = Vec2(.5, 0);
			positionCallback = () => Vec2(.5*win.size.x, .17*win.size.y);
			text = msg;
		}

		foreach(n, choice; choices)
		{
			with(choice.textRendering)
			{
				font = Fonts.text;
				sizeCallback = () => 35;
				boxWidth = win.size.y * .8 - ROWHEIGHT;
				if(choice.disabled)
					color = Color(128, 128, 128);
				else
					color = Color.Black;
				relativeOrigin = Vec2(0, .5);
				pos = Vec2(.02 * win.size.x + ROWHEIGHT+20, n*ROWHEIGHT + ROWHEIGHT/2);
				text = choice.text;
			}
			if(choice.sprite !is null)
			{
				with(choice.sprite)
				{
					relativeOrigin = Vec2(.5, .5);
					pos = Vec2(.02 * win.size.x + ROWHEIGHT/2, ROWHEIGHT/2 + n*ROWHEIGHT);
					auto bound = localBounds;
					scale = Vec2(ROWHEIGHT/bound.height, ROWHEIGHT/bound.height);
					if(choice.disabled)
						color = Color(128, 128, 128);
				}
			}
		}

		texts[1].update;
		auto bounds = texts[1].bounds;
		choicesTop = (bounds.top + bounds.height + 60)/win.size.y;
		choicesHeight = .92 - choicesTop;

		if(choicesHeight < 0.1)
		{
			choicesTop = .82;
			choicesHeight = .10;
		}

		camera = new View(Rect(0, 0, .45 * win.size.x, choicesHeight * win.size.y));
		camera.viewport = Rect(.275f, choicesTop, .45f, choicesHeight);

		cursor = new RectangleShape;
		cursor.size = Vec2(.40*win.size.x, ROWHEIGHT);
		cursor.color = Color(225, 188, 0, 80);

		background = new CoolSprite;
		background.textureName = "paper background";
		background.relativeOrigin = Vec2(0.5, 0.5);
		background.scale = win.size.y * Vec2(.9, .8) / Vec2(background.localBounds.width, background.localBounds.height);
		background.rotation = 90;
		background.pos = .5 * win.size;
	}

	override void setWindow(RenderWindow w) { win = w; }

	override void event(sfEvent e)
	{
		if(e.type == sfEvtMouseWheelScrolled)
			camera.center = Vec2(camera.center.x, clamp(camera.center.y - ROWHEIGHT*e.mouseWheelScroll.delta, camera.size.y/2, max(choices.length*ROWHEIGHT - camera.size.y/2, camera.size.y/2)));
		if(e.type == sfEvtMouseButtonPressed)
		{
			if(e.mouseButton.button == sfMouseLeft && mouseInBox() && !choices[mouseRow()].disabled)
			{
				if(choices[mouseRow()].popBox)
					Mainloop.popScreen;
				choices[mouseRow()].callback();
			}
		}
	}

	override void update(double dt)
	{
		texts.each!((t) => t.update);
		choices.each!((c) => c.textRendering.update);
	}

	override void updateInactive(double dt) { }

	override void draw()
	{
		/*
		RectangleShape r = new RectangleShape;
		r.size = Vec2(.5 * win.size.x, .9*win.size.y);
		r.color = Color.Black;
		r.outlineThickness = win.size.x/100;
		r.outlineColor = Color.Red;
		r.pos = Vec2(.25*win.size.x, .04*win.size.y);
		win.draw(r);
		*/

		win.draw(background);

		texts.each!((t) => win.draw(t));

		if(camera.size.y < choices.length*ROWHEIGHT)
		{
			RectangleShape bar = new RectangleShape;
			bar.size = Vec2(10f, camera.size.y ^^ 2 /ROWHEIGHT/choices.length);
			bar.color = Color.Black;
			bar.pos = Vec2(.6975*win.size.x - 5, choicesTop*win.size.y + camera.size.y * (camera.center.y - camera.size.y/2)/choices.length/ROWHEIGHT);
			win.draw(bar);
		}

		win.view = camera;
		foreach(c; choices)
		{
			if(c.sprite !is null)
				win.draw(c.sprite);
			win.draw(c.textRendering);
		}
		if(mouseInBox() && !choices[mouseRow()].disabled)
		{
			cursor.pos = Vec2(.02*win.size.x, ROWHEIGHT*mouseRow());
			win.draw(cursor);
		}

		win.view = win.defaultView;
	}

	private bool mouseInBox()
	{
		auto cw = camera.viewport;
		return Rect(cw.left*win.size.x, cw.top*win.size.y, cw.width*win.size.x, cw.height*win.size.y).contains(Mouse.position(win));
	}

	private int mouseRow()
	{
		auto cw = camera.viewport;
		if(mouseInBox())
		{
			auto pt = win.pixelToCoords(Mouse.position(win), camera);
			return clamp(to!int(pt.y/ROWHEIGHT), 0, choices.length-1);
		}
		else
			return 0;
	}

	override void finish()
	{
	}
}

