import std.range;
import std.random;
import std.algorithm;
import std.array;
import std.stdio;
import std.format;
import std.math;
import std.conv;
import std.datetime.systime;

import mainloop;
import resources;
import lua;
import world;
import player;
import reacttext;
import messagebox;
import choicebox;
import infoboxes;
import stressmeter;
import playeravatar, scriptedobject;
import scriptedtrigger;
import util;

import luad.all;
import bindbc.sfml;
import sfml;

class GameScreen: Screen
{
	private World world;
	private double zoom, maxZoom;
	private Vec2 defaultViewSize;
	private View camera;
	private InfoBox gametime, realtime, fame;
	private StressMeter stressMeter;
	private bool paused;
	private double daysPerSecond;

	private PlayerAvatar player;
	private ReactiveText travelTime;
	private RenderWindow win;
	private ScriptableObject[] objects;
	private ScriptableTrigger[] triggers;
	private ReactiveText[] objectNames;
	private ReactiveText descText;
	private ScriptableObject objectUnderMouse;

	this(World w)
	{
		world = w;
		paused = true;
		daysPerSecond = 14;
		player = new PlayerAvatar;

		travelTime = new ReactiveText;
		travelTime.font = Fonts.text;
		travelTime.fontSize = 30;
		travelTime.color = Color.Black;
		travelTime.outlineColor = Color.White;
		travelTime.outlineThickness = 2;
		travelTime.textCallback = () => format("%d days", player.travelTime(win.pixelToCoords(Mouse.position(win), camera)).to!int);
		travelTime.relativeOrigin = Vec2(.5, 0);
		travelTime.positionCallback = () => Mouse.position(win) + Vec2(0, 10);

		with(descText = new ReactiveText)
		{
			font = Fonts.text;
			fontSize = 30;
			color = Color.Black;
			outlineColor = Color.White;
			boxWidth = 250;
			outlineThickness = 1;
			textCallback = () => objectUnderMouse is null ? "" : objectUnderMouse.description;
			relativeOrigin = Vec2(.5, 1);
			positionCallback = () => objectUnderMouse is null ? Vec2(0,0) : win.coordsToPixel(objectUnderMouse.bounds.relativePoint(Vec2(.5, 0)), camera) - Vec2(0, 5);
		}
	}

	void addObject(ScriptableObject obj)
	{
		objects ~= obj;
		ReactiveText nameText = new ReactiveText;
		with(nameText)
		{
			font = Fonts.heading;
			fontSize = 30;
			color = Color.Black;
			outlineColor = Color.White;
			outlineThickness = 2;
			textCallback = () => obj.name;
			relativeOrigin = Vec2(.5, 0);
			positionCallback = () => win.coordsToPixel(obj.bounds.relativePoint(Vec2(.5, 1)), camera) + Vec2(0, 5);
		}
		objectNames ~= nameText;
	}

	void addTrigger(ScriptableTrigger trig) { triggers ~= trig; }

	private void putIntoLua()
	{
		LuaTable worldTable = Lua.lua.get!LuaTable("World");
		LuaTable playerTable = Lua.lua.get!LuaTable("Player");

		worldTable["gameScreenPtr"] = ptr2string(cast(void *) this);
		worldTable["addObject"] = delegate bool(LuaTable t, LuaTable o)
		{
			if(o["objectType"].isNil || o["pos"].isNil) return false;
			ScriptableObject obj = new ScriptableObject(o.get!string("objectType"), o);
			obj.relativeOrigin = Vec2(.5, .5);
			double[] pos = o.get!(double[])("pos");
			obj.pos = Vec2(pos[0], pos[1]);

			string2ptr!GameScreen(t.get!string("gameScreenPtr")).addObject(obj);
			return true;
		};
		worldTable["addTrigger"] = delegate bool(LuaTable t, LuaTable o)
		{
			if(o["triggerType"].isNil) return false;
			ScriptableTrigger obj = new ScriptableTrigger(o.get!string("triggerType"), o);
			string2ptr!GameScreen(t.get!string("gameScreenPtr")).addTrigger(obj);
			return true;
		};
		worldTable["listObjects"] = delegate LuaTable[](LuaTable t, bool delegate (LuaTable) filterFunc = x => true)
		{
			return string2ptr!GameScreen(t.get!string("gameScreenPtr")).objects
				.map!(o => o.luaObject).filter!filterFunc.array;
		};
		worldTable["nearestObject"] = delegate LuaTable (LuaTable t, LuaTable me, bool delegate (LuaTable) filterFunc = x => true)
		{
			ScriptableObject myObj = string2ptr!ScriptableObject(me.get!string("ptr"));
			ScriptableObject rv;
			double distance = double.infinity;
			foreach(o; string2ptr!GameScreen(t.get!string("gameScreenPtr")).objects)
			{
				double s = (myObj.bounds.center - o.bounds.center).norm;
				if(filterFunc(o.luaObject) && s < distance)
				{
					distance = s;
					rv = o;
				}
			}
			return distance == double.infinity ? Lua.makeTable : rv.luaObject;
		};
		worldTable["distanceFromNearest"] = delegate double (LuaTable t, double[] pos)
		{
			auto objectList = string2ptr!GameScreen(t.get!string("gameScreenPtr")).objects;
			if(pos.length < 2) return double.infinity;
			Vec2 v = Vec2(pos[0], pos[1]);
			double minimum = double.infinity;
			foreach(o; objectList)
				minimum = min(minimum, (o.bounds.center - v).norm);
			return minimum;
		};

		playerTable["gameScreenPtr"] = ptr2string(cast(void *) this);
		playerTable["passTime"] = (LuaTable t, double days) => update(days / daysPerSecond);
		player.putIntoLua();
	}

	override void setWindow(RenderWindow w)
	{
		win = w;
	}

	override void init()
	{
		camera = win.defaultView;
		camera.center = world.bounds.center;
		player.relativeOrigin = Vec2(.5, .5);
		player.pos = world.bounds.center;
		defaultViewSize = camera.size;

		maxZoom = min(1.*world.bounds.width/camera.size.x, 1.*world.bounds.height/camera.size.y) - .01;
		zoom = max(1, 1/maxZoom);
		camera.size = 1 / zoom * defaultViewSize;

		gametime = timeInfoBox();
		gametime.relativeOrigin = Vec2(0, 0);
		gametime.pos = Vec2(.02, .01) * win.size;

		fame = fameKarmaBox();
		fame.relativeOrigin = Vec2(1, 0);
		fame.pos = Vec2(.99, .01) * win.size;

		realtime = realTimeBox();
		realtime.relativeOrigin = Vec2(1, 1);
		realtime.pos = Vec2(.99, .96) * win.size;

		stressMeter = new StressMeter();
		stressMeter.size = Vec2(.6, .05) * win.size;
		stressMeter.relativeOrigin = Vec2(.5, 0);
		stressMeter.pos = Vec2(.5, .01) * win.size;
		Player.fatigue = 70;

		putIntoLua();
		Lua.dofile(`data/scripts/init-world.lua`);
	}

	override void event(sfEvent e)
	{
		if(e.type == sfEvtClosed)
			Mainloop.quit;
		if(e.type == sfEvtKeyPressed)
		{
			if(e.key.code == sfKeySpace)
				paused = !paused;
		}
		if(e.type == sfEvtMouseButtonPressed)
			if(e.mouseButton.button == sfMouseLeft)
			{
				paused = false;
				if(objectUnderMouse !is null)
					player.target(objectUnderMouse);
				else
					player.target(win.pixelToCoords(Mouse.position(win), camera));
			}
		if(e.type == sfEvtMouseWheelScrolled)
		{
			zoom = clamp(zoom * pow(2, e.mouseWheelScroll.delta/12.), 1/maxZoom, 4);
			camera.size = 1 / zoom * defaultViewSize;
		}
	}

	void addGameTime(double days)
	{
		Player.day += days;
		player.update(days);
		objects.each!(o => o.update(days));
		triggers.each!(t => t.update(days));
	}

	private void updateMouse(double dt)
	{
		auto mouse = Mouse.position(win);
		if(mouse.x < win.size.x/20)
			camera.center = camera.center + Vec2(-1024*dt, 0);
		if(mouse.y < win.size.y/20)
			camera.center = camera.center + Vec2(0, -1024*dt);
		if(mouse.x > win.size.x*19/20)
			camera.center = camera.center + Vec2(1024*dt, 0);
		if(mouse.y > win.size.y*19/20)
			camera.center = camera.center + Vec2(0, 1024*dt);
		normalizeCamera();

		objectUnderMouse = null;
		foreach(obj; objects)
		{
			if(obj.bounds.contains(win.pixelToCoords(Mouse.position(win), camera)))
			{
				obj.mouseOver(true);
				objectUnderMouse = obj;
			}
			else obj.mouseOver(false);
		}
	}

	private void updateTime(double dt)
	{
		bool hadTarget = player.hasTarget;
		addGameTime(daysPerSecond * dt);
		if(hadTarget && !player.hasTarget)
			paused = true;
	}

	private void updateHud(double dt)
	{
		gametime.update;
		fame.update;
		realtime.update;
		stressMeter.update;
		objectNames.each!`a.update`;
		descText.update;

		travelTime.update;
	}

	private void updateObjectRemovals()
	{
		ptrdiff_t nextToRemove;
		while((nextToRemove = objects.countUntil!`a.removed`) != -1)
		{
			destroy(objects[nextToRemove]);
			objects = objects.remove(nextToRemove);
			objectNames = objectNames.remove(nextToRemove);
		}
		while((nextToRemove = triggers.countUntil!`a.removed`) != -1)
		{
			destroy(triggers[nextToRemove]);
			triggers = triggers.remove(nextToRemove);
		}
	}

	private void checkCollisions()
	{
		foreach(obj; objects)
		{
			if((player.bounds.center - obj.bounds.center).norm < 16)
				obj.collideWithPlayer;
			foreach(other; objects)
				if((obj.bounds.center - other.bounds.center).norm < 16)
				{
					if(obj.name == "")
						writefln("Weird object type: %s and desc: %s", obj.type, obj.description);
					obj.collideWith(other);
				}
		}
	}

	override void update(double dt)
	{
		updateMouse(dt);
		if(!paused) updateTime(dt);
		updateHud(dt);
		checkCollisions();
		updateObjectRemovals();
	}

	override void updateInactive(double dt)
	{
	}

	override void draw()
	{
		win.view = camera;

		win.draw(world);
		objects.each!(o => win.draw(o));
		win.draw(player);

		win.view = win.defaultView;
		objectNames.each!(o => win.draw(o));
		win.draw(descText);
		win.draw(travelTime);
		win.draw(gametime);
		win.draw(fame);
		win.draw(realtime);
		win.draw(stressMeter);
	}

	override void finish()
	{
		foreach(obj; objects)
			destroy(obj);
		foreach(trig; triggers)
			destroy(trig);
		destroy(player);
	}

	private void normalizeCamera()
	{
		camera.center = Vec2(
			clamp(camera.center.x, camera.size.x/2, world.bounds.size.x - camera.size.x/2),
			clamp(camera.center.y, camera.size.y/2, world.bounds.size.y - camera.size.y/2)
		);
	}
}
