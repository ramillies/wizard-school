import std.range;
import std.array;
import std.algorithm;
import std.datetime;
import std.format;
import std.stdio;

import sfml;
import luad.all;
import bindbc.sfml;

import player;
import resources;
import coolsprite,reacttext;

class InfoBox: Transformable, Drawable, Bboxable
{
	private CoolSprite background;
	private ReactiveText label;

	this()
	{
		background = new CoolSprite;
		background.textureName = "paper horizontal slip";

		label = new ReactiveText;
		label.font = Fonts.text;
		label.fontSize = 40;
		label.color = Color.Black;
		label.relativeOrigin = Vec2(.5, .2);

		transform = new Transform;
	}

	@property string text() { return label.text; }
	@property void text(string s) { label.text = s; }
	@property void textCallback(string delegate () d) { label.textCallback = d; }

	void updateBackground()
	{
		background.scale = label.localBounds.size / Vec2(.887, .727) / background.localBounds.size;
		label.pos = background.bounds.relativePoint(Vec2(.467, .32));
		update();
	}

	mixin(customTransformable);
	private Vec2 _relativeOrigin = Vec2(0.5, 0.5);
	private bool _relativeOriginAllowed = false;

	void noRelativeOrigin() { _relativeOriginAllowed = false; }
	void updateOrigin() { if(_relativeOriginAllowed) origin = localBounds().relativePoint(_relativeOrigin) * background.scale; }

	@property void relativeOrigin(Vec2 v)
	{
		_relativeOrigin = v;
		_relativeOriginAllowed = true;
		updateOrigin;
	}

	@property Vec2 relativeOrigin() { return _relativeOrigin; }

	@property Rect bounds()
	{
		sfTransform myTransform = transform.matrix;
		return Rect.fromFloatRect(sfTransform_transformRect(&myTransform, localBounds().toFloatRect));
	}
	@property Rect localBounds() { return background.localBounds; }

	void update() { label.update; }

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		target.draw(background, statesToUse);
		target.draw(label, statesToUse);
	}
}

InfoBox timeInfoBox()
{
	InfoBox box = new InfoBox;
	box.text = "31 December\n9999 years old";
	box.updateBackground;
	box.textCallback = () => format("%s\n%s years old", Player.dateString, Player.age);
	return box;
}

InfoBox fameKarmaBox()
{
	InfoBox box = new InfoBox;
	box.text = "6576 fame | -487";
	box.updateBackground;
	box.textCallback = () => format("%s fame | %s", Player.fame, Player.karma);
	return box;
}

InfoBox realTimeBox()
{
	InfoBox box = new InfoBox;
	box.text = "46:789";
	box.updateBackground;
	box.textCallback = delegate string()
	{
		auto systime = std.datetime.systime.Clock.currTime;
		return format("%02u:%02u", systime.hour, systime.minute);
	};
	return box;
}

