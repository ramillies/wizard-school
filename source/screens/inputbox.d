import std.array;
import std.algorithm;
import std.stdio;
import std.json;
import std.range;
import std.conv;

import mainloop;
import resources;
import reacttext;
import coolsprite;

import sfml;
import bindbc.sfml;

class InputBox: Screen
{
	private ReactiveText[] texts;
	private RenderWindow win;
	private string heading, msg;
	private dchar[] input;
	private double boxHeight, boxWidth;
	private CoolSprite background;
	private RectangleShape underline, cursor;

	private void delegate (string s) onSubmit;

	this(string h, string m, void delegate (string s) f)
	{
		heading = h;
		msg = m;
		onSubmit = f;
		boxHeight = .6 * .75;
		boxWidth = .6;
	}

	override void init()
	{
		texts = 3.iota.map!((x) => new ReactiveText).array;
		foreach(txt; texts)
		{
			txt.font = Fonts.text;
			txt.color = Color.Black;
			txt.boxWidth = win.size.x*boxWidth - 50;
			txt.fontSize = to!uint(40 * win.size.x)/1920;
		}
		with(texts[0])
		{
			font = Fonts.heading;
			fontSize = to!uint(80 * win.size.x)/1920;
			style = TextStyle.Bold;
			relativeOrigin = Vec2(.5, 0);
			positionCallback = () => Vec2(.5*win.size.x, (.5-boxHeight*2/3)*win.size.y + 5);
			textCallback = () => heading;
		}

		with(texts[1])
		{
			relativeOrigin = Vec2(.5, .5);
			positionCallback = () => Vec2(.5*win.size.x, .5*win.size.y);
			textCallback = () => msg;
		}

		with(texts[2])
		{
			textCallback = () => input.to!string;
			positionCallback = () => Vec2((.55-this.boxWidth/2)*win.size.x, texts[1].bounds.bottom + .05*win.size.y);
		}
		texts[2].boxWidth = (boxWidth - .1)*win.size.x;

		underline = new RectangleShape;
		underline.size = Vec2((boxWidth - .1)*win.size.x, 2);
		underline.color = Color.Black;

		cursor = new RectangleShape;
		cursor.size = Vec2(1, texts[2].fontSize);
		cursor.relativeOrigin = Vec2(0, 1);
		cursor.color = Color.Black;

		background = new CoolSprite;
		background.textureName = "paper background";
		background.relativeOrigin = Vec2(.5, .5);
		background.scale = win.size.x * boxWidth / background.localBounds.width * Vec2(1, 1);
		background.pos = Vec2(.5 * win.size.x, .5 * win.size.y);
	}

	override void setWindow(RenderWindow w) { win = w; }

	override void event(sfEvent e)
	{
		if(e.type == sfEvtKeyPressed)
		{
			if(e.key.code == sfKeyEnter)
			{
				Mainloop.popScreen;
				onSubmit(input.to!string);
			}
		}
		if(e.type == sfEvtTextEntered)
		{
			if(e.text.unicode == 8 && !input.empty)
				input = input[0 .. $-1];
			if(e.text.unicode >= 32)
				input ~= e.text.unicode.to!dchar;
		}
	}

	override void update(double dt)
	{
		texts.each!((t) => t.update);
		underline.pos = texts[2].pos + Vec2(0, texts[2].fontSize*(to!int(texts[2].bounds.height/texts[2].fontSize) + 1.1));
		float spaceWidth = Fonts.text.glyph(32, texts[2].fontSize, false, 0.).advance;
		long spacesAtEnd = countUntil!`a != b`(input.retro, ' ');
		cursor.pos = Vec2(texts[2].bounds.right + 3 + spaceWidth * (spacesAtEnd < 0 ? 0 : spacesAtEnd), underline.pos.y - 3);
	}
	override void updateInactive(double dt) { }

	override void draw()
	{
		win.draw(background);
		win.draw(underline);
		//win.draw(cursor);
		texts.each!((t) => win.draw(t));
	}

	override void finish()
	{
	}
}

