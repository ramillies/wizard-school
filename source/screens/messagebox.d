import std.array;
import std.algorithm;
import std.stdio;
import std.json;
import std.range;
import std.conv;

import mainloop;
import resources;
import reacttext;
import coolsprite;

import sfml;
import bindbc.sfml;

class MessageBox: Screen
{
	private ReactiveText[] texts;
	private RenderWindow win;
	private string heading, msg;
	private double boxHeight, boxWidth;
	private CoolSprite background;

	private bool closable;
	private void delegate () onClose;

	this(string h, string m, void delegate () f, bool c = true)
	{
		heading = h;
		msg = m;
		closable = c;
		onClose = f;
		boxHeight = .6 * .75;
		boxWidth = .6;
	}

	override void init()
	{
		texts = 2.iota.map!((x) => new ReactiveText).array;
		foreach(txt; texts)
		{
			txt.font = Fonts.text;
			txt.color = Color.Black;
			txt.boxWidth = win.size.x*boxWidth - 50;
		}
		with(texts[0])
		{
			font = Fonts.heading;
			fontSize = to!uint(80 * win.size.x)/1920;
			style = TextStyle.Bold;
			relativeOrigin = Vec2(.5, 0);
			positionCallback = () => Vec2(.5*win.size.x, (.5-boxHeight*2/3)*win.size.y + 5);
			textCallback = () => heading;
		}
		with(texts[1])
		{
			fontSize = to!uint(40 * win.size.x)/1920;
			relativeOrigin = Vec2(.5, .5);
			positionCallback = () => Vec2(.5*win.size.x, .5*win.size.y);
			textCallback = () => msg;
		}

		background = new CoolSprite;
		background.textureName = "paper background";
		background.relativeOrigin = Vec2(.5, .5);
		background.scale = win.size.x * boxWidth / background.localBounds.width * Vec2(1, 1);
		background.pos = Vec2(.5 * win.size.x, .5 * win.size.y);
	}

	override void setWindow(RenderWindow w) { win = w; }

	override void event(sfEvent e)
	{
		if(closable)
			if((e.type == sfEvtKeyPressed && e.key.code == sfKeyEnter) || e.type == sfEvtMouseButtonPressed)
			{
				Mainloop.popScreen;
				onClose();
			}
	}

	override void update(double dt) { texts.each!((t) => t.update); }
	override void updateInactive(double dt) { }

	override void draw()
	{
		win.draw(background);
		texts.each!((t) => win.draw(t));
	}

	override void finish()
	{
	}
}

