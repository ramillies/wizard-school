import std.stdio;
import std.array;
import std.algorithm;
import std.stdio;
import std.json;
import std.range;
import std.conv;
import std.format;
import std.math;
import std.random;

import mainloop;
import resources;
import reacttext;
import coolsprite;

import sfml;
import bindbc.sfml;

class ScenePongDistractor: Transformable, Drawable, Bboxable
{
	public bool paused = false;
	public double speed;

	private Texture stadiumTexture;
	private Sprite stadium;
	private RectangleShape paddle;
	private CircleShape ball;

	private Vec2 unitVelocity, position;
	private double paddlePos;

	private double r;

	private bool gameLost;

	this(Vec2 winSize)
	{
		r = 90. * winSize.x  / 1920;

		makeStadium();

		ball = new CircleShape;
		ball.radius = r/16;
		ball.pointCount = 10;
		ball.color = Color.Red;
		ball.pos = Vec2(100, 0);
		ball.relativeOrigin = Vec2(.5, .5);
		position = ball.pos;

		paddle = new RectangleShape;
		paddle.size = Vec2(r/20, r/2);
		paddle.color = Color.Yellow;
		paddle.pos = Vec2(0, 0);

		paddlePos = 0;
		double angle = uniform(5*PI/9, 8*PI/9);
		speed = .1; 
		unitVelocity = Vec2(sin(angle), -cos(angle));

		gameLost = false;

		transform = new Transform;
	}

	private void makeStadium()
	{
		ConvexShape shape = new ConvexShape;
		Vec2[] points = [ Vec2(0, 0), Vec2(2*r, 0) ];
		for(double k = 0.; k < 1.; k += 0.02)
			points ~= r*Vec2(2, 1) + r*Vec2(sin(PI*k), -cos(PI*k));
		points ~= [ Vec2(2*r,2*r), Vec2(0, 2*r) ];
		shape.points = points;
		shape.color = Color.None;
		shape.outlineColor = Color.Yellow;
		shape.outlineThickness = -r/20;
		shape.pos = Vec2(0, 0);

		RenderTexture tex = new RenderTexture(3*r.to!uint, 2*r.to!uint);
		tex.draw(shape);

		RectangleShape cover = new RectangleShape;
		cover.size = Vec2(r/20, 2*r);
		cover.color = Color.None;
		cover.pos = Vec2(0,0);

		RenderStates states = RenderStates(sfBlendNone);
		tex.draw(cover, states);

		stadiumTexture = tex.texture;
		stadium = new Sprite;
		stadium.pos = Vec2(0,0);
		stadium.texture = stadiumTexture;
	}

	mixin(customTransformable);
	mixin(relativeOriginMixin);

	@property Rect bounds() { return stadium.bounds; }
	@property Rect localBounds() { return stadium.localBounds; }

	@property bool lost() { return gameLost; }

	void reset()
	{
		position = ball.pos;
		double angle = uniform(2*PI/3, PI);
		unitVelocity = Vec2(sin(angle), -cos(angle));
		gameLost = false;

		update(0);
	}

	void pause() { paused = true; }
	void resume() { paused = false; }
	void toggle() { paused = !paused; }

	void update(double dt)
	{
		if(gameLost) return;

		processHitSides();
		processHitCircle();
		processHitPaddle();

		position += speed * 2 * r * dt * unitVelocity;
		ball.pos = position;
	}

	private void reflectByAxis(Vec2 axis) { unitVelocity -= 2 * unitVelocity.dot(axis) * axis; }

	private void processHitSides()
	{
		if((position.x < 0) || (position.x > 2*r))
			return;
		if(position.y < r/20)
		{
			position.y = r/20;
			reflectByAxis(Vec2(0,1));
		}
		if(position.y > 1.95*r)
		{
			position.y = 1.95*r;
			reflectByAxis(Vec2(0,-1));
		}
	}

	private void processHitPaddle()
	{
		if(position.x > r/20)
			return;
		if((position.y >= 2*r*paddlePos) && (position.y <= 2*r*(paddlePos + .25)))
		{
			position.x = r/20;
			reflectByAxis(Vec2(1,0));
		}
		else
			gameLost = true;
	}

	private void processHitCircle()
	{
		double R = (position - Vec2(2*r,r)).norm;
		if((position.x < 2*r) || (R < .95*r))
			return;
		double angle = acos((r - position.y) / R);
		position = Vec2(2*r, r) + .95*r*Vec2(sin(angle), -cos(angle));
		reflectByAxis(Vec2(-sin(angle), cos(angle)));
	}

	void updateMouse(Vec2 normalizedMousePos)
	{
		paddlePos = normalizedMousePos.y * .75;
		paddle.pos = Vec2(0, 2*r*paddlePos);
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		target.draw(stadium, statesToUse);
		target.draw(paddle, statesToUse);
		target.draw(ball, statesToUse);
	}
}
