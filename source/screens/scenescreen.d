import std.algorithm;
import std.array;
import std.range;
import std.stdio;
import std.format;

import sfml;
import bindbc.sfml;
import luad.all;

import mainloop;
import coolsprite;
import reacttext;
import timer;
import stressmeter;
import pongdistractor;
import resources;
import player;
import util;
import messagebox;
import inputbox;
import lua;
import charactericon;
import choicebox;
import infoboxes;

private Screen screenFromTable(LuaTable t)
{
	switch(t.get!string("screenType"))
	{
		case "MessageBox":
			return new MessageBox(
				t.get!string("title"),
				t.get!string("text"),
				t["onExit"].isNil ? delegate(){} : t.get!(void delegate())("onExit"),
				t["closable"].isNil ? true : t.get!bool("closable")
			);
		case "InputBox":
			return new InputBox(
				t.get!string("title"),
				t.get!string("text"),
				t["onSubmit"].isNil ? delegate(string s){} : t.get!(void delegate(string))("onSubmit"),
			);
		case "ChoiceBox":
			return new ChoiceBox(
				t.get!string("title"),
				t.get!string("text"),
				t.get!(LuaTable[])("choices").map!(c => Choice.fromLuaTable(c)).array
			);
		default:
			return new MessageBox("Invalid screen type!", "Invalid screen type has been specified.", delegate(){});
	}
}

struct Scene
{
	string background;
	CharacterIcon[] characters;
	Screen screen;
	void delegate() onInit;
	void delegate() onTimeout;
	void delegate() onDistraction;

	static Scene fromLuaTable(LuaTable t)
	{
		auto s = Scene(
			t.get!string("background"),
			t["characters"].isNil ? [] : t.get!(LuaTable[])("characters").map!(c => CharacterIcon.fromLuaTable(c)).array,
			screenFromTable(t.get!LuaTable("screen")),
			t["onInit"].isNil ? delegate(){} : t.get!(void delegate())("onInit"),
			t["onTimeout"].isNil ? delegate(){} : t.get!(void delegate())("onTimeout"),
			t["onDistraction"].isNil ? delegate(){} : t.get!(void delegate())("onDistraction")
		);
		return s;
	}
}

class SceneScreen: Screen
{
	private Scene currentScene;
	private RenderWindow win;
	private SceneTimer timer;
	private ScenePongDistractor distractor;
	private StressMeter stressMeter;
	private InfoBox gametime, realtime, fame;
	private string timeText;
	private bool timerEnabled = false, distractorEnabled = false;
	private bool gametimeVisible = true, realtimeVisible = true, fameVisible = true, meterVisible = true;

	private CoolSprite background;

	this()
	{
		background = new CoolSprite;
	}

	void setScene(Scene s) { currentScene = s; }

	void changeScene(Scene s)
	{
		Mainloop.popUntil(this);

		currentScene = s;
		background.textureName = s.background;
		background.scale = max(win.size.x / background.localBounds.width, win.size.y / background.localBounds.height) * Vec2(1, 1);
		background.relativeOrigin = Vec2(.5, .5);
		background.pos = .5 * win.size;

		if(currentScene.characters.length >= 1)
			with(currentScene.characters[0])
			{
				relativeOrigin = Vec2(1, 1);
				pos = Vec2(.17, .90) * win.size;
				scale = .15 * win.size.x / localBounds.width * Vec2(1,1);
			}
		if(currentScene.characters.length >= 2)
			with(currentScene.characters[1])
			{
				relativeOrigin = Vec2(0, 1);
				pos = Vec2(.83, .90) * win.size;
				scale = .15 * win.size.x / localBounds.width * Vec2(1,1);
			}

		currentScene.onInit();

		Mainloop.pushScreen(currentScene.screen);
	}

	void exit()
	{
		Mainloop.popUntil(this);
		Mainloop.popScreen;
	}

	void enableTimer(double timeout) { timerEnabled = true; timer.remainingTime = timeout; }
	void disableTimer() { timerEnabled = false; }
	void enableDistractor(double speed) { distractorEnabled = true; distractor.speed = speed; }
	void disableDistractor() { distractorEnabled = false; }

	LuaTable toLuaTable()
	{
		LuaTable t = Lua.makeTable;
		t["ptr"] = ptr2string(cast(void *) this);
		t["setScene"] = delegate void(LuaTable t, LuaTable u) {
			string2ptr!SceneScreen(t.get!string("ptr")).setScene(Scene.fromLuaTable(u));
		};
		t["changeScene"] = delegate void(LuaTable t, LuaTable u) {
			string2ptr!SceneScreen(t.get!string("ptr")).changeScene(Scene.fromLuaTable(u));
		};
		t["exitScene"] = delegate void(LuaTable t) { string2ptr!SceneScreen(t.get!string("ptr")).exit; };
		t["enableTimer"] = delegate void(LuaTable t, double f) { string2ptr!SceneScreen(t.get!string("ptr")).enableTimer(f); };
		t["disableTimer"] = delegate void(LuaTable t) { string2ptr!SceneScreen(t.get!string("ptr")).disableTimer; };
		t["enableDistractor"] = delegate void(LuaTable t, double f) { string2ptr!SceneScreen(t.get!string("ptr")).enableDistractor(f); };
		t["disableDistractor"] = delegate void(LuaTable t) { string2ptr!SceneScreen(t.get!string("ptr")).disableDistractor; };
		t["setHudVisibility"] = delegate void(LuaTable t, LuaTable v)
		{
			SceneScreen s = string2ptr!SceneScreen(t.get!string("ptr"));
			if(!v["gametime"].isNil) s.gametimeVisible = v.get!bool("gametime");
			if(!v["realtime"].isNil) s.realtimeVisible = v.get!bool("realtime");
			if(!v["fame"].isNil) s.fameVisible = v.get!bool("fame");
			if(!v["stressmeter"].isNil) s.meterVisible = v.get!bool("stressmeter");
		};
		t["setTimeString"] = delegate void(LuaTable t, string s) { string2ptr!SceneScreen(t.get!string("ptr")).timeText = s; };

		return t;
	}

	override void init()
	{
		timer = new SceneTimer(win.size);
		timer.pos = win.size * Vec2(.1, .12);

		distractor = new ScenePongDistractor(win.size);
		distractor.pos = win.size * Vec2(.85, .12);

		gametime = timeInfoBox();
		gametime.relativeOrigin = Vec2(0, 0);
		gametime.pos = Vec2(.02, .01) * win.size;
		gametime.textCallback = () => timeText == "" ? format("%s\n%s years old", Player.dateString, Player.age) : timeText;

		fame = fameKarmaBox();
		fame.relativeOrigin = Vec2(1, 0);
		fame.pos = Vec2(.99, .01) * win.size;

		realtime = realTimeBox();
		realtime.relativeOrigin = Vec2(1, 1);
		realtime.pos = Vec2(.99, .97) * win.size;

		stressMeter = new StressMeter();
		stressMeter.size = Vec2(.6, .05) * win.size;
		stressMeter.relativeOrigin = Vec2(.5, 0);
		stressMeter.pos = Vec2(.5, .01) * win.size;

		changeScene(currentScene);
	}

	override void setWindow(RenderWindow w) { win = w; }

	override void event(sfEvent) { }
	override void update(double dt) { updateInactive(dt); }
	override void updateInactive(double dt)
	{
		if(timerEnabled)
		{
			timer.update(dt);
			if(timer.finished)
				currentScene.onTimeout();
		}
		if(distractorEnabled)
		{
			distractor.update(dt);
			distractor.updateMouse(Mouse.position(win) / win.size);
			if(distractor.lost)
				currentScene.onDistraction();
		}
		gametime.update;
		fame.update;
		realtime.update;
		stressMeter.update;
	}

	override void draw()
	{
		win.draw(background);
		if(gametimeVisible) win.draw(gametime);
		if(fameVisible) win.draw(fame);
		if(realtimeVisible) win.draw(realtime);
		if(meterVisible) win.draw(stressMeter);
		if(timerEnabled)
			win.draw(timer);
		if(distractorEnabled)
			win.draw(distractor);
		currentScene.characters.each!(a => win.draw(a));
	}

	override void finish() { }
}
