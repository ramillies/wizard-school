import std.range;
import std.array;
import std.algorithm;
import std.datetime;
import std.format;
import std.stdio;
import std.conv;

import sfml;
import luad.all;
import bindbc.sfml;

import player;
import resources;
import coolsprite,reacttext;

class StressMeter: Transformable, Drawable, Bboxable
{
	private CoolSprite back, front;
	private ReactiveText txt;

	this()
	{
		back = new CoolSprite;
		back.textureName = "progress meter";
		back.tilenumber = 0;

		front = new CoolSprite;
		front.textureName = "progress meter";
		front.tilenumber = 1;

		txt = new ReactiveText;
		txt.relativeOrigin = Vec2(.5, .5);
		txt.color = Color(213,178,11);
		txt.outlineColor = Color(39, 19, 0);
		txt.outlineThickness = 2;
		txt.textCallback = () => format("%s / 100", Player.stress == 0 ? to!int(Player.fatigue + .5) : to!int(Player.stress + .5));
		txt.font = Fonts.text;

		transform = new Transform;
	}

	@property void size(Vec2 v)
	{
		back.scale = v / back.bounds.size;
		front.scale = v / front.bounds.size;
		txt.pos = .5 * v;
		txt.fontSize = to!uint(v.y * .75);
		txt.update;
	}

	mixin(customTransformable);
	private Vec2 _relativeOrigin = Vec2(0.5, 0.5);
	private bool _relativeOriginAllowed = false;

	void noRelativeOrigin() { _relativeOriginAllowed = false; }
	void updateOrigin() { if(_relativeOriginAllowed) origin = localBounds().relativePoint(_relativeOrigin) * back.scale; }

	@property void relativeOrigin(Vec2 v)
	{
		_relativeOrigin = v;
		_relativeOriginAllowed = true;
		updateOrigin;
	}

	@property Vec2 relativeOrigin() { return _relativeOrigin; }

	@property Rect bounds()
	{
		sfTransform myTransform = transform.matrix;
		return Rect.fromFloatRect(sfTransform_transformRect(&myTransform, localBounds().toFloatRect));
	}
	@property Rect localBounds() { return back.localBounds; }

	void update()
	{
		if(Player.stress > 0.)
		{
			front.tilenumber = 2;
			Rect r = front.textureRect;
			r.width *= .06889 + Player.stress/100 * .86197;
			front.textureRect = r;
		}
		else
		{
			front.tilenumber = 1;
			Rect r = front.textureRect;
			r.width *= .06889 + Player.fatigue/100 * .86197;
			front.textureRect = r;
		}
		txt.update;
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		target.draw(back, statesToUse);
		target.draw(front, statesToUse);
		target.draw(txt, statesToUse);
	}
}
