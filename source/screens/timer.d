import std.array;
import std.algorithm;
import std.stdio;
import std.json;
import std.range;
import std.conv;
import std.format;

import mainloop;
import resources;
import reacttext;
import coolsprite;

import sfml;
import bindbc.sfml;

class SceneTimer: ReactiveText
{
	public double remainingTime;
	public bool paused = false;

	this(Vec2 winSize)
	{
		super();
		remainingTime = 0.;
		font = Fonts.heading;
		color = Color.Red;
		outlineColor = Color.Yellow;
		fontSize = to!uint(100 * winSize.x)/1920;
		outlineThickness = 4 * winSize.x / 1920;
		relativeOrigin = Vec2(.5, 0);
		textCallback = () => format("%d", (remainingTime+.5).to!int);
	}

	@property bool finished() { return remainingTime <= 0; }

	void pause() { paused = true; }
	void resume() { paused = false; }
	void toggle() { paused = !paused; }

	void update(double dt)
	{
		if(!paused)
			remainingTime -= dt;
		if(remainingTime <= 0)
			remainingTime = 0;
		super.update;
	}
}
