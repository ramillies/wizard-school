import std.algorithm;
import std.array;
import std.range;
import std.stdio;

import coolsprite, reacttext;
import worldobject;
import lua;
import resources;
import util;

import luad.all;
import sfml;
import bindbc.sfml;

class ScriptableObject: WorldObject
{
	private CoolSprite sprite;
	LuaTable luaObject;

	private bool _removed;
	private string _name, _desc, _type;
	private void delegate(LuaTable, LuaTable) initFunc;
	private void delegate(LuaTable, double) updateFunc;
	private void delegate(LuaTable, LuaTable) collideWithObjectFunc;
	private void delegate(LuaTable) collideWithPlayerFunc;

	this(string objectName, LuaTable params = Lua.makeTable)
	{
		sprite = new CoolSprite;
		transform = new Transform;
		luaObject = makeLuaObject();
		_removed = false;

		auto objectTable = ConfigFiles.get("objects");
		if(objectName in objectTable)
		{
			LuaTable scripts = Lua.loadfile(`data/scripts/objects/` ~ objectTable[objectName]["scriptFile"].str).call!LuaTable();
			initFunc = scripts.get!(void delegate(LuaTable, LuaTable))("init");
			updateFunc = scripts.get!(void delegate(LuaTable, double))("update");
			collideWithObjectFunc = scripts.get!(void delegate(LuaTable, LuaTable))("collide");
			collideWithPlayerFunc = scripts.get!(void delegate(LuaTable))("collidePlayer");

			initFunc(luaObject, params);
		}
	}

	mixin(customTransformable);
	mixin(relativeOriginMixin);
	mixin(transformableBbox);

	@property Rect localBounds() { return sprite.localBounds; }

	override @property string name() { return _name; }
	override @property string description() { return _desc; }
	override @property string type() { return _type; }

	LuaTable makeLuaObject()
	{
		LuaTable obj = Lua.makeTable;
		obj["ptr"] = ptr2string(cast(void *) this);
		obj["setSprite"] = delegate void(LuaTable self, LuaTable spr)
		{
			ScriptableObject o = string2ptr!ScriptableObject(self.get!string("ptr"));
			if(!spr["tileset"].isNil)
				o.sprite.textureName = spr.get!string("tileset");
			if(!spr["tilenumber"].isNil)
				o.sprite.tilenumber = spr.get!int("tilenumber");
		};
		obj["setName"] = delegate void(LuaTable self, string n) { string2ptr!ScriptableObject(self.get!string("ptr"))._name = n; };
		obj["setType"] = delegate void(LuaTable self, string n) { string2ptr!ScriptableObject(self.get!string("ptr"))._type = n; };
		obj["setDescription"] = delegate void(LuaTable self, string n) { string2ptr!ScriptableObject(self.get!string("ptr"))._desc = n; };
		obj["getName"] = delegate string(LuaTable self) { return string2ptr!ScriptableObject(self.get!string("ptr"))._name; };
		obj["getType"] = delegate string(LuaTable self) { return string2ptr!ScriptableObject(self.get!string("ptr"))._type; };
		obj["getDescription"] = delegate string(LuaTable self) { return string2ptr!ScriptableObject(self.get!string("ptr"))._desc; };
		obj["remove"] = delegate void(LuaTable self) { string2ptr!ScriptableObject(self.get!string("ptr")).remove; };
		obj["getPos"] = delegate double[](LuaTable self)
		{
			ScriptableObject obj = string2ptr!ScriptableObject(self.get!string("ptr"));
			Vec2 pos = obj.bounds.center;
			return [ pos.x, pos.y ];
		};
		obj["setPos"] = delegate void(LuaTable self, double[] pos)
		{
			if(pos.length < 2) return;
			ScriptableObject obj = string2ptr!ScriptableObject(self.get!string("ptr"));
			Vec2 v = Vec2(pos[0], pos[1]);
			obj.pos = v;
		};
		obj["move"] = delegate void(LuaTable self, double[] m)
		{
			if(m.length < 2) return;
			ScriptableObject obj = string2ptr!ScriptableObject(self.get!string("ptr"));
			Vec2 movement = Vec2(m[0], m[1]);
			obj.pos = obj.pos + movement;
		};
		obj["invalid"] = false;

		return obj;
	}

	void remove() { _removed = true; luaObject["invalid"] = true; }
	@property bool removed() { return _removed; }

	override void mouseOver(bool b)
	{
		sprite.color = b ? Color(255, 255, 120) : Color.White;
	}

	override void update(double days)
	{
		updateFunc(luaObject, days);
	}

	void collideWith(ScriptableObject other) { collideWithObjectFunc(luaObject, other.luaObject); }
	void collideWithPlayer() { collideWithPlayerFunc(luaObject); }

	override void draw(RenderTarget target, RenderStates states)
	{
		RenderStates statesToUse = states;
		sfTransform myTransform = transform.matrix;
		sfTransform_combine(&(statesToUse.transform), &myTransform);

		target.draw(sprite, statesToUse);
	}

}
