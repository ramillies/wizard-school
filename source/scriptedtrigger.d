import std.algorithm;
import std.array;
import std.range;
import std.stdio;

import coolsprite, reacttext;
import worldobject;
import lua;
import resources;
import util;

import luad.all;
import sfml;
import bindbc.sfml;

class ScriptableTrigger
{
	LuaTable luaObject;

	private bool _removed;
	private void delegate(LuaTable, LuaTable) initFunc;
	private void delegate(LuaTable, double) fireFunc;

	this(string objectName, LuaTable params = Lua.makeTable)
	{
		luaObject = makeLuaObject();
		_removed = false;

		auto objectTable = ConfigFiles.get("triggers");
		if(objectName in objectTable)
		{
			LuaTable scripts = Lua.loadfile(`data/scripts/triggers/` ~ objectTable[objectName]["scriptFile"].str).call!LuaTable();
			initFunc = scripts.get!(void delegate(LuaTable, LuaTable))("init");
			fireFunc = scripts.get!(void delegate(LuaTable, double))("fire");

			initFunc(luaObject, params);
		}
	}

	LuaTable makeLuaObject()
	{
		LuaTable obj = Lua.makeTable;
		obj["ptr"] = ptr2string(cast(void *) this);
		obj["remove"] = delegate void(LuaTable self) { string2ptr!ScriptableTrigger(self.get!string("ptr")).remove; };

		return obj;
	}

	void remove() { _removed = true; }
	@property bool removed() { return _removed; }

	void update(double days)
	{
		fireFunc(luaObject, days);
	}
}
