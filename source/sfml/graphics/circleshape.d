module sfml.graphics.circleshape;

import sfml.graphics.shape;
import sfml.graphics.transformable;
import sfml.graphics.drawable;
import sfml.graphics.rendertarget;
import sfml.graphics.color;
import sfml.graphics.texture;
import sfml.graphics.renderstates;
import sfml.system.vec;
import sfml.system.rect;

import bindbc.sfml;

class CircleShape: Transformable, Drawable
{
	private sfCircleShape *t;

	this() { t = sfCircleShape_create(); }
	
	mixin(transformationsForType("sfCircleShape"));
	mixin(bboxesForType("sfCircleShape"));
	mixin(shapeMethodsForType("sfCircleShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@property float radius() { return sfCircleShape_getRadius(t); }
	@property void radius(float f) { sfCircleShape_setRadius(t, f); }
	@property void pointCount(size_t c) { sfCircleShape_setPointCount(t, c); }

	@property sfCircleShape *internalPtr() { return t; }
}
