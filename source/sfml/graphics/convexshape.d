module sfml.graphics.convexshape;

import sfml.graphics.shape;
import sfml.graphics.transformable;
import sfml.graphics.drawable;
import sfml.graphics.rendertarget;
import sfml.graphics.color;
import sfml.graphics.texture;
import sfml.graphics.renderstates;
import sfml.system.vec;
import sfml.system.rect;

import bindbc.sfml;

class ConvexShape: Transformable, Drawable
{
	private sfConvexShape *t;

	this() { t = sfConvexShape_create(); }
	
	mixin(transformationsForType("sfConvexShape"));
	mixin(bboxesForType("sfConvexShape"));
	mixin(shapeMethodsForType("sfConvexShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@property void pointCount(size_t c) { sfConvexShape_setPointCount(t, c); }
	void setPoint(size_t index, Vec2 point) { sfConvexShape_setPoint(t, index, point.toVector2f); }
	@property void points(Vec2[] pts)
	{
		this.pointCount = pts.length;
		foreach(n, p; pts)
			setPoint(n, p);
	}

	@property sfConvexShape *internalPtr() { return t; }
}
