module sfml.graphics.drawable;

import sfml.graphics.rendertarget;
import sfml.graphics.renderstates;

import bindbc.sfml;

enum drawableMixin = `override void draw(RenderTarget target, RenderStates states)
{
	target.draw(this, states);
}`;

interface Drawable
{
	void draw(RenderTarget target, RenderStates states);
}
