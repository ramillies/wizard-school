module sfml.graphics.font;

import std.string;
import std.conv;

import bindbc.sfml;

class Font
{
	private sfFont *f;

	this(sfFont *ptr) { f = ptr; }
	~this() { sfFont_destroy(f); }

	static Font fromFile(string path) { return new Font(sfFont_createFromFile(path.toStringz)); }

	sfGlyph glyph(dchar character, uint fontSize, bool isBold = false, float outline = 0.)
	{
		return sfFont_getGlyph(f, character.to!uint, fontSize, isBold, outline);
	}

	@property sfFont *internalPtr() { return f; }
}
