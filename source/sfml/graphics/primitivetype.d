module sfml.graphics.primitivetype;

import bindbc.sfml;

enum PrimitiveType { Points, Lines, LineStrip, Triangles, TriangleStrip, TriangleFan, Quads };
