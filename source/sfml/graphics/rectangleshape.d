module sfml.graphics.rectangleshape;

import sfml.graphics.shape;
import sfml.graphics.transformable;
import sfml.graphics.drawable;
import sfml.graphics.rendertarget;
import sfml.graphics.color;
import sfml.graphics.texture;
import sfml.graphics.renderstates;
import sfml.system.vec;
import sfml.system.rect;

import bindbc.sfml;

class RectangleShape: Transformable, Drawable
{
	private sfRectangleShape *t;

	this() { t = sfRectangleShape_create(); }
	
	mixin(transformationsForType("sfRectangleShape"));
	mixin(bboxesForType("sfRectangleShape"));
	mixin(shapeMethodsForType("sfRectangleShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@property Vec2 size() { return Vec2.fromVector2f(sfRectangleShape_getSize(t)); }
	@property void size(Vec2 v) { sfRectangleShape_setSize(t, v.toVector2f); }

	@property sfRectangleShape *internalPtr() { return t; }
}
