module sfml.graphics.renderstates;

import bindbc.sfml;
import sfml.graphics.texture;


struct RenderStates
{
	sfBlendMode blendMode = sfBlendAlpha;
	sfTransform transform = sfTransform_Identity;
	Texture texture = null;
	const(sfShader) *shader = null;

	sfRenderStates toSfRenderStates() { return sfRenderStates(blendMode, transform, texture is null ? null : texture.internalPtr, shader); }
	RenderStates fromSfRenderStates(sfRenderStates r) { return RenderStates(r.blendMode, r.transform, new Texture(sfTexture_copy(r.texture)), r.shader); }
}
