module sfml.graphics.rendertarget;

import bindbc.sfml;

import sfml.system.vec;
import sfml.graphics;

interface RenderTarget
{
	@property Vec2 size();

	void clear(Color c);
	void display();

	void draw(Sprite what, RenderStates states);
	void draw(Text what, RenderStates states);
	void draw(const(sfShape) *what, RenderStates states);
	void draw(CircleShape what, RenderStates states);
	void draw(ConvexShape what, RenderStates states);
	void draw(RectangleShape what, RenderStates states);
	void draw(VertexArray what, RenderStates states);
	void draw(const(sfVertexBuffer) *what, RenderStates states);
	void draw(Drawable what, RenderStates states);

	@property void view(View);
	@property View view();
	@property View defaultView();

	Vec2 pixelToCoords(Vec2 point, View);
	Vec2 coordsToPixel(Vec2 point, View);
}
