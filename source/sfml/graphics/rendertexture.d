module sfml.graphics.rendertexture;

import std.conv;

import bindbc.sfml;

import sfml.system.vec;
import sfml.graphics;
import sfml.window.videomode;
import sfml.window.window;
import sfml.util;

class RenderTexture: RenderTarget
{
	private sfRenderTexture* win;

	this(uint width, uint height)
	{
		win = sfRenderTexture_create(width, height, 0);
	}

	~this() { sfRenderTexture_destroy(win); }

	@property Vec2 size() { return Vec2.fromVector2u(sfRenderTexture_getSize(win)); }

	void clear(Color c = Color.Black) { sfRenderTexture_clear(win, c.toSfColor); }
	void display() { sfRenderTexture_display(win); }

	void draw(Sprite what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawSprite(win, what.internalPtr, &s); }
	void draw(Text what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawText(win, what.internalPtr, &s); }
	void draw(const(sfShape) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawShape(win, what, &s); }
	void draw(CircleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawCircleShape(win, what.internalPtr, &s); }
	void draw(ConvexShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawConvexShape(win, what.internalPtr, &s); }
	void draw(RectangleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawRectangleShape(win, what.internalPtr, &s); }
	void draw(VertexArray what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawVertexArray(win, what.internalPtr, &s); }
	void draw(const(sfVertexBuffer) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawVertexBuffer(win, what, &s); }
	void draw(Drawable what, RenderStates states = RenderStates()) { what.draw(this, states); }

	@property void view(View v) { sfRenderTexture_setView(win, v.internalPtr); }
	@property View view() { return new View(sfView_copy(sfRenderTexture_getView(win))); }
	@property View defaultView() { return new View(sfView_copy(sfRenderTexture_getDefaultView(win))); }

	Vec2 pixelToCoords(Vec2 point, View v = null) { return Vec2.fromVector2f(sfRenderTexture_mapPixelToCoords(win, point.toVector2i, v is null ? null : v.internalPtr)); }
	Vec2 coordsToPixel(Vec2 point, View v = null) { return Vec2.fromVector2i(sfRenderTexture_mapCoordsToPixel(win, point.toVector2f, v is null ? null : v.internalPtr)); }

	@property Texture texture() { return new Texture(sfTexture_copy(cast(sfTexture *) sfRenderTexture_getTexture(win))); }

	@property void smooth(bool b) { sfRenderTexture_setSmooth(win, b.to!uint); }
	@property bool smooth() { return sfRenderTexture_isSmooth(win).to!bool; }

	@property sfRenderTexture *internalPtr() { return win; }
}
