module sfml.graphics.renderwindow;

import std.conv;

import bindbc.sfml;

import sfml.system.vec;
import sfml.graphics;
import sfml.window.videomode;
import sfml.window.window;
import sfml.util;

class RenderWindow: RenderTarget
{
	private sfRenderWindow* win;

	this(VideoMode mode, string title, WindowStyle style)
	{
		sfContextSettings settings = sfContextSettings(0, 0, 8, 1, 1, 0, 0);
		win = sfRenderWindow_createUnicode(mode.toSfMode, stringToUintArray(title).ptr, style, &settings);
	}

	~this() { sfRenderWindow_destroy(win); }

	void close() { sfRenderWindow_close(win); }
	bool isOpen() { return sfRenderWindow_isOpen(win).to!bool; }

	bool pollEvent(sfEvent *e) { return sfRenderWindow_pollEvent(win, e).to!bool; }
	bool waitEvent(sfEvent *e) { return sfRenderWindow_waitEvent(win, e).to!bool; }

	@property Vec2 position() { return Vec2.fromVector2i(sfRenderWindow_getPosition(win)); }
	@property void position(Vec2 pos) { sfRenderWindow_setPosition(win, pos.toVector2i); }
	@property Vec2 size() { return Vec2.fromVector2u(sfRenderWindow_getSize(win)); }
	@property void size(Vec2 pos) { sfRenderWindow_setSize(win, pos.toVector2u); }

	void clear(Color c = Color.Black) { sfRenderWindow_clear(win, c.toSfColor); }
	void display() { sfRenderWindow_display(win); }

	void draw(Sprite what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawSprite(win, what.internalPtr, &s); }
	void draw(Text what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawText(win, what.internalPtr, &s); }
	void draw(const(sfShape) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawShape(win, what, &s); }
	void draw(CircleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawCircleShape(win, what.internalPtr, &s); }
	void draw(ConvexShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawConvexShape(win, what.internalPtr, &s); }
	void draw(RectangleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawRectangleShape(win, what.internalPtr, &s); }
	void draw(VertexArray what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawVertexArray(win, what.internalPtr, &s); }
	void draw(const(sfVertexBuffer) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawVertexBuffer(win, what, &s); }
	void draw(Drawable what, RenderStates states = RenderStates()) { what.draw(this, states); }

	@property void fpsLimit(uint limit) { sfRenderWindow_setFramerateLimit(win, limit); }
	@property void vsync(bool enabled) { sfRenderWindow_setVerticalSyncEnabled(win, enabled ? 1 : 0); }
	void show() { sfRenderWindow_setVisible(win, 1); }
	void hide() { sfRenderWindow_setVisible(win, 0); }

	@property void view(View v) { sfRenderWindow_setView(win, v.internalPtr); }
	@property View view() { return new View(sfView_copy(sfRenderWindow_getView(win))); }
	@property View defaultView() { return new View(sfView_copy(sfRenderWindow_getDefaultView(win))); }

	@property void cursorVisible(bool b) { sfRenderWindow_setMouseCursorVisible(win, b.to!uint); }
	@property void cursorGrabbed(bool b) { sfRenderWindow_setMouseCursorGrabbed(win, b.to!uint); }

	Vec2 pixelToCoords(Vec2 point, View v = null) { return Vec2.fromVector2f(sfRenderWindow_mapPixelToCoords(win, point.toVector2i, v is null ? null : v.internalPtr)); }
	Vec2 coordsToPixel(Vec2 point, View v = null) { return Vec2.fromVector2i(sfRenderWindow_mapCoordsToPixel(win, point.toVector2f, v is null ? null : v.internalPtr)); }

	@property sfRenderWindow *internalPtr() { return win; }
}
