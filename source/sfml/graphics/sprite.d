module sfml.graphics.sprite;

import bindbc.sfml;

import sfml.graphics.color;
import sfml.graphics.transformable;
import sfml.graphics.texture;
import sfml.graphics.drawable;
import sfml.graphics.rendertarget;
import sfml.graphics.renderstates;
import sfml.system.vec;
import sfml.system.rect;

class Sprite: Transformable, Drawable, Bboxable
{
	protected sfSprite *t;

	this() { t = sfSprite_create(); }
	this(sfSprite *ptr) { t = ptr; }

	~this() { sfSprite_destroy(t); }

	static Sprite create() { return new Sprite(sfSprite_create()); }

	@property Color color() { return Color.fromSfColor(sfSprite_getColor(t)); }
	@property Texture texture() { return new Texture(cast(sfTexture *) sfTexture_copy(sfSprite_getTexture(t))); }
	@property Rect textureRect() { return Rect.fromIntRect(sfSprite_getTextureRect(t)); }
	@property void color(Color c) { sfSprite_setColor(t, c.toSfColor()); }
	@property void texture(Texture tex) { sfSprite_setTexture(t, tex.internalPtr, 0); }
	@property void textureRect(Rect r) { sfSprite_setTextureRect(t, r.toIntRect); }

	Sprite dup() { return new Sprite(sfSprite_copy(t)); }

	mixin(transformationsForType("sfSprite"));
	mixin(relativeOriginMixin);
	mixin(bboxesForType("sfSprite"));

	@property sfSprite *internalPtr() { return t; }

	mixin(drawableMixin);
}
