module sfml.graphics.text;

import bindbc.sfml;

import sfml.system.rect;
import sfml.system.vec;
import sfml.graphics.transformable;
import sfml.graphics.color;
import sfml.graphics.font;
import sfml.graphics.rendertarget;
import sfml.graphics.drawable;
import sfml.graphics.renderstates;
import sfml.util;

enum TextStyle : uint { Regular = 0U, Bold = 1U, Italic = 2U, Underlined = 4U, Strikethrough = 8U }

class Text: Transformable, Drawable, Bboxable
{
	private sfText *t;

	this() { t = sfText_create(); }
	this(sfText *ptr) { t = ptr; }
	~this() { sfText_destroy(t); }

	static Text create() { return new Text(sfText_create()); }

	@property Color color() { return Color.fromSfColor(sfText_getFillColor(t)); }
	@property void color(Color c) { sfText_setFillColor(t, c.toSfColor()); }
	@property Color outlineColor() { return Color.fromSfColor(sfText_getOutlineColor(t)); }
	@property void outlineColor(Color c) { sfText_setOutlineColor(t, c.toSfColor()); }

	mixin(transformationsForType("sfText"));
	mixin(relativeOriginMixin);
	mixin(bboxesForType("sfText"));
	
	@property Font font() { return new Font(cast(sfFont *) sfText_getFont(t)); }
	@property void font(Font f) { sfText_setFont(t, f.internalPtr); }
	@property uint fontSize() { return sfText_getCharacterSize(t); }
	@property void fontSize(uint size) { sfText_setCharacterSize(t, size); }
	@property void lineSpacing(float f) { sfText_setLineSpacing(t, f); }
	@property float letterSpacing() { return sfText_getLetterSpacing(t); }
	@property void letterSpacing(float f) { sfText_setLetterSpacing(t, f); }
	@property uint style() { return sfText_getStyle(t); }
	@property void style(uint f) { sfText_setStyle(t, f); }

	@property float outlineThickness() { return sfText_getOutlineThickness(t); }
	@property void outlineThickness(float f) { sfText_setOutlineThickness(t, f); }
			
	@property string text() { return uintPtrToString(sfText_getUnicodeString(t)); }
	@property void text(string s) { sfText_setUnicodeString(t, stringToUintArray(s).ptr); }

	@property sfText *internalPtr() { return t; }

	mixin(drawableMixin);
}
