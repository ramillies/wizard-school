module sfml.graphics.texture;

import std.conv;
import std.stdio;
import std.string;

import sfml.system.rect;
import sfml.system.vec;

import bindbc.sfml;

class Texture
{
	private sfTexture *t;

	this(sfTexture *ptr) { t = ptr; }
	~this() { sfTexture_destroy(t); }

	static Texture create(uint width, uint height) { return new Texture(sfTexture_create(width, height)); }
	static Texture fromFile(string path) { return new Texture(sfTexture_createFromFile(path.toStringz, null)); }
	static Texture fromFile(string path, Rect rect)
	{
		sfIntRect r = rect.toIntRect;
		return new Texture(sfTexture_createFromFile(path.toStringz, &r));
	}

	@property void smooth(bool b) { sfTexture_setSmooth(t, b.to!uint); }
	@property bool smooth() { return sfTexture_isSmooth(t).to!bool; }
	@property void repeated(bool b) { sfTexture_setRepeated(t, b.to!uint); }
	@property bool repeated() { return sfTexture_isRepeated(t).to!bool; }

	@property Vec2 size() { return Vec2.fromVector2u(sfTexture_getSize(t)); }

	Texture dup() { return new Texture(sfTexture_copy(t)); }

	@property sfTexture *internalPtr() { return t; }
}
