module sfml.graphics.transform;

import sfml.graphics.transformable;
import sfml.system.vec;
import sfml.system.rect;

import bindbc.sfml;

class Transform: Transformable
{
	private sfTransformable* t;

	this() { t = sfTransformable_create(); }
	
	mixin(transformationsForType("sfTransformable"));
}
