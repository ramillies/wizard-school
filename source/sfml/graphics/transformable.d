module sfml.graphics.transformable;

import std.format;

import bindbc.sfml;

import sfml.system.vec;
import sfml.system.rect;


interface Transformable
{
	@property Vec2 pos();
	@property float rotation();
	@property Vec2 scale();
	@property Vec2 origin();
	@property void pos(Vec2 v);
	@property void rotation(float f);
	@property void scale(Vec2 v);
	@property void origin(Vec2 v);

	@property sfTransform matrix();
	@property sfTransform inverseMatrix();
}

interface Bboxable
{
	@property Rect bounds();
	@property Rect localBounds();

	@property void relativeOrigin(Vec2 v);
	@property Vec2 relativeOrigin();
	void updateOrigin();
	void noRelativeOrigin();
}

string transformationsForType(string type)
{
	return format(`
	@property Vec2 pos() { return Vec2.fromVector2f(%1$s_getPosition(t)); }
	@property float rotation() { return %1$s_getRotation(t); }
	@property Vec2 scale() { return Vec2.fromVector2f(%1$s_getScale(t)); }
	@property Vec2 origin() { return Vec2.fromVector2f(%1$s_getOrigin(t)); }
	@property void pos(Vec2 v) { %1$s_setPosition(t, v.toVector2f); }
	@property void rotation(float f) { %1$s_setRotation(t, f); }
	@property void scale(Vec2 v) { %1$s_setScale(t, v.toVector2f); }
	@property void origin(Vec2 v) { %1$s_setOrigin(t, v.toVector2f); }

	@property sfTransform matrix() { return %1$s_getTransform(t); }
	@property sfTransform inverseMatrix() { return %1$s_getInverseTransform(t); }
	`, type);
}

string bboxesForType(string type)
{
	return format(`
	@property Rect bounds() { return Rect.fromFloatRect(%1$s_getGlobalBounds(t)); }
	@property Rect localBounds() { return Rect.fromFloatRect(%1$s_getLocalBounds(t)); }
	`, type);
}

enum relativeOriginMixin = `
private Vec2 _relativeOrigin = Vec2(0.5, 0.5);
private bool _relativeOriginAllowed = false;

void noRelativeOrigin() { _relativeOriginAllowed = false; }
void updateOrigin() { if(_relativeOriginAllowed) origin = localBounds().relativePoint(_relativeOrigin); }

@property void relativeOrigin(Vec2 v)
{
	_relativeOrigin = v;
	_relativeOriginAllowed = true;
	updateOrigin;
}

@property Vec2 relativeOrigin() { return _relativeOrigin; }
`;

enum customTransformable = `
private Transform transform;

@property Vec2 pos() { return transform.pos; }
@property float rotation() { return transform.rotation; }
@property Vec2 scale() { return transform.scale; }
@property Vec2 origin() { return transform.origin; }
@property void pos(Vec2 v) { transform.pos = v; }
@property void rotation(float f) { transform.rotation = f; }
@property void scale(Vec2 v) { transform.scale = v; }
@property void origin(Vec2 v) { transform.origin = v; }

@property sfTransform matrix() { return transform.matrix; }
@property sfTransform inverseMatrix() { return transform.matrix; }
`;

enum transformableBbox = `
	@property Rect bounds()
	{
		sfTransform myTransform = transform.matrix;
		return Rect.fromFloatRect(sfTransform_transformRect(&myTransform, localBounds().toFloatRect));
	}
`;
