module sfml.graphics.vertex;

import bindbc.sfml;
import sfml.system.vec;
import sfml.graphics.color;

struct Vertex
{
	Vec2 pos = Vec2(0, 0);
	Color color = Color(0,0,0,255);
	Vec2 texCoords = Vec2(0, 0);

	sfVertex toSfVertex() { return sfVertex(pos.toVector2f, color.toSfColor, texCoords.toVector2f); }
	static Vertex fromSfVertex(sfVertex v) { return Vertex(Vec2.fromVector2f(v.position), Color.fromSfColor(v.color), Vec2.fromVector2f(v.texCoords)); }
}
