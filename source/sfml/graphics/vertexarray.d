module sfml.graphics.vertexarray;

import bindbc.sfml;
import sfml.graphics.vertex;
import sfml.graphics.primitivetype;
import sfml.graphics.rendertarget;
import sfml.graphics.renderstates;
import sfml.graphics.drawable;
import sfml.system.rect;

class VertexArray: Drawable
{
	private sfVertexArray *t;

	this() { t = sfVertexArray_create(); }

	void append(Vertex v) { sfVertexArray_append(t, v.toSfVertex); }
	void append(Vertex[] vertices) { foreach(v; vertices) append(v); }
	void clear() { sfVertexArray_clear(t); }
	
	@property PrimitiveType primitiveType() { return cast(PrimitiveType) sfVertexArray_getPrimitiveType(t); }
	@property void primitiveType(PrimitiveType p) { sfVertexArray_setPrimitiveType(t, cast(sfPrimitiveType) p); }

	@property Rect bounds() { return Rect.fromFloatRect(sfVertexArray_getBounds(t)); }

	mixin(drawableMixin);

	@property sfVertexArray *internalPtr() { return t; }
}
