module sfml.graphics.view;

import sfml.system.rect;
import sfml.system.vec;

import bindbc.sfml;

class View
{
	private sfView *t;

	this() { t = sfView_create(); }
	this(sfView *ptr) { t = ptr; }
	this(Rect r) { t = sfView_createFromRect(r.toFloatRect); }
	~this() { sfView_destroy(t); }

	@property Vec2 size() { return Vec2.fromVector2f(sfView_getSize(t)); }
	@property Vec2 center() { return Vec2.fromVector2f(sfView_getCenter(t)); }
	@property float rotation() { return sfView_getRotation(t); }
	@property Rect viewport() { return Rect.fromFloatRect(sfView_getViewport(t)); }

	@property void size(Vec2 v) { sfView_setSize(t, v.toVector2f); }
	@property void center(Vec2 v) { sfView_setCenter(t, v.toVector2f); }
	@property void rotation(float r) { sfView_setRotation(t, r); }
	@property void viewport(Rect r) { sfView_setViewport(t, r.toFloatRect); }

	void reset(Rect r) { sfView_reset(t, r.toFloatRect); }
	View dup() { return new View(sfView_copy(t)); }

	@property sfView *internalPtr() { return t; }
}
