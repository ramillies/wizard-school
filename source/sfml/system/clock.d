module sfml.system.clock;

import bindbc.sfml;

class Clock
{
	private sfClock* t;

	this() { t = sfClock_create(); }
	~this() { sfClock_destroy(t); }

	double restart() { return sfTime_asSeconds(sfClock_restart(t)); }
	double elapsed() { return sfTime_asSeconds(sfClock_getElapsedTime(t)); }
}
