module sfml.system.rect;

import std.algorithm;
import std.conv;

import bindbc.sfml;

import sfml.system.vec;

struct Rect
{
	float left, top, width, height;


	@property float right() { return left + width; }
	@property float bottom() { return top + height; }

	Vec2 relativePoint(Vec2 rel) { return Vec2(left + rel.x * width, top + rel.y * height); }
	@property Vec2 center() { return relativePoint(Vec2(.5, .5)); }
	@property Vec2 size() { return Vec2(width, height); }

	sfFloatRect toFloatRect() { return sfFloatRect(left, top, width, height); }
	sfIntRect toIntRect() { return sfIntRect(left.to!int, top.to!int, width.to!int, height.to!int); }

	static Rect fromFloatRect(sfFloatRect r) { return Rect(r.left, r.top, r.width, r.height); }
	static Rect fromIntRect(sfIntRect r) { return Rect(r.left*1., r.top*1., r.width*1., r.height*1.); }

	bool contains(Vec2 v)
	{
		return (v.x >= left) && (v.x <= right) && (v.y >= top) && (v.y <= bottom);
	}

	bool intersects(Rect r)
	{
		return (max(left, r.left) <= min(right, r.right)) && (max(top, r.top) <= min(bottom, r.bottom));
	}
}
