module sfml.system.vec;

import std.conv;
import std.math;
import bindbc.sfml;

struct Vec2
{
	double x, y;

	sfVector2f toVector2f() { return sfVector2f(x, y); }
	sfVector2i toVector2i() { return sfVector2i(x.to!int, y.to!int); }
	sfVector2u toVector2u() { return sfVector2u(x.to!uint, y.to!uint); }

	static Vec2 fromVector2f(sfVector2f v) { return Vec2(v.x, v.y); }
	static Vec2 fromVector2i(sfVector2i v) { return Vec2(v.x*1., v.y*1.); }
	static Vec2 fromVector2u(sfVector2u v) { return Vec2(v.x*1., v.y*1.); }

	double dot(Vec2 rhs) { return x*rhs.x + y*rhs.y; }
	double norm() { return sqrt(this.dot(this)); }
	Vec2 normalized() { return this/norm(); }

	Vec2 opBinary(string op)(Vec2 rhs)
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("return Vec2(x " ~ op ~ " rhs.x, y " ~ op ~ " rhs.y);");
	}

	Vec2 opBinary(string op)(double f)
	if((op == "*") || (op == "/"))
	{
		mixin("return Vec2(x " ~ op ~ " f, y " ~ op ~ " f);");
	}

	Vec2 opBinaryRight(string op)(double f)
	if(op == "*")
	{
		return Vec2(x*f, y*f);
	}

	ref Vec2 opOpAssign(string op)(Vec2 rhs)
	if((op == "+") || (op == "-"))
	{
		mixin("x " ~ op ~ "= rhs.x; y " ~ op ~ "= rhs.y;");
		return this;
	}
}
