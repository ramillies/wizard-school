module sfml.util;

import std.algorithm;
import std.array;
import std.range;
import std.conv;

uint[] stringToUintArray(string s)
{
	uint[] result;
	foreach(dchar c; s)
		result ~= c.to!uint;
	result ~= 0U;
	return result;
}

string uintPtrToString(const(uint) *s)
{
	dchar[] result;
	for(; *s != 0; s++)
		result ~= (*s).to!dchar;
	return result.to!string;
}
