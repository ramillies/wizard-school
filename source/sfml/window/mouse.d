module sfml.window.mouse;

import std.conv;

import bindbc.sfml;
import sfml.system.vec;
import sfml.graphics.renderwindow;

enum MouseButton { Left, Right, Middle, Extra1, Extra2, ButtonCount }

class Mouse
{
	static Vec2 position(RenderWindow win) { return Vec2.fromVector2i(sfMouse_getPositionRenderWindow(win.internalPtr)); }
	static void setPosition(Vec2 pos, RenderWindow win) { sfMouse_setPositionRenderWindow(pos.toVector2i, win.internalPtr); }
	static bool buttonPressed(MouseButton b) { return sfMouse_isButtonPressed(cast(sfMouseButton) b).to!bool; }
}
