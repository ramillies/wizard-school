module sfml.window.videomode;

import std.conv;

import bindbc.sfml;

struct VideoMode
{
	uint width, height, bitsPerPixel = 32;

	sfVideoMode toSfMode() { return sfVideoMode(width, height, bitsPerPixel); }
	static VideoMode fromSfMode(sfVideoMode mode) { return VideoMode(mode.width, mode.height, mode.bitsPerPixel); }

	static VideoMode desktopMode() { return VideoMode.fromSfMode(sfVideoMode_getDesktopMode()); }
	static VideoMode[] listModes()
	{
		size_t modeCount;
		const(sfVideoMode) *modes = sfVideoMode_getFullscreenModes(&modeCount);
		VideoMode[] result;

		for(size_t i = 0; i < modeCount; i++)
			result ~= VideoMode.fromSfMode(*(modes + i));
		return result;
	}

	bool valid() { return sfVideoMode_isValid(toSfMode()).to!bool; }
}
