module sfml.window.window;

import bindbc.sfml;

enum WindowStyle: uint { None = 0U, Titlebar = 1U, Resize = 2U, Close = 4U, Fullscreen = 8U, Default = 7U };
