import std.conv;
import std.format;

string ptr2string(void *ptr)
{
	return format("%s", ptr);
}

T string2ptr(T)(string chunk)
{
	return (cast(T) (cast(void *) to!size_t(chunk, 16)));
}

