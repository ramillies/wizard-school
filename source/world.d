import std.algorithm;
import std.range;
import std.array;
import std.math;
import std.random;
import std.stdio;
import std.conv;
import std.string;

import sfml;
import bindbc.sfml;
import luad.all;

import lua;
import util;

class World: Drawable
{
	double[][] map;
	enum magnification = 2 ^^ 1;
	private Texture worldTexture;
	private Sprite worldSprite;

	this() { }

	private int heightLevelAtSquare(int x, int y)
	{
		double h = map[y][x];
		return h <= 0 ? 0 :
			h < .25 ? 1 :
			h < .35 ? 2 :
			h < .5 ? 3 :
			h < .7 ? 4 :
			h < .8 ? 5 :
			6;
	}

	private Vertex vertexForPoint(int x, int y, int maxy)
	{
		Color interpolate(Color a, Color b, double fraction)
		{
			return Color(to!ubyte(a.r * 1. + (b.r - a.r)*fraction),
					to!ubyte(a.g * 1. + (b.g - a.g)*fraction),
					to!ubyte(a.b * 1. + (b.b - a.b)*fraction),
					to!ubyte(a.a * 1. + (b.a - a.a)*fraction)
				);
		}

		Color colorForHeight(double h)
		{
			return h <= 0 ? Color(0, 0, 128) :
			h < .25 ? interpolate(Color(0,0,128), Color(40,40,255), h*4) :
			h < .35 ? interpolate(Color(228,232,121), Color(225,252,22), (h-.25)*10) :
			h < .5 ? interpolate(Color(225,252,22), Color(10,177,7), (h-.35)/.15) :
			h < .7 ? interpolate(Color(121,83,41), Color(82,40,2), (h-.5)*5) :
			h < .8 ? interpolate(Color(63,53,42), Color(70,70,70), (h-.7)*10) :
			h < 1 ? interpolate(Color(176,175,178), Color(255,255,255), (h-.8)*5) :
			Color(255,255,255);
		}

		return Vertex(Vec2(magnification * x, magnification * (maxy-y)), colorForHeight(map[y][x]));
	}

	void generateMap(int logOfSide)
	{
		int side = 1 + 2^^logOfSide;
		map = side.iota.map!(n => double.nan.repeat(side).array).array;
		
		map[0][0] = uniform(0., 10.);
		map[$-1][0] = uniform(0., 10.);
		map[0][$-1] = uniform(0., 10.);
		map[$-1][$-1] = uniform(0., 10.);


		for(int n = logOfSide - 1; n >= 0; n--)
		{
			double squareWithStep(int x, int y)
			{
				if(x < 0 || y < 0 || x > 2^^(logOfSide - n) || y > 2^^(logOfSide - n))
					return 0.;
				return map[y*2^^n][x*2^^n];
			}
			for(int x = 0; x <= 2^^(logOfSide - n); x++)
				for(int y = 0; y <= 2^^(logOfSide - n); y++)
					if(x%2 == 1 && y%2 == 1)
						map[y * 2^^n][x * 2^^n] = (squareWithStep(x-1,y-1) + squareWithStep(x-1,y+1) + squareWithStep(x+1,y-1) + squareWithStep(x+1,y+1))/4 + uniform(-15., 15.)*pow(1.5, n - logOfSide);
			for(int x = 0; x <= 2^^(logOfSide - n); x++)
				for(int y = 0; y <= 2^^(logOfSide - n); y++)
					if((x+y) % 2 == 1)
						map[y * 2^^n][x * 2^^n] = (squareWithStep(x,y-1) + squareWithStep(x,y+1) + squareWithStep(x+1,y) + squareWithStep(x+1,y))/4 + uniform(-15., 15.)*pow(1.5,n - logOfSide);
		}

		double minVal = map.map!(x => x.minElement).minElement;
		double maxVal = map.map!(x => x.maxElement).maxElement;

		for(int x = 0; x < side; x++)
			for(int y = 0; y < side; y++)
				map[y][x] = (map[y][x] - minVal) / (maxVal - minVal);

		RenderTexture tex = new RenderTexture(magnification * 2^^logOfSide, magnification * 2^^logOfSide);
		tex.clear(Color.Black);

		VertexArray grid = new VertexArray;
		grid.primitiveType = PrimitiveType.Quads;
		for(int xChunk = 0; xChunk < side-1; xChunk+=256)
		{
			for(int yChunk = 0; yChunk < side-1; yChunk+=256)
			{
				grid.clear;
				for(int x = xChunk; x < xChunk+256; x++)
					for(int y = yChunk; y < yChunk+256; y++)
						grid.append([
							vertexForPoint(x, y, side),
							vertexForPoint(x+1, y, side),
							vertexForPoint(x+1, y+1, side),
							vertexForPoint(x, y+1, side)
						]);
				tex.draw(grid);
			}
		}

		worldTexture = tex.texture;

		worldSprite = new Sprite;
		worldSprite.texture = worldTexture;

		putIntoLua();
	}

	int heightLevelAtPoint(Vec2 v)
	{
		Vec2 coords = v / magnification;
		return heightLevelAtSquare(coords.x.to!int, coords.y.to!int);
	}

	Vec2 randomPoint() { return bounds.relativePoint(Vec2(.05 + .9*uniform01(), .05 + .9*uniform01)); }

	private void putIntoLua()
	{
		LuaTable t = Lua.lua.get!LuaTable("World");

		t["ptr"] = ptr2string(cast(void *) this);
		t["heightLevelAt"] = delegate int(LuaTable t, double[] pos)
		{
			if(pos.length < 2) return -1;
			return string2ptr!World(t.get!string("ptr")).heightLevelAtPoint(Vec2(pos[0], pos[1]));
		};
		t["size"] = delegate double[](LuaTable t)
		{
			Vec2 s = string2ptr!World(t.get!string("ptr")).bounds.size;
			return [ s.x, s.y ];
		};
		t["randomPoint"] = delegate double[](LuaTable t, bool delegate(double[]) filter)
		{
			World w = string2ptr!World(t.get!string("ptr"));
			Vec2 pt = w.randomPoint;
			while(!filter([ pt.x, pt.y ]))
				pt = w.randomPoint;
			return [ pt.x, pt.y ];
		};
	}

	@property Rect bounds() { return worldSprite.bounds; }

	void draw(RenderTarget target, RenderStates states)
	{
		worldSprite.draw(target, states);
	}
}
