import sfml;

interface WorldObject: Transformable, Drawable, Bboxable
{
	@property string name();
	@property string type();
	@property string description();

	@property bool removed();

	void mouseOver(bool);
	void update(double);
}
